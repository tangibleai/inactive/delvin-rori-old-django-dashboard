from datetime import datetime

from dashboard.utils import define_dates

today_string = datetime.now().strftime("%Y-%m-%d")


def message_count_sql(start=None, stop=None, step="day"):
    if not start or not stop:
        start, stop = define_dates()

    start = start.strftime("%Y-%m-%d")
    stop = stop.strftime("%Y-%m-%d")

    if step == "day":
        return f"SELECT concat(to_char(created_at, 'YYYY'), ' ', to_char(created_at, 'MM'), ' ', to_char(created_at, 'DD')) as \"Date\", " \
               f"count(id) as \"Message Count\" from message " \
               f"where direction = 'inbound' and created_at >= '{start}' and created_at <= '{stop}' " \
               f"group by concat(to_char(created_at, 'YYYY'), ' ', to_char(created_at, 'MM'), ' ', to_char(created_at, 'DD')) " \
               f"order by concat(to_char(created_at, 'YYYY'), ' ', to_char(created_at, 'MM'), ' ', to_char(created_at, 'DD')) asc;"

    if step == "week":
        return f"SELECT concat(to_char(created_at, 'IYYY'), ' ', 'W', to_char(created_at, 'IW')) as \"Date\", " \
               f"count(id) as \"Message Count\" from message " \
               f"where direction = 'inbound' and created_at >= '{start}' and created_at <= '{stop}' " \
               f"group by concat(to_char(created_at, 'IYYY'), ' ', 'W', to_char(created_at, 'IW')) " \
               f"order by concat(to_char(created_at, 'IYYY'), ' ', 'W', to_char(created_at, 'IW')) asc;"

    if step == "month":
        return f"SELECT concat(to_char(created_at, 'YYYY'), ' ', 'M', to_char(created_at, 'MM')) as \"Date\", " \
               f"count(id) as \"Message Count\" from message " \
               f"where direction = 'inbound' and created_at >= '{start}' and created_at <= '{stop}' " \
               f"group by concat(to_char(created_at, 'YYYY'), ' ', 'M', to_char(created_at, 'MM')) " \
               f"order by concat(to_char(created_at, 'YYYY'), ' ', 'M', to_char(created_at, 'MM')) asc;"


def student_count_sql(start=None, stop=None, step="day"):
    if not start or not stop:
        start, stop = define_dates()

    start = start.strftime("%Y-%m-%d")
    stop = stop.strftime("%Y-%m-%d")

    if step == "day":
        return f"SELECT concat(to_char(created_at, 'YYYY'), ' ', to_char(created_at, 'MM'), ' ', to_char(created_at, 'DD')) as \"Date\", " \
               f"count(distinct contact) as \"Student Count\" from message " \
               f"where direction = 'inbound' and created_at >= '{start}' and created_at <= '{stop}' " \
               f"group by concat(to_char(created_at, 'YYYY'), ' ', to_char(created_at, 'MM'), ' ', to_char(created_at, 'DD')) " \
               f"order by concat(to_char(created_at, 'YYYY'), ' ', to_char(created_at, 'MM'), ' ', to_char(created_at, 'DD')) asc;"

    if step == "week":
        return f"SELECT concat(to_char(created_at, 'IYYY'), ' ', 'W', to_char(created_at, 'IW')) as \"Date\", " \
               f"count(distinct contact) as \"Student Count\" from message " \
               f"where direction = 'inbound' and created_at >= '{start}' and created_at <= '{stop}' " \
               f"group by concat(to_char(created_at, 'IYYY'), ' ', 'W', to_char(created_at, 'IW')) " \
               f"order by concat(to_char(created_at, 'IYYY'), ' ', 'W', to_char(created_at, 'IW')) asc;"

    if step == "month":
        return f"SELECT concat(to_char(created_at, 'YYYY'), ' ', 'M', to_char(created_at, 'MM')) as \"Date\", " \
               f"count(distinct contact) as \"Student Count\" from message " \
               f"where direction = 'inbound' and created_at >= '{start}' and created_at <= '{stop}' " \
               f"group by concat(to_char(created_at, 'YYYY'), ' ', 'M', to_char(created_at, 'MM')) " \
               f"order by concat(to_char(created_at, 'YYYY'), ' ', 'M', to_char(created_at, 'MM')) asc;"


def today_message_count_sql():
    return f"SELECT count(id) as \"Message Count\" from message " \
           f"where direction = 'inbound' and to_char(created_at, 'YYYY-MM-DD') = '{today_string}';"


def today_student_count_sql():
    return f"SELECT count(distinct contact) as \"Student Count\" from message " \
           f"where to_char(created_at, 'YYYY-MM-DD') = '{today_string}';"
