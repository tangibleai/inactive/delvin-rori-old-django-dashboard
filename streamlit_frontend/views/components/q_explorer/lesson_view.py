import streamlit as st
import plotly.express as px

from streamlit_frontend.src.sql_utils import WhereClause
from streamlit_frontend.src.etl import get_math_data_with_clause
from streamlit_frontend.src.const import *
from streamlit_frontend.charts.question_explorer_charts import lesson_question_corr_by_attempt

def display_component(where_clause, lesson):
        lesson_clause = WhereClause(where_clause)
        lesson_clause.add_str_subclause(field=FN_LESSON,
                                        value=lesson)
        fields = [
            FN_QUESTION_NUM,
            FN_LESSON,
            FN_TOPIC,
            FN_SKILL,
            FN_QUESTION_TEXT,
            FN_ANSWER_TYPE,
            FN_HINT_SHOWN,
            FN_INSERTED_AT,
            FN_MSG_ID,
            FN_USER_ID
        ]

        lesson_df = get_math_data_with_clause(fields, lesson_clause)

        col_topic, col_skill = st.columns([1, 1])
        with col_topic:
            topic = lesson_df[FN_TOPIC].unique()[0]
            st.write("### Microlesson Topic: ", topic)

        with col_skill:
            skill = lesson_df[FN_SKILL].unique()[0]
            st.write("### Microlesson Skill: ", skill)

        possible_questions = lesson_df[FN_QUESTION_NUM].nunique()
        grouped = lesson_df.groupby(FN_USER_ID)[FN_QUESTION_NUM].nunique()
        contact_uuid_count = len(grouped[grouped == possible_questions])

        st.write(
            f"### {contact_uuid_count} students tried every question in this lesson ({possible_questions} questions total)")
        q_right_wrong_df = lesson_df[
            (lesson_df[FN_ANSWER_TYPE].isin(["failure", "hint", "success", "recovered"]))
        ]
        q_right_wrong_df = q_right_wrong_df.replace("recovered", "success")
        q_right_wrong_df = q_right_wrong_df.replace("hint", "failure")
        q_right_wrong_df = (
            q_right_wrong_df.groupby([FN_QUESTION_NUM, FN_ANSWER_TYPE])
            .agg({FN_MSG_ID: "nunique"})
            .reset_index()
            .rename(columns={FN_MSG_ID: "count"})
        )
        fig = px.histogram(
            q_right_wrong_df,
            x=FN_QUESTION_NUM,
            y="count",
            color=FN_ANSWER_TYPE,
            barnorm="percent",
            nbins=len(lesson_df[FN_QUESTION_NUM].unique()),
            category_orders={FN_ANSWER_TYPE: ["success", "failure"]},
            title="Answer correctness",
        )
        fig.update_layout(bargap=0.3)
        fig.add_hline(y=80)
        fig.update_xaxes(categoryorder="category ascending")
        st.plotly_chart(fig)

        question_corr_fig = lesson_question_corr_by_attempt(lesson_df)
        st.plotly_chart(question_corr_fig)
