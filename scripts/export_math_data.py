import os
import time
import pandas as pd
import datetime

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()

from dashboard.utils import select_from_supabase
from streamlit_frontend.src.const import *
from streamlit_frontend.src.sql_utils import WhereClause

supabase_db_name = "supabase-db-rori"
fields =  [
    FN_MSG_ID,
    FN_MSG_TEXT,
    FN_INSERTED_AT,
    FN_USER_ID,
    FN_QUESTION_TEXT,
    FN_EXPECTED_ANS,
    FN_LEVEL,
    FN_LESSON,
    FN_SKILL,
    FN_TOPIC,
    FN_QUESTION_NUM
]

where_clause = WhereClause()
where_clause.add_int_subclause(FN_LINE_NUM, RORI_RISING_LINE, '=')

query = f""" SELECT {','.join(fields)},  
             CASE 
                WHEN answer_type='success' THEN 'success'
                WHEN answer_type='failure' then 'failure'
                else 'other'
              end as answer_type
             FROM math_question_answer {where_clause.get_clause()};"""
query_result = select_from_supabase(db_name=supabase_db_name, query=query)

df = pd.DataFrame.from_records(query_result)

df.to_csv("math_question_answers.2023-05-16-2023-10-18.rising_line.csv", index=False)


