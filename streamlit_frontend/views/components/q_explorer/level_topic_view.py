import streamlit as st
import plotly.express as px
import pandas as pd

from streamlit_frontend.src.const import *
from streamlit_frontend.src.etl import get_math_data_with_clause
def display_component(where_clause):

    fields = [
        FN_QUESTION_NUM,
        FN_LESSON,
        FN_USER_ID,
        FN_MSG_ID
    ]

    level_df = get_math_data_with_clause(fields, where_clause)
    level_df['question_and_lesson'] = 'Q' + level_df[FN_QUESTION_NUM] + ' - ' + level_df[FN_LESSON]

    contact_uuid_count = level_df[FN_USER_ID].nunique()
    col_questions_tried, col_lessons_tried = st.columns([1, 1])
    with col_questions_tried:
        num_possible_questions = level_df['question_and_lesson'].nunique();

        grouped = level_df.groupby(FN_USER_ID)['question_and_lesson'].nunique()
        counts = grouped.value_counts()
        contact_uuid_count_every_question = len(grouped[grouped == num_possible_questions])

        q_counts_df = counts.reset_index()
        q_counts_df.columns = ['Number of Questions Tried', 'Number of Users']

        # Calculate percentage

        total_questions = level_df['question_and_lesson'].nunique()
        q_counts_df['% of Questions Tried'] = (q_counts_df[
                                                   'Number of Questions Tried'] / total_questions) * 100

        # Calculate reverse cumulative sum for y axis
        q_counts_df = q_counts_df.sort_values('% of Questions Tried')
        q_counts_df['Cumulative Number of Users'] = q_counts_df['Number of Users'][::-1].cumsum()[::-1]

        q_counts_df['Percentage of Users'] = (q_counts_df['Number of Users'] / contact_uuid_count) * 100
        q_counts_df['Cumulative Percentage of Users'] = (q_counts_df[
                                                             'Cumulative Number of Users'] / contact_uuid_count) * 100
        col_questions_tried_title, col_q_cum= st.columns([2, 2])

        with col_q_cum:
            percent_q_log = st.checkbox("Percentage(log scale)", key=1)
            q_cum = st.checkbox("Cumulative", key=3)

        with col_questions_tried_title:
            st.write("### Students by number of questions tried")
            if q_cum:
                st.write("Y axis specifies the number / percentage of users that tried **at least** "
                         "the percent of questions on axis X")

        if percent_q_log:
            if q_cum:
                y = 'Cumulative Percentage of Users'
                hover_data = ["Cumulative Number of Users", "Cumulative Percentage of Users"]
            else:
                y = 'Percentage of Users'
                hover_data = ["Number of Users", "Percentage of Users"]
        else:
            if q_cum:
                y = 'Cumulative Number of Users'
                hover_data = ["Cumulative Number of Users", "Cumulative Percentage of Users"]
            else:
                y = 'Number of Users'
                hover_data = ["Number of Users", "Percentage of Users"]

        fig = px.bar(q_counts_df,
                     x='% of Questions Tried',
                     y=y,
                     hover_data=hover_data,
                     color_discrete_sequence=["darkseagreen"])

        if percent_q_log:
            fig.update_yaxes(type='log')

        st.plotly_chart(fig, use_container_width=True)

    with col_lessons_tried:

        grouped = level_df.groupby(FN_USER_ID)[FN_LESSON].nunique()
        counts = grouped.value_counts()

        l_counts_df = counts.reset_index()
        l_counts_df.columns = ['Number of Lessons Tried', 'Number of Users']

        # Calculate percentage
        total_lessons = level_df[FN_LESSON].nunique()
        l_counts_df['% of Lessons Tried'] = (l_counts_df[
                                                 'Number of Lessons Tried'] / total_lessons) * 100

        # Calculate reverse cumulative sum for y axis
        l_counts_df = l_counts_df.sort_values('% of Lessons Tried')
        l_counts_df['Cumulative Number of Users'] = l_counts_df['Number of Users'][::-1].cumsum()[::-1]

        l_counts_df['Percentage of Users'] = (l_counts_df['Number of Users'] / contact_uuid_count) * 100
        l_counts_df['Cumulative Percentage of Users'] = (l_counts_df[
                                                             'Cumulative Number of Users'] / contact_uuid_count) * 100
        col_lessons_tried_title, col_l_cum = st.columns([2, 2])

        with col_l_cum:
            percent_l_log = st.checkbox("Percentage(log scale)", key=2)
            l_cum = st.checkbox("Cumulative", key=4)

        with col_lessons_tried_title:
            st.write("### Students by number of lessons tried")
            if l_cum:
                st.write("Y axis specifies the number / percentage of users that tried **at least**"
                         "the percent of lessons on axis X")

        if percent_l_log:
            if l_cum:
                y = 'Cumulative Percentage of Users'
                hover_data = ["Cumulative Number of Users", "Cumulative Percentage of Users"]
            else:
                y = 'Percentage of Users'
                hover_data = ["Number of Users", "Percentage of Users"]
        else:
            if l_cum:
                y = 'Cumulative Number of Users'
                hover_data = ["Cumulative Number of Users", "Cumulative Percentage of Users"]
            else:
                y = 'Number of Users'
                hover_data = ["Number of Users", "Percentage of Users"]

        fig = px.bar(l_counts_df,
                     x='% of Lessons Tried',
                     y=y,
                     hover_data=hover_data,
                     color_discrete_sequence=["darkviolet"],
                     text_auto=True)

        if percent_q_log:
            fig.update_yaxes(type='log')

        st.plotly_chart(fig, use_container_width=True)
    # dropdown_level = 'skill' if skill else ('topic' if topic else 'level')

    # st.write(f"### {contact_uuid_count} students tried **_at least one question_** in this {dropdown_level}")
    # st.write(f"### {contact_uuid_count_every_question} students tried **_every_** question in this {dropdown_level}")

    df_started = (
        level_df[level_df[FN_QUESTION_NUM] == str(1)]
        .groupby(FN_LESSON)
        .agg({FN_USER_ID: "nunique"})
    )
    df_started = df_started.reset_index().rename(columns={FN_USER_ID: "users_started"})
    df_completed = (
        level_df[level_df[FN_QUESTION_NUM] == str(10)]
        .groupby(FN_LESSON)
        .agg({FN_USER_ID: "nunique"})
    )
    df_completed = df_completed.reset_index().rename(
        columns={FN_USER_ID: "users_completed"}
    )
    df_ratio = pd.merge(df_completed, df_started, on=FN_LESSON, how="outer")
    df_ratio.dropna(inplace=True)
    df_ratio["ratio"] = df_ratio["users_completed"] / df_ratio["users_started"]
    fig = px.bar(df_ratio, x=FN_LESSON, y="ratio", title="Lesson Completion Ratio")

    st.plotly_chart(fig)