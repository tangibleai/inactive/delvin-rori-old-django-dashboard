from datetime import datetime

from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from dashboard.context import (
    active_user_context,
    user_lifetime_context,
    user_engagement_context,
    question_answer_context,
)
from dashboard.utils import validate_date, define_dates

port = 443 if settings.RENDER else 8000


@require_http_methods(["GET", "POST"])
@staff_member_required()
def active_user(request):
    start_date, stop_date = None, None

    if request.method == "POST":
        content = request.POST
        start_date = validate_date(content.get("start_date"))
        stop_date = validate_date(content.get("stop_date"))

    if not start_date or not stop_date:
        start_date, stop_date = define_dates()

    context = active_user_context.get_context_by_dates(start_date, stop_date)
    return render(request, 'dashboard/dashboard_gbq.html', context=context)


@require_http_methods(["GET", "POST"])
@staff_member_required()
def user_lifetime(request):
    # From the beginning of Rori
    start_date = datetime(2022, 1, 8)
    stop_date = datetime.now()

    context = user_lifetime_context.get_context(start_date, stop_date)
    return render(request, 'dashboard/dashboard_gbq.html', context=context)


@require_http_methods(["GET", "POST"])
@staff_member_required()
def user_engagement(request):
    # From the beginning of Rori
    start_date = datetime(2022, 1, 8)
    stop_date = datetime.now()

    context = user_engagement_context.get_context_by_dates(start_date, stop_date)
    return render(request, 'dashboard/dashboard_gbq.html', context=context)


@require_http_methods(["GET", "POST"])
@staff_member_required()
def question_answer(request):
    # From the beginning of Rori
    start_date = datetime(2022, 1, 8)
    stop_date = datetime.now()

    context = question_answer_context.get_context_by_dates(start_date, stop_date)
    return render(request, 'dashboard/dashboard_gbq.html', context=context)
