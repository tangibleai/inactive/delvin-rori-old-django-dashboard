SELECT cards.uuid as faq_uuid,
      cards.content as content,
      cards.title as title,
      tags.id as tag_id,
      tags.tag_name as tag_name
  FROM 
  ({{variables.content_table}} as cards
    left join 
  {{variables.content_tag_map}} as faq_tag_map 
   ON cards.uuid=faq_tag_map.faq_uuid
   AND cards.is_deleted is false AND cards.language='fra'
    left join
   {{variables.content_tags}} as tags
   ON faq_tag_map.tag_id = tags.id )
ORDER BY tag_id DESC