import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from dashboard.utils import select_from_supabase
from dashboard.models import MessagesIndicatorsModel
from django.core.exceptions import ObjectDoesNotExist


def calculate_metrics():
    print("Selecting data from Supabase!")

    _db_name = "supabase-db-rori"
    # Query string to select data from Supabase with Django connection
    query = "select sum(message_sent) as total_messages " \
            "from user_engagement_data1;"

    query_result = select_from_supabase(db_name=_db_name, query=query)
    total_messages = query_result[0]["total_messages"]
    print(total_messages)

    query = "select count(contact_uuid) as total_users " \
            "from user_engagement_data1;"

    query_result = select_from_supabase(db_name=_db_name, query=query)
    total_users = query_result[0]["total_users"]
    print(total_users)

    query = "select sum(lifetime) as total_lifetime " \
            "from user_engagement_data1;"

    query_result = select_from_supabase(db_name=_db_name, query=query)
    total_lifetime = query_result[0]["total_lifetime"]
    print(total_lifetime)

    average_messages_per_user = round(total_messages / total_users, 2) if total_users and total_messages else 0
    print(average_messages_per_user)

    average_by_lifetime = round(total_messages / total_lifetime, 2) if total_messages and total_lifetime else 0
    print(average_by_lifetime)

    query = "select sum(active_days) as total_active_days " \
            "from user_engagement_data2;"

    query_result = select_from_supabase(db_name=_db_name, query=query)
    total_active_days = query_result[0]["total_active_days"]
    print(total_active_days)

    average_messages_per_user_per_active_days = round(total_messages / total_active_days, 2) if total_messages and total_active_days else 0
    print(average_messages_per_user_per_active_days)

    query = "select sum(active_weeks) as total_active_weeks " \
            "from user_engagement_data3;"

    query_result = select_from_supabase(db_name=_db_name, query=query)
    total_active_weeks = query_result[0]["total_active_weeks"]
    print(total_active_weeks)

    messages_per_week_per_user = round(total_messages / total_active_weeks, 2) if total_messages and total_active_weeks else 0
    print(messages_per_week_per_user)

    print("Indicator update!")

    indicators = {
        "total_messages": total_messages,
        "all_users_count": total_users,
        "total_lifetime": total_lifetime,
        "average_messages_per_user": average_messages_per_user,
        "average_messages_per_day_per_user": average_by_lifetime,
        "average_messages_per_user_per_active_days": average_messages_per_user_per_active_days,
        "messages_per_week_per_user": messages_per_week_per_user,
    }

    for name_, value_ in indicators.items():
        print(name_, value_)
        try:
            indicator = MessagesIndicatorsModel.objects.get(
                name=name_,
            )
        except ObjectDoesNotExist:
            MessagesIndicatorsModel.objects.create(
                name=name_,
                value=value_
            )
        else:
            indicator.name = name_
            indicator.value = value_
            indicator.save()


if __name__ == "__main__":
    calculate_metrics()
