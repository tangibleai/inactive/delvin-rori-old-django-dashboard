
SELECT
      contact_id
      ,fields
      ,updated_at as contact_updated_at
    FROM
      (
      SELECT
      contact_id
      ,fields
      ,updated_at
      ,ROW_NUMBER() OVER (PARTITION BY uuid ORDER BY updated_at DESC) AS rownumber
      FROM
        `dimagi-turnio-chatbot-111120.243827325289.contact_details`
      )
    WHERE
    rownumber = 1