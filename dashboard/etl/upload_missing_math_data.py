"""Standalone script"""

import os
import time
import pandas as pd

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()

from dashboard.utils import select_from_supabase
from dashboard.etl.utils import etl_mqa_records, get_missing_message_records
from dashboard.models import MathQuestionAnswerModel
from django.db.utils import IntegrityError
from django.forms.models import model_to_dict
from pytz import UTC
import json

BATCH_SIZE=1000
def insert_records_to_supabase_with_model(result):
    # By using subabase query result object, inserts the data to supabase
    if not len(result):
        return 0
    processed_df = etl_mqa_records(result)
    print("Inserting rows to Supabase")

    rows = []
    for i, row in processed_df.iterrows():
        row_dict = dict(row)
        row_model = MathQuestionAnswerModel(**row_dict)
        rows.append(row_model)
        if i % 50 == 0 or i == (len(processed_df) - 1):
            try:
                MathQuestionAnswerModel.objects.bulk_create(rows)
            except IntegrityError:
                print("Duplicate record detected")
                for individual_row in rows:

                    if MathQuestionAnswerModel.objects.filter(
                            original_message_id=individual_row.original_message_id).count() > 0:
                        print(f"Row already exists, id:{individual_row.original_message_id}")
                        continue
                    else:
                        individual_row.save()

            print(f"Inserted: {len(rows)} rows. Time: {time.asctime()}!")
            rows.clear()

    return len(processed_df)


if __name__ == "__main__":
    records = get_missing_message_records(BATCH_SIZE)
    num_rows = insert_records_to_supabase_with_model(records)
    while num_rows > 0:
        records = get_missing_message_records(BATCH_SIZE)
        num_rows = insert_records_to_supabase_with_model(records)
