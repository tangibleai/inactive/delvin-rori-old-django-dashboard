/*
CONTEXT:
- A SESSION is defined as all the messages that one user (defined by chat_id) exchanged with the chatbot within the same calendar day.
RESULT EXPECTATION
A histogram of number of sessions each user (`chat_id`) had with the bot. It should be returned in the following format
- x column should contain the name of the bin (we want the bins 1-2, 3-5, 6-10, >10)
- y should contain the amount of distinct chat_ids that had the appropriate number of sessions.
*/
SELECT
  session_count_buckets
  ,COUNT(num_sessions) num_chats
FROM
-- creating buckets
  (
  SELECT
    num_sessions
    ,CASE WHEN num_sessions >= 1 AND num_sessions <= 2 THEN '1-2'
    WHEN num_sessions >= 3 AND num_sessions <= 5 THEN '3-5'
    WHEN num_sessions >= 6 AND num_sessions <= 10 THEN '6-10'
    WHEN num_sessions > 10 THEN '>10' ELSE NULL
    END AS session_count_buckets
  FROM
  -- counting number of sessions per chat_id
    (
    SELECT
      chat_id
      ,COUNT(session_id) AS num_sessions
    FROM
    -- generating automated id for session_id
      (
      SELECT
       GENERATE_UUID() AS session_id
        ,chat_id
        ,num_messages
        ,first_message_time
        ,last_message_time
      FROM
      -- deducing start time and end time of a session
        (
        SELECT
          chat_id
          ,inserted_date
          ,num_messages
          ,MIN(inserted_at) AS first_message_time
          ,MAX(inserted_at) AS last_message_time
        FROM
          (
          SELECT
            chat_id
            ,DATE(inserted_at) AS inserted_date
            ,COUNT(id) OVER (PARTITION BY chat_id, DATE(inserted_at)) AS num_messages
            ,inserted_at
          FROM
          `sample-turnio-bot-20201103.15550159753.messages`
          )
          GROUP BY chat_id,inserted_date,num_messages
        )
      )
    GROUP BY chat_id
    )
  )
GROUP BY session_count_buckets

/* TRANSFORMATIONS */

return data.map(item => {
	return {
    x: item.session_count_buckets,
    y: item.num_chats     
  }
})
