/* 
CONTEXT: 
- Creates a table that shows how many users accessed each content domain (without those that accessed none)
RESULT EXPECTATION
- Generates a table with two columns: 
  * domain_tag - the domain tag
  * unique_users - number of unique users that accessed content tagged with domain_tag_name
*/
SELECT domain_tag,
       COUNT(distinct chat_id) as unique_users
FROM
  {{variables.tables.messages_augmented}}
WHERE domain_tag is not null
GROUP BY domain_tag