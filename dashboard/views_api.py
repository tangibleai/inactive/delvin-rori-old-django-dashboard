from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from dashboard.metrics.student_progress_metrics import get_student_stats, get_student_details

"""
Django rest framework API endpoints
https://www.django-rest-framework.org/api-guide/authentication/#basicauthentication
"""


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def student_stats(request):
    """
    Returns student stats
    curl -X GET https://hostname/dashboard/student_stats/?uuid=the-uuid -H 'Authorization: Token the-token'
    """
    user_uuid = request.query_params.get('uuid')
    stats = get_student_stats(user_uuid)
    return Response(stats)


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def student_details(request):
    """
    Returns student details
    curl -X GET https://hostname/dashboard/student_details/?uuid=the-uuid -H 'Authorization: Token the-token'
    """
    user_uuid = request.query_params.get('uuid')
    stats = get_student_details(user_uuid)
    return Response(stats)
