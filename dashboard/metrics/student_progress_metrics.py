import os
import time
import pandas as pd
import django
import json

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from django.db.models import Count

from dashboard.models import MathQuestionAnswerModel

import time
def get_student_details(contact_uuid: str):
    '''
    Returns all the math question answers for a student with given BQ contact_uuid.
    '''

    student_records = MathQuestionAnswerModel.objects.filter(contact_uuid=contact_uuid).values(
        'question_level',
        'question_skill',
        'question_topic',
        'question_micro_lesson',
        'question_number',
        'answer_type')

    return list(student_records)

    student_records = pd.DataFrame.from_records(student_records)

def get_student_stats(contact_uuid: str):

    result = {}
    student_records = MathQuestionAnswerModel.objects.filter(contact_uuid=contact_uuid).values(
        'question_level',
        'question_skill',
        'question_topic',
        'question_micro_lesson',
        'question_number',
        'answer_type')
    student_records = pd.DataFrame.from_records(student_records)

    # number of lessons that the student finished
    result["num_lessons"] = student_records['question_micro_lesson'].nunique()
    # num_lessons = MathQuestionAnswerModel.objects.filter(contact_uuid=contact_uuid).values('question_micro_lesson')\
    #     .annotate(Count('question_micro_lesson',distinct=True)).count()
    #
    # orm_time = time.time()
    # print(f"Pandas time:{(pandas_time-start)%60}s\n ORM time: {(orm_time-pandas_time)%60}s")

    # number of questions the student answered correctly
    student_records['microlesson_question'] = student_records['question_micro_lesson'] +'-'+student_records['question_number']

    #number of question
    result["num_correct_answers"] = student_records[student_records.answer_type == 'success']['microlesson_question'].nunique()

    #correctness ratio
    result["correct_percent"] = (student_records[student_records.answer_type == 'success']['microlesson_question'].nunique() /
     (student_records[student_records.answer_type == 'success']['microlesson_question'].nunique()+
     student_records[student_records.answer_type == 'failure']['microlesson_question'].nunique()+
     student_records[student_records.answer_type == 'hint']['microlesson_question'].nunique()))*100

    return result


#sample contact_uuid for experiments
contact_uuid = '356c40b6-ae76-4574-a36c-4d57c05ccbdd'