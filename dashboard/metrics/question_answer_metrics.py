import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

import json
from collections import Counter
from datetime import timedelta, datetime
from dashboard.utils import select_from_supabase, execute_many_on_supabase
from dashboard.models import MathQuestionAnswerModel

import pandas as pd
import time
from pytz import UTC


def calculate_metrics():
    print("Selecting data from Supabase!")

    _db_name = "supabase-db-rori"  # Don't change it unless you know what you are doing!
    # Query string to select data from Supabase with Django connection
    query = "select *" \
            "from message " \
            "where message_inserted_at > '2023-05-16';"

    query_result = select_from_supabase(db_name=_db_name, query=query)

    for row in query_result:
        for field in ['nlu_response', 'request_object']:
            if field in row:
                # Convert the string to a dictionary
                row[field] = json.loads(row[field])

    df = pd.json_normalize(query_result)

    tabular_cols_needed='id,original_message_id,text,message_inserted_at'.split(',')
    nlu_cols_needed = 'nlu_response.data,nlu_response.type,nlu_response.confidence'.split(',')
    new_ro_col_names = ['contact_uuid',
                        'question',
                       'question_level',
                       'question_skill',
                       'question_topic',
                        'question_number',
                       'expected_answer',
                       'question_micro_lesson',
                        'line_name',
                        'line_number']
    ro_cols_needed = ['request_object.'+ str for str in new_ro_col_names]

    #choosing only the cols that we need
    all_cols = tabular_cols_needed + nlu_cols_needed + ro_cols_needed
    df_final = df[all_cols]

    ro_cols_renaming = {}
    for old_name, new_name in zip(ro_cols_needed, new_ro_col_names):
        ro_cols_renaming[old_name] = new_name
    df_final = df_final.rename(columns=ro_cols_renaming)
    df_final = df_final.rename(columns={"id": "supabase_id"})

    new_nlu_cols = [col.replace('.', '__') for col in nlu_cols_needed]
    df_final.rename(columns=dict(zip(nlu_cols_needed, new_nlu_cols)), inplace=True)

    def calc_ans_type(row):
        if (row['nlu_response__type']=='intent'):
            if row['expected_answer']=='Yes' and row['nlu_response__data']=='yes':
                return 'success'
            else:
                return row['nlu_response__data'].lower()
        elif (str(row['nlu_response__data'])==str(row['expected_answer']) or
              str(row['text']) == str(row['expected_answer']) ):
            return 'success'
        else:
            return 'failure'

    df_final['answer_type'] = df_final.apply(calc_ans_type,axis=1)
    df_final.sort_values(by='message_inserted_at',inplace=True)

    rows = []
    for i, row in df_final.iterrows():
        row_dict = dict(row)
        # making timezone aware to avoid warnings.
        row_dict["message_inserted_at"] = row_dict["message_inserted_at"].replace(tzinfo=UTC)
        row_model = MathQuestionAnswerModel(**row_dict)
        rows.append(row_model)
        if i % 1000 == 0:
            MathQuestionAnswerModel.objects.bulk_create(rows)
            print(f"Inserted: {len(rows)} rows. Time: {time.asctime()}!")
            rows.clear()

    query = "delete from math_question_answer;"
    select_from_supabase(db_name=_db_name, query=query)

    # Outdated data creation code. Keeping it here just in case.
    # data_list = df_final.values.tolist()
    # field_values = ['original_message_id',
    #                 'text',
    #                 'message_inserted_at',
    #                 'nlu_response__data',
    #                 'nlu_response__type',
    #                 'nlu_response__confidence',
    #                 'contact_uuid',
    #                 'question',
    #                 'question_level',
    #                 'question_skill',
    #                 'question_topic',
    #                 'question_number',
    #                 'expected_answer',
    #                 'question_micro_lesson',
    #                 'line_name',
    #                 'line_number',
    #                 'answer_type']
    # values_string = '%s, '*len(field_values)
    # values_string = values_string[:-2]
    #
    # query = f"insert into math_question_answer " \
    #         f"({','.join(field_values)}) values ({values_string});"
    # execute_many_on_supabase(
    #     db_name=_db_name,
    #     query=query,
    #     data_list=data_list)



if __name__ == "__main__":
    # From beginning of Rori till today calculate activity metrics
    calculate_metrics()
