import streamlit as st
import numpy as np
import os, dotenv
import plotly.express as px
from datetime import datetime

from streamlit_frontend.src.const import *
from streamlit_frontend.src.etl import (
    get_student_records_by_id,
    get_student_conversation_by_id,
    multiple_user_search
)
from streamlit_frontend.views.components import student_view, conversation_view

fields = [FN_MSG_ID,
        FN_LEVEL,
         FN_SKILL,
        FN_TOPIC,
        FN_LESSON,
        FN_QUESTION_NUM,
        FN_ANSWER_TYPE,
        FN_INSERTED_AT]


metrics = ['num_messages',
           'num_lessons_completed',
           'num_lessons_visited',
           'num_sessions',
           'num_active_days',
           'num_active_weeks',
           ]

def set_session_state():
    if 'contact_uuid' not in st.session_state:
        st.session_state.contact_uuid = ""

    if 'display' not in st.session_state:
        st.session_state.display = ""

    if 'search_results' not in st.session_state:
        st.session_state.search_results = ""

    if 'metric_select' not in st.session_state:
        st.session_state.metric_select = ""

def change_contact_uuid(contact_uuid):
    st.session_state['contact_uuid'] = contact_uuid

def submit_single_student(single_contact_uuid):
    st.session_state['display'] = 'single'
    st.session_state['contact_uuid'] = single_contact_uuid


def submit_multiple_students(var_select, oper_select,num_input,stud_segment_select):
    st.session_state['display'] = 'multiple'
    st.session_state['contact_uuid'] = ""
    st.session_state['search_results'] = multiple_user_search(var_select,
                                                      oper_select,
                                                      num_input,
                                                      stud_segment_select)

def display_student():
    student_records = get_student_records_by_id(fields=fields,
                                                contact_uuid=st.session_state['contact_uuid'])

    student_view.display_component(st.session_state['contact_uuid'], student_records)


def load_page():
    set_session_state()

    with st.expander("Advanced Search", expanded=False):
        with st.form ("Multiple Student Search"):
            stud_segment_select = st.selectbox("Student Segment", MATH_USER_SEGMENTS)

            col_var, col_oper, col_num, _= st.columns([2,1,1,2])

            with col_var:
                var_select = st.selectbox("Students with", metrics)

            with col_oper:
                oper_select = st.selectbox("Condition", [">", "<", "="])

            with col_num:
                num_input = st.text_input("Threshold")

            submit = st.form_submit_button("Search Multiple Students")
            if submit:
                submit_multiple_students(var_select, oper_select,num_input, stud_segment_select)

    st.write("OR")

    with st.form ('Single Student Search'):
        single_contact_uuid = st.text_input("Contact UUID", value='356c40b6-ae76-4574-a36c-4d57c05ccbdd')

        submit = st.form_submit_button("Display a Single Student")

        if submit:
            submit_single_student(single_contact_uuid)

    st.divider()


    if st.session_state['display'] == 'multiple':
        #Display a list of multiple students

        col_list, col_student = st.columns([1,3])

        with col_list:
            if not len(st.session_state['search_results']):
                st.write("### No Students Found With This Criteria")
            for i, row in st.session_state['search_results'].iterrows():
                col_stud_name, col_button = st.columns ([2,2])
                with col_stud_name:
                    st.write(row['contact_uuid'][:5]+'...')

                with col_button:
                    st.button('Open', key=i, on_click=change_contact_uuid, args=[row['contact_uuid']])

                st.divider()

        with col_student:
            if st.session_state['contact_uuid']:
                display_student()

            else:
                st.write("### No student chosen")

    if st.session_state['display'] == 'single':
        display_student()
