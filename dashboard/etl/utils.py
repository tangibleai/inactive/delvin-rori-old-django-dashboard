
import json
import pandas as pd
from pytz import UTC

from dashboard.utils import select_from_supabase

PRODUCTION_DB_NAME = "supabase-db-rori"

FULL_INFO_ROLLOUT_SUPABASE_ID=647988
V3_CONTENT_ROLLOUT_SUPABASE_ID=1249797
V3_ENDPOINT_ROLLOUT_SUPABASE_ID=1276333


def get_max_id(table_name, field):
    """
    Finds the maximum supabase_id in math_question_answer
    """
    print("Finding max_id")

    # Query string to select data from Supabase with Django connection
    query = f"select max({field}) as max_id from {table_name};"
    query_result = select_from_supabase(db_name=PRODUCTION_DB_NAME, query=query)
    max_id = query_result[0]["max_id"] or 0

    print(f"max id: {max_id}")
    return max_id

def get_message_records_batch(batch_size):
    # Fetches Supabase data starting from the max_id_ parameter
    max_id_ = get_max_id(table_name='math_question_answer', field='supabase_id')

    query = f"select * from message where id>{max_id_} " \
            f"order by id " \
            f"limit {batch_size};"
    query_result = select_from_supabase(db_name=PRODUCTION_DB_NAME, query=query)

    for row in query_result:
        for field in ["nlu_response", "request_object"]:
            if field in row:
                # Convert the string to a dictionary
                row[field] = json.loads(row[field])

    df = pd.json_normalize(query_result)
    return df

def get_missing_message_records(batch_size):
    query = f"""
            SELECT t1.id, t1.nlu_response, t1.request_object
              FROM
                message as t1
                LEFT JOIN math_question_answer t2 ON t2.original_message_id = t1.request_object ->> 'message_id'
              WHERE t2.original_message_id IS NULL and t1.request_object::varchar LIKE '%question_number%'
            LIMIT {batch_size}
            """
    query_result = select_from_supabase(db_name=PRODUCTION_DB_NAME, query=query)

    for row in query_result:
        for field in ["nlu_response", "request_object"]:
            if field in row:
                # Convert the string to a dictionary
                row[field] = json.loads(row[field])

    df = pd.json_normalize(query_result)
    return df


def calc_new_question_number(row):
    '''
    Calculates the new question number based on content change on 2023.08.31
    '''
    res = 0
    qn = int(row['question_number'])

    if row['supabase_id'] > V3_CONTENT_ROLLOUT_SUPABASE_ID:
        if qn < 10:
            res = qn - 4
        else:
            res = qn - 6
    else:
        res = qn

    if res < 0:
        res = qn

    if not isinstance(res, int):
        pass

    return res

def calc_ans_type_v2(row):
    if (row["nlu_response_type"] == "intents" or row["nlu_response_type"] == "keyword") :
        if (row["expected_answer"] == "Yes" and row["nlu_response_data"] == "yes") :
            return "success"
        if (row["nlu_response_data"] == 'answer' or row["nlu_response_data"]=="numerical_expression"):
            return "failure"
        else:
            return row["nlu_response_data"].lower()
    elif str(row["nlu_response_data"]) == str(row["expected_answer"]) or str(
        row["text"]) == str(row["expected_answer"]):
        return "success"
    else:
        return "failure"


def calc_ans_type_v3(row):
    if (row["nlu_response_type"] == "intent" or row["nlu_response_type"] == "keyword") :
        if ["nlu_response_data"] == "math_answer":
            return "failure"
        else:
            return row["nlu_response_data"].lower()

    elif row["nlu_response_type"] == "out_of_scope":
        return "out_of_scope"

    elif str(row["nlu_response_data"]) == str(row["expected_answer"]) or str(
        row["text"]) == str(row["expected_answer"]):
        return "success"

    else:
        return "failure"

def calc_answer_type(row):
    if int(row["supabase_id"]) > V3_ENDPOINT_ROLLOUT_SUPABASE_ID:
        return calc_ans_type_v3(row)
    else:
        return calc_ans_type_v2(row)

def select_mqa_cols(message_records_df):
    """
    Selects all necessary columns from the original df and renames them
    """
    tabular_cols_needed = ["id"]
    nlu_cols_needed = [
        "nlu_response.data",
        "nlu_response.type",
        "nlu_response.confidence"
    ]
    new_ro_col_names = [
        "contact_uuid",
        "message_id",
        "message_updated_at",
        "message_body",
        "question",
        "question_level",
        "question_skill",
        "question_topic",
        "question_number",
        "expected_answer",
        "question_micro_lesson",
        "line_name",
        "line_number",
    ]
    ro_cols_needed = ["request_object." + str for str in new_ro_col_names]

    # choosing only the cols that we need
    all_cols = tabular_cols_needed + nlu_cols_needed + ro_cols_needed
    df_final = message_records_df[all_cols]

    hint_col_name = "request_object.hint_shown"
    if hint_col_name in message_records_df.columns:
        df_final["hint_shown"] = message_records_df[hint_col_name]

    ro_cols_renaming = {}
    for old_name, new_name in zip(ro_cols_needed, new_ro_col_names):
        ro_cols_renaming[old_name] = new_name
    df_final = df_final.rename(columns=ro_cols_renaming)
    df_final = df_final.rename(columns={'message_body':'text', "message_id":"original_message_id"})
    df_final = df_final.rename(columns={"id": "supabase_id"})
    df_final["message_inserted_at"] = df_final["message_updated_at"].apply(pd.Timestamp)
    df_final = df_final.drop(columns=["message_updated_at"])

    new_nlu_cols = [col.replace(".", "_") for col in nlu_cols_needed]
    df_final.rename(columns=dict(zip(nlu_cols_needed, new_nlu_cols)), inplace=True)

    return df_final


def remove_junk_mqa_records(df_final):
    df_final.dropna(subset=['question_level', 'question_number', 'question_micro_lesson', 'expected_answer'], inplace=True)
    index_empty = df_final[(df_final['question_level'] == '') |
                           (df_final['question_number'] == '') |
                           (df_final['question_micro_lesson'] == '') |
                           (df_final['expected_answer'] =='') |
                           (df_final['line_name'] == 'runninghill-244') |
                           (df_final['line_name'] == 'Rori Staging') |
                           (df_final['question_micro_lesson'] == 'Microlessons(Lessons)')
                           ].index
    df_clean = df_final.drop(index_empty)

    if len(index_empty):
        print(f"Omitted {len(index_empty)} empty rows")

    df_clean = df_clean.reset_index(drop=True)

    return df_clean



def etl_mqa_records(message_records_df):
    df_final = select_mqa_cols(message_records_df)
    # data validation - droping rows continuing NAs and empty strings
    df_final = remove_junk_mqa_records(df_final)

    if not len(df_final):
        return df_final

    #calculate the type of the answer
    df_final['new_question_number'] = df_final.apply(lambda row: calc_new_question_number(row), axis=1)
    df_final['question_number'] = df_final['new_question_number']
    df_final = df_final.drop(columns=["new_question_number"])
    df_final["answer_type"] = df_final.apply(calc_answer_type, axis=1)

    df_final.sort_values(by="message_inserted_at", inplace=True)
    return df_final


if __name__=="__main__":
    query="""
            select
  m.contact_uuid as contact_uuid,
  m.message_text as student_input,
  m.next_message_text as rori_generative_response
from
  (SELECT original_message_id from math_question_answer where nlu_response_type = 'out_of_scope') a
  inner join
  (SELECT original_message_id,
        contact_uuid,
        message_text,
        lead(message_text) over (
          partition by
            contact_uuid
          order by
            id
        ) as next_message_text
    FROM bq_messages
    WHERE id>569171224
  ) m
  on m.original_message_id = a.original_message_id
          """

    result=select_from_supabase(db_name=PRODUCTION_DB_NAME, query=query)
    df = pd.DataFrame.from_records(result)
    df.to_csv("generative_responses.csv")