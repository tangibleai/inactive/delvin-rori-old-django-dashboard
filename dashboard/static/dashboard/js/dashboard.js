let datepicker = document.getElementById("start_datepicker");
const csrftoken = document.querySelector('input[name="csrfmiddlewaretoken"]').value;
let load_statistics_inscription = document.getElementById("load-inscription");
const base_url = `${window.location.protocol}//${window.location.hostname}:${document.body.getAttribute('data-port')}`;

function updateStatisticNumbers(indicators) {
    let statistic_numbers_container = document.querySelector(".numbers-data");
    let ul_elem = statistic_numbers_container.querySelectorAll('ul');
    if (ul_elem.length == 0) {
        ul_elem = document.createElement("ul");
        for(let i = 0; i < indicators.length; i++) {
            let li_elem = document.createElement("li");
            let div_elem = document.createElement("div");
            let p_inscription = document.createElement("p");
            p_inscription.classList.add("inscription");
            p_inscription.innerText = indicators[i].label;
            let p_numbers = document.createElement("p");
            p_numbers.innerText = indicators[i].value;
            p_numbers.classList.add("numbers");
            div_elem.appendChild(p_inscription);
            div_elem.appendChild(p_numbers);
            li_elem.appendChild(div_elem);
            ul_elem.appendChild(li_elem);
        }
        statistic_numbers_container.appendChild(ul_elem);
    }
}

fetch(`${base_url}/dashboard/context/`, {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    "X-CSRFToken": csrftoken
  }
})
.then(response => {
  return response.json();
})
.then(data => {
    load_statistics_inscription.style.display = "none";
    updateStatisticNumbers(data.indicators);

    let chart_container = document.getElementById("charts");
    for(let i = 0; i < 6; i++) {
        let chart_div = document.createElement("div");
        chart_div.classList.add('card');
        let chart_canvas = document.createElement("canvas");
        chart_canvas.classList.add("chart");
        chart_div.appendChild(chart_canvas);
        chart_container.appendChild(chart_div);
    }

    let crtx = document.getElementsByClassName('chart');

    Array.from(crtx).forEach((chart, i) => {
        
        let newChart = new Chart(chart, {
        type: 'line',
        data: {
            labels: data['charts'][i].labels,
            datasets: [{
                data: data['charts'][i].data,
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.2
            }]
        },
        options: {
            backgroundColor: 'red',
            plugins: {
                tooltip: {
                    backgroundColor: "#90EE90",
                    titleFont: {
                        size: 15
                    },
                    bodyFont: {
                        size: 15
                    },
                    titleColor: "black",
                    bodyColor: "black",
                    callbacks: {
                            label: function(context) {
                                return 'Message Count: ' + context.parsed.y;
                            },
                            title: function(context) {
                                return "Date: " + context[0].label;
                            }
                        }
                },
                background: {
                    color: 'lightBlue'
                },
                title: {
                    display: true,
                    align: 'start',
                    text: data['charts'][i].title,
                    font: {
                        size: 20
                    },
                    padding: {
                        bottom: 50
                    }
                },
                legend: {
                    display: false
                }
            },
            scales: {
                x: {
                    title: {
                        display: true,
                        padding: 10,
                        text: data['charts'][i].xlabel,
                        font: {
                            size: 20
                        }
                    },
                    ticks: {
                        display: true,
                        font: {
                            size: 15
                        }
                    }
                },
                y: {
                    title: {
                        display: true,
                        text: data['charts'][i].ylabel,
                        padding: 25,
                        font: {
                            size: 20
                        }
                    },
                    ticks: {
                        display: true,
                        font: {
                            size: 15
                        }
                    }
                }
            }
        }
        });
        newChart.update();
    });
})


const fp = flatpickr("#start_datepicker", {
    mode: "range",
    dateFormat: "Y-m-d",
    onClose: function(selectedDates) {
        const startDate = selectedDates[0];
        const stopDate = selectedDates[1];
        const options = {year: 'numeric', month: 'long', day: 'numeric' };
        const monthOrder = {
            'January': '01',
            'February': '02',
            'March': '03',
            'April': '04',
            'May': '05',
            'June': '06',
            'July': '07',
            'August': '08',
            'September': '09',
            'October': '10',
            'November': '11',
            'December': '12'
        };
        let localStartDateString = startDate.toLocaleDateString('en-US', options);
        const startDateParts = localStartDateString.split(" ");
        let startYear = startDateParts[2];
        let startMonth = monthOrder[startDateParts[0]];
        let startDay = startDateParts[1].split(",")[0];
        startDay = parseInt(startDay) < 10 ? "0" + startDay : startDay;
        let localStopDateString = stopDate.toLocaleDateString('en-US', options);
        const stopDateParts = localStopDateString.split(" ");
        let endYear = stopDateParts[2];
        let endMonth = monthOrder[stopDateParts[0]];
        let endDay = stopDateParts[1].split(",")[0];
        endDay = parseInt(endDay) < 10 ? "0" + endDay : endDay;

        const data = {
            startDate: startYear + "-" + startMonth + "-" + startDay,
            stopDate: endYear + "-" + endMonth + "-" + endDay
        };
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                const response = JSON.parse(xhr.responseText);
                updateStatisticNumbers(response.indicators);
                // Reload the page once the request is completed successfully
                let charts = document.getElementsByClassName('chart');

                Array.from(charts).forEach((chart, i) => {
                    const chartToUpdate = Chart.getChart(chart);
                    chartToUpdate.data.labels = response['charts'][i].labels;
                    chartToUpdate.data.datasets[0].data = response['charts'][i].data;
                    chartToUpdate.update();
                })
            }
            load_statistics_inscription.style.display = "none";
        }
        xhr.open('POST', `${base_url}/dashboard/context/`);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
        xhr.send(JSON.stringify(data));
        load_statistics_inscription.style.display = "block";
    }
});

datepicker.addEventListener("mouseover", () => {
    fp.open();
})
