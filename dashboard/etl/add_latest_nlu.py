import pandas as pd
import os
import time

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()

from dashboard.utils import select_from_supabase
from dashboard.models import MathQuestionAnswerModel
from mathtext.predict_intent import predict_message_intent


supabase_db_name = "supabase-db-rori"
query = f"""
        select original_message_id, text, nlu_response_data, nlu_response_confidence from math_question_answer
        where nlu_response_type='intent';
        """
query_result = select_from_supabase(db_name=supabase_db_name, query=query)

df = pd.DataFrame.from_records(query_result)
df['raw_nlu_output'] = df['text'].apply(lambda x: predict_message_intent(x))
df_nlu_output = pd.json_normalize(df['raw_nlu_output'])
df['latest_nlu_response_data'] = df_nlu_output['data']
df['latest_nlu_response_confidence'] = df_nlu_output['confidence']
df = df.drop(columns=['raw_nlu_output'])

rows = []
for i, row in df.iterrows():
        row_dict = dict(row)
        row_model = MathQuestionAnswerModel(**row_dict)
        rows.append(row_model)
        if i % 5000 == 0 or i == (len(df) - 1):
                MathQuestionAnswerModel.objects.bulk_update(rows, ['latest_nlu_response_data','latest_nlu_response_confidence' ])
                print(f"Updated: {i} rows. Time: {time.asctime()}!")
                rows.clear()
