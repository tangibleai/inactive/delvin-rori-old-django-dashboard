#! /bin/bash
set -e

echo "Starting creation and calculations" > metrics.log
date >> metrics.log
echo >> metrics.log

# Fetching base data from Supabase, calculating and pushing results to Supabase
echo "Updating data on Supabase!"
echo dashboard.etl.create_ds_data_on_supabase
echo $(python -m dashboard.etl.create_ds_data_on_supabase && echo "Supabase user data created successfully!" || echo "Error while creating Supabase user data!") >> metrics.log
echo >> metrics.log
sleep 3

echo "Updating math data on Supabase!"
echo dashboard.etl.create_math_qa_data_on_supabase
echo $(python -m dashboard.etl.create_math_qa_data_on_supabase && echo "Supabase math data created successfully!" || echo "Error while creating Supabase math data!") >> metrics.log
echo >> metrics.log
sleep 3

echo "Uploading missing math data to Supabase"
echo dashboard.etl.upload_missing_math_data
echo $(python -m dashboard.etl.upload_missing_math_data && echo "Supabase math data created successfully!" || echo "Error while uploading missing math data!") >> metrics.log
echo >> metrics.log
sleep 3


echo "Updating full message data on Supabase!"
echo dashboard.etl.create_full_message_data_on_supabase
echo $(python -m dashboard.etl.create_full_message_data_on_supabase && echo "Full message data created successfully!" || echo "Error while creating Supabase full message data!") >> metrics.log
echo >> metrics.log
sleep 3

echo "Updating student data on Supabase!"
echo dashboard.etl.create_student_data_on_supabase
echo $(python -m dashboard.etl.create_student_data_on_supabase && echo "Student data created successfully!" || echo "Error while creating Supabase student data!") >> metrics.log
echo >> metrics.log
sleep 3

echo "Updating student metrics on Supabase!"
echo dashboard.metrics.calculate_user_features
echo $(python -m dashboard.metrics.calculate_user_features && echo "Student Metrics created successfully!" || echo "Error while creating Supabase student data!") >> metrics.log
echo >> metrics.log
sleep 3

echo dashboard.metrics.active_user_metrics1
echo $(python -m dashboard.metrics.active_user_metrics1 && echo "Active user metrics 1 created successfully!" || echo "Error while creating active user metrics 1!")  >> metrics.log
echo >> metrics.log
sleep 3

echo dashboard.metrics.active_user_metrics2
echo $(python -m dashboard.metrics.active_user_metrics2 && echo "Active user metrics 2 created successfully!" || echo "Error while creating active user metrics 2!") >> metrics.log
echo >> metrics.log
sleep 3

echo dashboard.metrics.user_lifetime_metrics
echo $(python -m dashboard.metrics.user_lifetime_metrics && echo "User lifetime metrics created successfully!" || echo "Error while creating user lifetime metrics!") >> metrics.log
echo >> metrics.log
sleep 3

echo dashboard.metrics.user_engagement_metrics1
echo $(python -m dashboard.metrics.user_engagement_metrics1 && echo "User engagement metrics 1 created successfully!" || echo "Error while creating user engagement metrics 1!") >> metrics.log
echo >> metrics.log
sleep 3

echo dashboard.metrics.user_engagement_metrics2
echo $(python -m dashboard.metrics.user_engagement_metrics2 && echo "User engagement metrics 2 created successfully!" || echo "Error while creating user engagement metrics 2!") >> metrics.log
echo >> metrics.log
sleep 3

echo dashboard.metrics.user_engagement_metrics3
echo $(python -m dashboard.metrics.user_engagement_metrics3 && echo "User engagement metrics 3 created successfully!" || echo "Error while creating user engagement metrics 3!") >> metrics.log
echo >> metrics.log
sleep 3

echo dashboard.metrics.indicator_metrics
echo $(python -m dashboard.metrics.indicator_metrics && echo "Indicator metrics created successfully!" || echo "Error while creating indicator metrics!") >> metrics.log
echo >> metrics.log
sleep 3

echo dashboard.metrics.user_engagement_create_png
echo $(python -m dashboard.metrics.user_engagement_create_png && echo "User engagement metrics png created successfully!" || echo "Error while creating user engagement metrics png!") >> metrics.log
echo >> metrics.log
sleep 3

echo "End of the calculations!" >> metrics.log
echo >> metrics.log

exit 0
