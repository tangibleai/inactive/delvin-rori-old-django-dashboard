/* 
CONTEXT: 
- Generates breakdown of users by gender
RESULT EXPECTATION
- returns 
  x - a value for gender 
  y - number of unique users of this gender
*/

SELECT 
   gender as x,
   COUNT(distinct chat_id) as y
FROM 
	{{variables.tables.messages_augmented}}
WHERE    
	inserted_at > cast('{{variables.state.startDate}}' as datetime) AND
  inserted_at < cast('{{variables.state.endDate}}' as datetime)
GROUP BY 
  gender;