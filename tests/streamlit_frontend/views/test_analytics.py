import pytest
from datetime import date

from streamlit_frontend.src.sql_utils import WhereClause
from streamlit_frontend.views.components.analytics.student_progress import display_student_progress

def test_acquistion():
    with pytest.raises(Exception) as excinfo:
        start_date = date(2023,9,1)
        end_date = date(2023,9,30)
        where_clause = WhereClause()
        where_clause.add_datelimit_subclause('inserted_at', start_date, end_date)
        metric_select = 'num_math_messages'
        segment_select = '4 - Persisted for >5 Math Sessions'
        display_student_progress(metric_select, segment_select, where_clause)

    print(excinfo.type, excinfo.value)
