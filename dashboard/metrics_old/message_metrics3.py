import json
from datetime import datetime

import pandas as pd


def calculate_metrics(file_name, start_date, stop_date):
    df = pd.read_csv(file_name, usecols=["id", "sec_addr", "short_inserted_at"])
    df["short_inserted_at"] = df["short_inserted_at"].apply(
        lambda x: datetime.strptime(x, "%Y-%m-%d").date()
    )

    all_students = []

    mask = (df["short_inserted_at"] > start_date.date()) & (df["short_inserted_at"] <= stop_date.date())
    all_df = df[mask]
    agg_addr_df = all_df.groupby(['sec_addr', 'short_inserted_at']).agg({'id': ['count']}).reset_index()

    if agg_addr_df.empty:
        return {
            'total_messages': 0,
            'total_students': 0,
            'total_active_days': 0,
            'average_by_students': 0,
            'average_by_days': 0,
            'final_df': pd.DataFrame(
                [],
                columns=["Student", "Message sent", "Active days", "Average messages"]
            ),
            'all_messages_df': pd.DataFrame([], columns=["Message"]),
        }

    # all_messages = []

    for student in agg_addr_df[('sec_addr', '')].unique():
        active_days = agg_addr_df[('short_inserted_at', '')][agg_addr_df[('sec_addr', '')] == student].count()
        messages = agg_addr_df[('id', 'count')][agg_addr_df[('sec_addr', '')] == student].to_list()
        message_sent = sum(messages)
        # all_messages.extend(messages)
        average_messages = round(message_sent / active_days, 2) if active_days else 0
        all_students.append({
            "Student": student,
            "Message sent": message_sent,
            "Active days": active_days,
            "Average messages": average_messages,
        })

    final_df = pd.DataFrame.from_records(all_students)
    total_messages = final_df['Message sent'].sum()
    total_students = final_df['Student'].count()
    total_active_days = final_df['Active days'].sum()
    average_by_students = total_messages / total_students if total_students else 0
    average_by_days = total_messages / total_active_days if total_active_days else 0

    final_df.to_csv("data/message_metrics3.csv", index=False),

    indicators = {
        "total_messages": f"{total_messages:,}",
        "total_students": f"{total_students:,}",
        "total_active_days": f"{total_active_days:,}",
        "average_by_students": f"{average_by_students:,.2f}",
        "average_by_days": f"{average_by_days:,.2f}",
    }

    with open('data/message_metrics3_indicators.json', 'w') as file:
        json.dump(indicators, file)


if __name__ == "__main__":
    start_date_ = datetime(2022, 8, 1)
    stop_date_ = datetime.now()
    calculate_metrics(
        file_name="data/lifetime.csv",
        start_date=start_date_,
        stop_date=stop_date_,
    )
