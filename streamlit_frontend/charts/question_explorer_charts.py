import pandas as pd
import numpy as np

import plotly.express as px

from streamlit_frontend.src.const import *
from streamlit_frontend.src.etl import get_math_data_with_field_constraint


def question_answer_distribution(lesson_df):
    '''
    Plots distribution of answers for every question in the lesson as a stacked bar chart
    :return:
    '''
    q_dist_df = (
        lesson_df.groupby([FN_QUESTION_NUM, FN_ANSWER_TYPE])
        .agg({FN_MSG_ID: "nunique"})
        .reset_index()
        .rename(columns={FN_MSG_ID: "count"})
        .sort_values(by=[FN_QUESTION_NUM])
    )
    fig = px.bar(
        q_dist_df,
        x=FN_QUESTION_NUM,
        y="count",
        color=FN_ANSWER_TYPE,
        title="Answers composition per question",
    )
    fig.update_xaxes(categoryorder="category ascending")
    fig.update_xaxes(tickmode="linear")
    return fig

def get_success_type(group):
    count = group.shape[0]
    has_success = (group['answer_type'] == 'success').any()
    has_failure = (group['answer_type'] == 'failure').any()
    has_hint = (group['hint_shown'] == 'true').any()
    no_hint = (group['hint_shown'] == 'false').all()

    if count == 1 and has_success:
        return "1st attempt success"
    elif count == 2 and has_success:
        if has_hint:
            return "2nd attempt success - with hint"
        if no_hint:
            return "2nd attempt success - no hint"
        return "2nd attempt success"
    elif count == 3 and has_success:
        return "3rd attempt success"
    elif not has_success and has_failure:
        if has_hint:
            return "failure - with hint"
        return "failure"
    else:
        return "other"

def lesson_question_corr_by_attempt(lesson_df):

    lesson_df =  (lesson_df
                     .groupby(['contact_uuid', 'question_number'])
                     .apply(get_success_type)
                     .reset_index()
                     .rename(columns={0:"success_type"}))
    agg = lesson_df.groupby('question_number')['success_type'].value_counts().reset_index(name='count')

    color_map = {
        "1st attempt success": "deepskyblue",
        "2nd attempt success": "dodgerblue",
        "3rd attempt success": "lightblue",
        "2nd attempt success - with hint": "aliceblue",
        "2nd attempt success - no hint": "lightsteelblue",
        "failure": "red",
        "failure - with hint": "pink",
        "other": "gray"
    }

    fig = px.bar(agg, x='question_number', y='count', color='success_type',
                  labels={'count': 'Number of occurrences'},
                 color_discrete_map=color_map,
                 category_orders={"success_type":(list(color_map.keys()))},
                 barmode='stack')
    return fig


if __name__ == "__main__":
    lesson_df = get_math_data_with_field_constraint('question_micro_lesson', 'G1.N1.1.1.1')