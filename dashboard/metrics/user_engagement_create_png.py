import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from dashboard.utils import select_from_supabase
from io import BytesIO
import base64
from dashboard.models import MessagesIndicatorsModel
from django.core.exceptions import ObjectDoesNotExist
import json


def calculate_metrics():
    print("Selecting data from Supabase!")

    _db_name = "supabase-db-rori"
    query = "select contact_uuid, weekly_messages from user_engagement_data3"

    query_result = select_from_supabase(db_name=_db_name, query=query)

    print("Starting calculations!")

    df = pd.DataFrame.from_records(query_result)

    df.columns = [item.replace("_", " ").capitalize() for item in df.columns]

    df["Weekly messages"] = df["Weekly messages"].apply(lambda z: json.loads(z))

    # Calculating the length of the data weeks to generate x axis.
    # It should be 25 if not changed on the metrics module

    length_x = len(df.iloc[0]["Weekly messages"])
    x = list(range(length_x))
    y_min = 0
    y_max = 501
    y_step = 50
    x_lim = 25
    y_lim = 500

    # Each student has a weekly message count data.
    # This module draws a line for each student on the same chart.

    plt.figure(figsize=(12, 4))
    plt.yticks(np.arange(y_min, y_max, y_step))
    plt.title('Messages per student per week (each line is a student)', fontsize=12)
    plt.xlabel('Week', fontsize=12)
    plt.ylabel('Message', fontsize=12)
    ax = plt.gca()
    ax.set_xlim([0, x_lim])
    ax.set_ylim([0, y_lim])

    # Iterating over the dataframe to plot the chart
    for index, row in df.iterrows():
        plt.plot(
            x,
            row["Weekly messages"],
            color="blue",
            alpha=0.05,
        )

    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    image_png = buffer.getvalue()
    image_data = base64.b64encode(image_png).decode('utf-8')
    buffer.close()

    print("Indicator update!")

    try:
        indicator = MessagesIndicatorsModel.objects.get(
            name="messages_per_student_per_week_image",
        )
    except ObjectDoesNotExist:
        MessagesIndicatorsModel.objects.create(
            name="messages_per_student_per_week_image",
            image=image_data
        )
    else:
        indicator.image = image_data
        indicator.save()


if __name__ == "__main__":
    calculate_metrics()
