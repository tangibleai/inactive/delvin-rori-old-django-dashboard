import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from datetime import timedelta, datetime
from dashboard.utils import select_from_supabase
import pandas as pd
import time
from dashboard.models import LifetimeUserModel


def calculate_metrics(start_date, stop_date, delta):
    print("Selecting data from Supabase!")

    _db_name = "supabase-db-rori"
    # Query string to select data from Supabase with Django connection
    query = "select contact_uuid, date(inserted_at) as date_inserted_at " \
            "from django_delvin_messages " \
            "group by contact_uuid, date(inserted_at) " \
            "order by contact_uuid;"

    query_result = select_from_supabase(db_name=_db_name, query=query)

    df = pd.DataFrame.from_records(query_result)

    print("Starting calculations!")

    active_users = []
    dormant_users = []

    mask = (df["date_inserted_at"] > start_date.date()) & (df["date_inserted_at"] <= stop_date.date())
    all_df = df[mask]
    agg_all_df = all_df.groupby('contact_uuid').agg({'date_inserted_at': ['min', 'max']})

    agg_all_df["Lifetime"] = agg_all_df.apply(
        lambda row: (row[("date_inserted_at", "max")] - row[("date_inserted_at", "min")]).days + 1,
        axis=1
    )

    date_range_start = stop_date - timedelta(days=delta)
    mask = (df["date_inserted_at"] > date_range_start.date()) & (df["date_inserted_at"] <= stop_date.date())
    windows_df = df[mask]
    window_users = windows_df["contact_uuid"].unique()

    for user in agg_all_df.index.to_list():
        lifetime = int(agg_all_df.loc[user, "Lifetime"].values[0])
        if user in window_users:
            active_users.append(
                {"user_lifetime_in_days": lifetime, "user_type": "active_users"}
            )
        if user not in window_users:
            dormant_users.append(
                {"user_lifetime_in_days": lifetime, "user_type": "dormant_users"}
            )

    print("Deleting data!")
    # Delete all old data
    query = f"delete from lifetime_user_data;"
    select_from_supabase(db_name=_db_name, query=query)

    for data_list in [active_users, dormant_users]:
        print("Inserting data!")
        slice_size = 1000
        while data_list:
            part, data_list = data_list[:slice_size], data_list[slice_size:]
            part_to_models = [LifetimeUserModel(**row) for row in part]
            LifetimeUserModel.objects.bulk_create(part_to_models)
            print(f"Inserted {len(part)} rows. Time: {time.asctime()}!")


if __name__ == "__main__":
    start_date_ = datetime(2022, 8, 1)
    stop_date_ = datetime.now()
    delta_ = 30  # start from 30 days back for calculation
    calculate_metrics(
        start_date=start_date_,
        stop_date=stop_date_,
        delta=delta_,
    )
