import importlib
import pandas as pd
from datetime import datetime
import time
import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()

from django.db.utils import IntegrityError
from dashboard.models import StudentMetricsModel
import dashboard.metrics.user_features as uf
from dashboard.utils import select_from_supabase, PROD_SUPABASE


module_name = 'dashboard.metrics.user_features'
# Import the module
module = importlib.import_module(module_name)

#Primary features calculated from BigQuery data
primary_features_bq = [
    'num_math_messages',
    'num_messages',
    'num_active_days',
    'num_active_weeks',
    'num_sessions',
    'onboarding_stage',
    'last_activity'
]

#Primary features calculated from Supabase math data
primary_features_sb = [
    'num_lessons_visited',
    'num_lessons_completed',
    'average_accuracy_rate'
]

secondary_features = [
    'user_segment'
]

def calculate_features():
    base_table = uf.select_from_users_table(['inserted_at'],
                                            inserted_after=datetime(2023, 1, 13))

    for feature_name in primary_features_bq:
        func_name = feature_name + '_feature'
        print(f"Calculating {feature_name}")
        func = getattr(module, func_name)
        feature_df = func()
        if 'num' in feature_name:
            feature_df[feature_name] = feature_df[feature_name].apply(int)
        base_table = pd.merge(base_table, feature_df, on='contact_uuid', how='left')

    for feature_name in primary_features_sb:
        func_name = feature_name + '_feature'
        print(f"Calculating {feature_name}")
        func = getattr(module, func_name)
        feature_df = func()
        if 'num' in feature_name:
            feature_df[feature_name] = feature_df[feature_name].apply(int).astype(object)
        base_table = pd.merge(base_table, feature_df, on='contact_uuid', how='left')
        mask = base_table['inserted_at'] < '2023-05-15'
        base_table.loc[mask, feature_name] = None

    for feature_name in secondary_features:
        func_name = feature_name + '_sec_feature'
        func = getattr(module, func_name)
        print(f"Calculating {feature_name}")
        feature_df = func(base_table)
        base_table = pd.merge(base_table, feature_df, on='contact_uuid', how='left')

    base_table = base_table.astype(object).where(base_table.notna(), None)

    return base_table


def erase_features_table():
    query = "DELETE from student_metrics"
    res = select_from_supabase(PROD_SUPABASE, query)

def save_features(feature_table):
    rows = []
    print("Inserting rows into supabase")

    for i, row in feature_table.iterrows():
        row_dict = dict(row)
        row_model = StudentMetricsModel(**row_dict)
        rows.append(row_model)

        if i % 1000 == 0 or i == (len(feature_table) - 1):
            try:
                StudentMetricsModel.objects.bulk_create(rows)
            # if there are duplicate rows
            except IntegrityError:
                for individual_row in rows:
                    try:
                        individual_row.save()
                    except IntegrityError:
                        continue

            print(f"Inserted: {len(rows)} rows. Time: {time.asctime()}!")
            rows.clear()


if __name__=="__main__":
    features = calculate_features()
    erase_features_table()
    save_features(features)