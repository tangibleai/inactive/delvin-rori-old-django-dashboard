"""Standalone script"""

import os
import time

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from google.cloud import bigquery
from dashboard.utils import select_from_supabase, execute_many_on_supabase
from dashboard.models import MessagesModel
from pytz import UTC


class SupabaseETL:
    def __init__(self):
        self.supabase_db_name = 'supabase-db-rori'

    def get_max_id_from_supabase(self):
        print("Selecting data from Supabase!")

        # Query string to select data from Supabase with Django connection
        query = "select max(id) as max_id " \
                "from django_delvin_messages;"

        query_result = select_from_supabase(db_name=self.supabase_db_name, query=query)

        max_id = query_result[0]["max_id"] or 0

        print(f"max id: {max_id}")
        return max_id

    def get_records_from_gbq(self):
        # Fetches BQG data starting from the id_ parameter

        sql_query = """
        SELECT
        id, contact_uuid, max(inserted_at) as inserted_at
        FROM
        (
                SELECT id, contact_uuid, inserted_at FROM `rori-turn.12065906259.messages` as messages
                LEFT JOIN
                (
                SELECT chat_id, contacts.uuid as contact_uuid FROM
                        (SELECT
                                id as chat_id,
                                contact_id as contact_id_1,
                                max(inserted_at)
                                FROM `rori-turn.12065906259.chats`
                                group by id, contact_id) as chats
                        INNER JOIN
                                (
                                SELECT
                                id as contact_id_2,
                                uuid,
                                max(inserted_at)
                                from `rori-turn.12065906259.contacts`
                                group by id, uuid
                                ) as contacts
                        on chats.contact_id_1=contacts.contact_id_2
                ) as contact_uuids
                on messages.chat_id = contact_uuids.chat_id
                where direction='inbound'  and id > @id_
        )
        GROUP by id, contact_uuid
        ORDER by id
        """

        id_ = self.get_max_id_from_supabase()

        client = bigquery.Client()

        query_parameters = [
            bigquery.ScalarQueryParameter("id_", "INTEGER", id_),
        ]

        job_config = bigquery.QueryJobConfig(
            query_parameters=query_parameters,
        )

        query_job = client.query(sql_query, job_config=job_config)

        result = query_job.result()

        message = f"{query_job.total_bytes_processed // 1_000_000} MB processed!"
        print(f"GBQ message: {message}")

        return result
    def insert_records_to_supabase_with_model(self):
        # By using GBQ query result object, inserts the data to supabase
        result = self.get_records_from_gbq()

        print("Inserting rows to Supabase")

        rows = []
        for i, row in enumerate(result):
            row_dict = dict(row)
            # making timezone aware to avoid warnings.
            row_dict["inserted_at"] = row_dict["inserted_at"].replace(tzinfo=UTC)
            row_model = MessagesModel(**row_dict)
            rows.append(row_model)
            if i % 5000 == 0:
                MessagesModel.objects.bulk_create(rows)
                print(f"Inserted: {len(rows)} rows. Time: {time.asctime()}!")
                rows.clear()
        MessagesModel.objects.bulk_create(rows)
        print(f"Inserted: {len(rows)} rows. Time: {time.asctime()}!")
        rows.clear()


if __name__ == "__main__":
    s = SupabaseETL()
    s.insert_records_to_supabase_with_model()
