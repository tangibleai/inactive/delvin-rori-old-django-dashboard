"""Standalone script"""

import os
import time

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from google.cloud import bigquery
from dashboard.utils import select_from_supabase, execute_many_on_supabase
from dashboard.etl.utils import get_max_id
from dashboard.models import BQMessageModel, BQMessageModelRising
from pytz import UTC
import json
from google.oauth2 import service_account

RORI_OPEN_LINE=12065906259
RORI_RISING_LINE=12062587201

def get_records_from_gbq(line_number):

    sql_query = f"""
        SELECT
        id, 
        original_message_id,
        contact_uuid, 
        max(inserted_at) as inserted_at, 
        direction, 
        message_text, 
        flow_name
        FROM
        (
                SELECT 
                    messages.id as id, 
                    external_id as original_message_id,
                    contacts.uuid as contact_uuid,
                    messages.inserted_at as inserted_at, 
                    direction, 
                    case
                        WHEN messages.content is not null then messages.content
                        -- there is no other information to extract about non-text messages
                        WHEN messages.content is null and messages.message_type in ('voice', 'image', 'video', 'ephemeral', 'contacts', 'document', 'audio', 'sticker') then  CONCAT('{{', messages.message_type, '}}')
                        WHEN messages.content is null and messages.button_reply_title is not NULL then messages.button_reply_title
                        WHEN messages.content is null and messages.list_reply_title is not NULL then messages.list_reply_title
                        WHEN messages.content is null and messages.interactive is not null and messages.interactive!='null' then REGEXP_EXTRACT(messages.interactive,'"text":"([^\"]+)\"' )
                end as message_text,
                IF(messages.author_type='STACK', REGEXP_EXTRACT(messages.author,'"name":"([^\"]+)\"' ), null) as flow_name 
                FROM 
                    `rori-turn.{line_number}.messages` as messages
                INNER JOIN `rori-turn.{line_number}.chats` as chats
                ON messages.chat_id = chats.id
                INNER JOIN `rori-turn.{line_number}.contacts` as contacts
                ON chats.contact_id = contacts.id
                where messages.id > @id_
        )
        GROUP by id, original_message_id, contact_uuid, direction, message_text, flow_name
        ORDER by id
        """

    table_name = 'bq_messages' if line_number==RORI_OPEN_LINE else 'bq_messages_rising'

    id_ = get_max_id(table_name, 'id')

    client = bigquery.Client()
    query_parameters = [
        bigquery.ScalarQueryParameter("id_", "INTEGER", id_),
    ]

    job_config = bigquery.QueryJobConfig(
        query_parameters=query_parameters,
    )
    print("Pulling data from BigQuery")
    query_job = client.query(sql_query, job_config=job_config)

    result = query_job.result()

    return result

def insert_records_to_supabase_with_model(line_number):
    # By using GBQ query result object, inserts the data to supabase
    result = get_records_from_gbq(line_number)

    print("Inserting rows to Supabase")

    rows = []
    for i, row in enumerate(result):
        row_dict = dict(row)
        # making timezone aware to avoid warnings.
        row_dict["inserted_at"] = row_dict["inserted_at"].replace(tzinfo=UTC)
        if line_number == RORI_OPEN_LINE:
            row_model = BQMessageModel(**row_dict)
        else:
            row_model = BQMessageModelRising(**row_dict)
        rows.append(row_model)
        if i % 5000 == 0:
            if line_number == RORI_OPEN_LINE:
                BQMessageModel.objects.bulk_create(rows)
            else:
                BQMessageModelRising.objects.bulk_create(rows)
            print(f"Inserted: {len(rows)} rows. Time: {time.asctime()}!")
            rows.clear()

    if line_number == RORI_OPEN_LINE:
        BQMessageModel.objects.bulk_create(rows)
    else:
        BQMessageModelRising.objects.bulk_create(rows)
    print(f"Inserted: {len(rows)} rows. Time: {time.asctime()}!")
    rows.clear()


if __name__ == "__main__":
    insert_records_to_supabase_with_model(RORI_OPEN_LINE)
    insert_records_to_supabase_with_model(RORI_RISING_LINE)
