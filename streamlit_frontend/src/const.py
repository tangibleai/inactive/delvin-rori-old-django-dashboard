import os, dotenv
dotenv.load_dotenv()


SB_DB_NAME = "supabase-db-rori"


FN_MSG_ID = "original_message_id"
FN_MSG_TEXT = "text"
FN_QUESTION_TEXT = "question"
FN_LESSON = "question_micro_lesson"
FN_QUESTION_NUM = "question_number"
FN_ANSWER_TYPE = "answer_type"
FN_USER_ID = "contact_uuid"
FN_EXPECTED_ANS = "expected_answer"
FN_LEVEL = "question_level"
FN_TOPIC = "question_topic"
FN_SKILL = "question_skill"
FN_INSERTED_AT = "message_inserted_at"
FN_LINE_NUM = "line_number"
FN_HINT_SHOWN = "hint_shown"

QUESTION_EXPLORER_FIELDS = [FN_MSG_ID,
                            FN_MSG_TEXT,
                            FN_LESSON,
                            FN_QUESTION_NUM,
                            FN_ANSWER_TYPE,
                            FN_USER_ID,
                            FN_LEVEL,
                            FN_TOPIC,
                            FN_SKILL,
                            FN_INSERTED_AT,
                            FN_LINE_NUM,
                            FN_HINT_SHOWN]




ALL_USER_SEGMENTS = [
    '1a - Subscribed',
    '1b - Started Registration',
    '1c - Started Onboarding',
    '1d - Finished Onboarding',
    '2 - Engaged in 1 Math Session',
    '3 - Returned for 2-5 Math Sessions',
    '4 - Persisted for >5 Math Sessions'
]

MATH_USER_SEGMENTS = [
    '2 - Engaged in 1 Math Session',
    '3 - Returned for 2-5 Math Sessions',
    '4 - Persisted for >5 Math Sessions'
]

RORI_RISING_LINE = 12062587201
RORI_OPEN_LINE = 12065906259

CONVERSATIONS_INDEX = 3

DOMAIN = os.environ.get("DOMAIN")