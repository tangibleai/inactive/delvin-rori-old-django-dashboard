import pandas as pd

from dashboard.utils import draw_histogram_plotly, select_from_supabase


def get_context_by_dates(start_date, stop_date):
    _db_name = "supabase-db-rori"

    # Get metrics from Supabase
    total_messages = select_from_supabase(
        db_name=_db_name,
        query="select value from django_delvin_messages_indicators where name = 'total_messages';"
    )[0]["value"]

    all_users_count = select_from_supabase(
        db_name=_db_name,
        query="select value from django_delvin_messages_indicators where name = 'all_users_count';"
    )[0]["value"]

    average_messages_per_user = select_from_supabase(
        db_name=_db_name,
        query="select value from django_delvin_messages_indicators where name = 'average_messages_per_user';"
    )[0]["value"]

    average_messages_per_day_per_user = select_from_supabase(
        db_name=_db_name,
        query="select value from django_delvin_messages_indicators where name = 'average_messages_per_day_per_user';"
    )[0]["value"]

    average_messages_per_user_per_active_days = select_from_supabase(
        db_name=_db_name,
        query="select value from django_delvin_messages_indicators where name = 'average_messages_per_user_per_active_days';"
    )[0]["value"]

    messages_per_week_per_user = select_from_supabase(
        db_name=_db_name,
        query="select value from django_delvin_messages_indicators where name = 'messages_per_week_per_user';"
    )[0]["value"]

    messages_per_student_per_week_image = select_from_supabase(
        db_name=_db_name,
        query="select image from django_delvin_messages_indicators where name = 'messages_per_student_per_week_image';"
    )[0]["image"]

    df1 = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select * from user_engagement_data1"
    ))
    df1.columns = [item.replace("_", " ").capitalize() for item in df1.columns]

    bin_start = int(df1["Message sent"].min())
    bin_end = int(df1["Message sent"].max())
    bin_size = 5

    # df and title is mandatory, rest are kwargs, define any key you like.

    activity_hist1 = draw_histogram_plotly(df=df1,
                                           title=f"Total messages sent per student (bin width: {bin_size})",
                                           x='Message sent',
                                           bin_start=bin_start,
                                           bin_end=bin_end,
                                           bin_size=bin_size,
                                           y_axis_title="Number of students")

    del df1

    # Get metrics from Supabase
    df2 = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select * from user_engagement_data2"
    ))
    df2.columns = [item.replace("_", " ").capitalize() for item in df2.columns]

    bin_start = int(df2["Average messages"].min())
    bin_end = int(df2["Average messages"].max())
    bin_size = 5

    activity_hist2 = draw_histogram_plotly(df=df2,
                                           title=f"Messages per active day (bin width: {bin_size})",
                                           x='Average messages',
                                           bin_start=bin_start,
                                           bin_end=bin_end,
                                           bin_size=bin_size)

    # set bin width or calculate it (better if its one)
    context = {
        "dates": {"start_date": start_date.strftime("%Y-%m-%d"), "stop_date": stop_date.strftime("%Y-%m-%d")},
        "indicators": [
            {"label": "Total messages", "value": f"{total_messages:,.0f}"},
            {"label": "Total students", "value": f"{all_users_count:,.0f}"},
            {"label": "Average messages per student", "value": f"{average_messages_per_user:.2f}"},
            {"label": "Average messages per day per student", "value": f"{average_messages_per_day_per_user:.2f}"},
            {
                "label": "Average messages per student per active days",
                "value": f"{average_messages_per_user_per_active_days:.2f}"
            },
            {"label": "Messages per week per student", "value": f"{messages_per_week_per_user:.2f}"},

        ],
        "charts": [
            {"chart": activity_hist1},
            {"chart": activity_hist2},
        ],
        "images_data": [
            {"image_data": messages_per_student_per_week_image},
        ],
    }

    return context
