import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from dashboard.utils import select_from_supabase
import pandas as pd
import time
from dashboard.models import UserEngagement2Model


def calculate_metrics():
    print("Selecting data from Supabase!")

    _db_name = 'supabase-db-rori'
    # Query string to select data from Supabase with Django connection
    query = "select id, contact_uuid, date(inserted_at) as date_inserted_at " \
            "from django_delvin_messages " \
            "order by contact_uuid;"

    query_result = select_from_supabase(db_name=_db_name, query=query)

    df = pd.DataFrame.from_records(query_result)

    print("Deleting data!")
    # Delete all old data
    query = "delete from user_engagement_data2;"
    select_from_supabase(db_name=_db_name, query=query)

    print("Starting calculations!")

    agg_addr_df = df.groupby(['contact_uuid', 'date_inserted_at']).agg({'id': ['count']}).reset_index()

    all_users = []
    for i, user in enumerate(agg_addr_df[('contact_uuid', '')].unique()):
        active_days = int(agg_addr_df[('date_inserted_at', '')][agg_addr_df[('contact_uuid', '')] == user].count())
        messages = agg_addr_df[('id', 'count')][agg_addr_df[('contact_uuid', '')] == user].to_list()
        message_sent = sum(messages)
        average_messages = round(message_sent / active_days, 2) if active_days else 0
        item = {
            "contact_uuid": user,
            "message_sent": message_sent,
            "active_days": active_days,
            "average_messages": average_messages,
        }
        all_users.append(item)

        if not i % 100:
            part_to_models = [UserEngagement2Model(**row) for row in all_users]
            UserEngagement2Model.objects.bulk_create(part_to_models)
            print(f"Inserted {len(all_users)} rows. Time: {time.asctime()}!")
            all_users.clear()

    part_to_models = [UserEngagement2Model(**row) for row in all_users]
    UserEngagement2Model.objects.bulk_create(part_to_models)
    print(f"Inserted {len(all_users)} rows. Time: {time.asctime()}!")
    all_users.clear()


if __name__ == "__main__":
    calculate_metrics()
