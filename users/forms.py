from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile

# New class inherits from usercreationform


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    # Gives us a nested namespace for configurations
    class Meta:
        # form.save will save to this model
        model = User
        # form fields in the form
        fields = ["username", "email", "password1", "password2"]


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ["username", "email"]


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ["bot_name"]
        # fields = "__all__"
