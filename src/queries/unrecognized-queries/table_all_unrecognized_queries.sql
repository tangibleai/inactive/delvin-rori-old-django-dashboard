/*
CONTEXT:

QUERY RESULT:
When a user sends a message to the chatbot and it doesn't match any of the chatbot's existing rules, it replies with a fallback message (i.e. "Sorry, I didn't understand..."). We want the query to return all the users' messages that resulted in the fallback reply.
QUERY result:
Return uuid, content, inserted_at fields of all the messages that preceded in conversation a message with card_uuid equal to <variables.app_constants.catchall_card_id>
*/

SELECT
    prev_uuid
    ,prev_content
    ,prev_inserted_at
    ,chat_id
FROM
    (
    SELECT
        LAG(uuid,1) OVER (PARTITION BY chat_id ORDER BY inserted_at) prev_uuid
        , LAG(content,1) OVER (PARTITION BY chat_id ORDER BY inserted_at) prev_content
        , LAG(inserted_at,1) OVER (PARTITION BY chat_id ORDER BY inserted_at) prev_inserted_at
        , card_uuid
        ,chat_id
    FROM `dimagi-turnio-chatbot-111120.243827325289.messages`  
    WHERE 
        chat_id IN
            (
                SELECT chat_id FROM `dimagi-turnio-chatbot-111120.243827325289.messages`  
                WHERE card_uuid = '1ee63e62-7822-11eb-b9a0-42010a720029'
            ) 
        and 
        (direction = 'inbound' OR card_uuid = '1ee63e62-7822-11eb-b9a0-42010a720029')
    ) ds 
WHERE card_uuid = '1ee63e62-7822-11eb-b9a0-42010a720029'