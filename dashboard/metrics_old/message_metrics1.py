from datetime import timedelta

import pandas as pd


def calculate_metrics(df, start_date, stop_date, delta, x_title, y_title):
    new_list = []
    current_date = start_date.date()
    while current_date <= stop_date.date():
        date_range_start = current_date - timedelta(days=delta)
        mask = (df["short_inserted_at"] > date_range_start) & (df["short_inserted_at"] <= current_date)
        new_df = df[mask]
        new_list.append({x_title: current_date, y_title: new_df["id"].nunique()})
        current_date = current_date + timedelta(days=1)

    return pd.DataFrame.from_records(new_list)
