import streamlit as st
import pandas as pd
import plotly.express as px
from streamlit_plotly_events import plotly_events
from streamlit_javascript import st_javascript

from streamlit_frontend.src.const import *
from streamlit_frontend.charts.student_charts import student_lesson_table, student_activity_chart
def display_component(contact_uuid, student_records):

    if len(student_records):

        col_num_sessions, col_tot_les, col_tot_q, col_avg_acc  = st.columns([1, 1, 1, 1])
        with col_tot_les:
            tot_les = student_records[FN_LESSON].nunique()
            st.metric("Total Lessons Visited", tot_les)

        with col_tot_q:
            student_records['microlesson_question'] = student_records[FN_LESSON] + '-' + student_records[
                FN_QUESTION_NUM]
            tot_q = student_records['microlesson_question'].nunique()
            st.metric("Total Questions Answered", tot_q)

        with col_avg_acc:
            num_successes = student_records[student_records[FN_ANSWER_TYPE] == 'success'][FN_MSG_ID].nunique();
            num_succ_and_fail = student_records[student_records[FN_ANSWER_TYPE].isin(['success', 'failure'])][FN_MSG_ID].nunique()
            avg_acc = num_successes*100/num_succ_and_fail if num_succ_and_fail > 0 else 0
            st.metric("Average Accuracy", f"{avg_acc:.1f}%")

        with col_num_sessions:
            # Compute time difference between consecutive messages
            student_records['time_diff'] = student_records['message_inserted_at'].diff()

            # Identify where a new session starts (difference is NaN for the first row or difference is >= 1 hour)
            student_records['new_session'] = (student_records['time_diff'].isna()) | (student_records['time_diff'] >= pd.Timedelta(hours=1))

            # Count the number of sessions
            num_sessions = student_records['new_session'].sum()
            st.metric("Number of Math Sessions", num_sessions)

        student_records['lesson_skill_topic'] = (student_records[FN_LESSON]
                                                 + ' (' + student_records[FN_TOPIC] + ' - '
                                                 + student_records[FN_SKILL] + ')')

        st.write('## Student Activity Chart')
        st.write ("Click anywhere on the chart to browse to the relevant spot in the conversation")
        activity_chart = student_activity_chart(student_records)
        click_data = plotly_events(activity_chart, override_height=200)

        if click_data:
            message_id = student_records['original_message_id'].iloc[click_data[0]['x']]
            url = f"{DOMAIN}/?menu_index=3&contact_uuid={contact_uuid}&message_id={message_id}"
            js = f'window.open("{url}", "_blank").then(r => window.parent.location.href);'
            st_javascript(js)

        st.write("## Student's Progress")
        lesson_table = student_lesson_table(student_records)
        num_rows = student_records[FN_LESSON].nunique()
        table_height = (num_rows + 1) * 35 + 3
        st.dataframe(lesson_table, hide_index=True, height=table_height, use_container_width=True)



    else:
        st.write('No math data available for this student')