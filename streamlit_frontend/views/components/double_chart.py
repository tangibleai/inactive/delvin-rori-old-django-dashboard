import streamlit as st

def show_double_chart (initial_date_clause,
                       comparison_date_clause,
                       chart_func,
                       kwargs={}):

    col_orig, col_comp = st.columns([1,1])

    with col_orig:
        fig = chart_func(initial_date_clause, **kwargs)
        st.plotly_chart(fig, use_container_width=True)

    with col_comp:
        fig = chart_func(comparison_date_clause, is_comparison=True, **kwargs)
        st.plotly_chart(fig, use_container_width=True)
