import pandas as pd

from dashboard.utils import draw_line_plotly_html, select_from_supabase


def get_context_by_dates(start_date, stop_date):
    _db_name = "supabase-db-rori"

    # Get metrics for active, dormant and super users.
    df = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select * from active_user_data"
    ))
    df.columns = [item.replace("_", " ").capitalize() for item in df.columns]
    mask = (df["Date"] >= start_date.date()) & (df["Date"] <= stop_date.date())
    df = df[mask]

    activity_chart = draw_line_plotly_html(df=df,
                                           title="Activity chart",
                                           x='Date',
                                           y=["New users", "Active users", "Super users", "Dormant users", "All users"])
    del df

    # Get metrics for day, send x and y axis title
    df_day = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select * from active_user_data_delta where delta = 1"
    ))
    df_day.columns = [item.replace("_", " ").capitalize() for item in df_day.columns]
    mask = (df_day["Date"] >= start_date.date()) & (df_day["Date"] <= stop_date.date())
    df_day = df_day[mask]
    day_line_chart = draw_line_plotly_html(df=df_day, title="Daily active user", x='Date', y='Active user')
    del df_day

    # Get metrics for week, send x and y axis title
    df_week = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select * from active_user_data_delta where delta = 7"
    ))
    df_week.columns = [item.replace("_", " ").capitalize() for item in df_week.columns]
    mask = (df_week["Date"] >= start_date.date()) & (df_week["Date"] <= stop_date.date())
    df_week = df_week[mask]
    week_line_chart = draw_line_plotly_html(df=df_week, title="Weekly active user", x='Date', y='Active user')
    del df_week

    # Get metrics for month, send x and y axis title
    df_month = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select * from active_user_data_delta where delta = 30"
    ))
    df_month.columns = [item.replace("_", " ").capitalize() for item in df_month.columns]
    mask = (df_month["Date"] >= start_date.date()) & (df_month["Date"] <= stop_date.date())
    df_month = df_month[mask]
    month_line_chart = draw_line_plotly_html(df=df_month, title="Monthly active user", x='Date', y='Active user')
    del df_month

    # Get user indicators, last record is indicator for these metrics
    indicators = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select * from active_user_data "
              "order by id desc "
              "limit 1"
    ))

    active_students = indicators.iloc[0]["active_users"]
    new_students = indicators.iloc[0]["new_users"]
    dormant_students = indicators.iloc[0]["dormant_users"]
    super_students = indicators.iloc[0]["super_users"]
    all_students = indicators.iloc[0]["all_users"]

    # Create a context to return to the view
    context = {
        "dates": {"start_date": start_date.strftime("%Y-%m-%d"), "stop_date": stop_date.strftime("%Y-%m-%d")},
        "indicators": [
            {"label": "Active students", "value": f"{(active_students + new_students + super_students):,.0f}"},
            {"label": "Dormant students", "value": f"{dormant_students:,.0f}"},
            {"label": "All students", "value": f"{all_students:,.0f}"},
        ],
        "charts": [
            {"chart": activity_chart},
            {"chart": day_line_chart},
            {"chart": week_line_chart},
            {"chart": month_line_chart}
        ],
    }

    return context
