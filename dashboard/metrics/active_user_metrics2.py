import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from datetime import timedelta, datetime
from dashboard.utils import select_from_supabase
import pandas as pd
import time
from dashboard.models import ActiveUserDataDeltaModel


def get_data(_db_name):
    print("Selecting data from Supabase!")

    # Query string to select data from Supabase with Django connection
    query = "select contact_uuid, date(inserted_at) as date_inserted_at " \
            "from django_delvin_messages " \
            "group by contact_uuid, date(inserted_at) " \
            "order by contact_uuid;"

    query_result = select_from_supabase(db_name=_db_name, query=query)

    return pd.DataFrame.from_records(query_result)


def delete_data(_db_name):
    print("Deleting data!")
    # Delete all old data
    query = f"delete from active_user_data_delta;"
    select_from_supabase(db_name=_db_name, query=query)


def calculate_metrics(df, db_model, start_date, stop_date, delta):
    # For each day leave one record
    df = df

    final_list = []
    current_date = start_date.date()
    while current_date <= stop_date.date():
        date_range_start = current_date - timedelta(days=delta)
        mask = (df["date_inserted_at"] > date_range_start) & (df["date_inserted_at"] <= current_date)
        new_df = df[mask]
        final_list.append(
            # count of the users on that date with a 'delta' window
            {
                "date": datetime.strftime(current_date, "%Y-%m-%d"),
                "active_user": new_df["contact_uuid"].nunique(),
                "delta": delta,
            }
        )
        current_date = current_date + timedelta(days=1)

    del df

    print("Inserting data!")
    slice_size = 1000
    while final_list:
        part, final_list = final_list[:slice_size], final_list[slice_size:]
        part_to_models = [db_model(**row) for row in part]
        db_model.objects.bulk_create(part_to_models)
        print(f"Inserted {len(part)} rows. Time: {time.asctime()}!")


if __name__ == "__main__":
    db_name = "supabase-db-rori"

    # Get base data for calculations
    df_ = get_data(db_name)
    # Delete old calculations data
    delete_data(db_name)

    start_date_ = datetime(2022, 8, 1)
    stop_date_ = datetime.now()

    db_model_ = ActiveUserDataDeltaModel
    delta_list = [1, 7, 30]

    for delta_ in delta_list:
        calculate_metrics(
            df=df_,
            db_model=db_model_,
            start_date=start_date_,
            stop_date=stop_date_,
            delta=delta_,
        )
