"""Standalone script which uses Supabase API to update records.
Causing problem with httpx. You can use this script locally, but don't add it to the
TODO: see if we can delete it
"""

import os
import time
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from supabase import create_client, Client
from google.cloud import bigquery


class SupabaseETL:

    def __init__(self):
        # Get connection parameters and create a client
        self.url: str = os.environ.get("SUPABASE_URL_CC")
        self.key: str = os.environ.get("SUPABASE_API_KEY_CC")
        self.supabase: Client = create_client(self.url, self.key)

    def get_max_id_from_supabase(self):
        # Get max id from supabase to define where to fetch from
        response = self.supabase \
            .table('cc_delvin_messages') \
            .select("id") \
            .order(column="id", desc=True) \
            .limit(1) \
            .execute()
        data = response.data
        id_ = data[0]["id"] if data else 0
        print(f"max id: {id_}")
        return id_

    def get_records_from_gbq(self):
        # Fetches BQG data starting from the id_ parameter
        sql_query = f"select msg.id as id, to_hex(sha256(max(msg.from_addr))) as sec_addr, max(msg.inserted_at) as inserted_at, " \
                    f"from `rori-turn.12065906259.messages` as msg     " \
                    f"where direction = 'inbound' and id > @id_ " \
                    f"group by msg.id " \
                    f"order by msg.id asc " \
                    f"limit 10000000;"

        id_ = self.get_max_id_from_supabase()

        client = bigquery.Client()

        query_parameters = [
            bigquery.ScalarQueryParameter("id_", "INTEGER", id_),
        ]

        job_config = bigquery.QueryJobConfig(
            query_parameters=query_parameters,
        )

        query_job = client.query(sql_query, job_config=job_config)

        result = query_job.result()

        message = f"{query_job.total_bytes_processed // 1_000_000} MB processed!"
        print(f"GBQ message: {message}")

        return result

    def insert_records_to_supabase(self):
        # By using GBQ query result object, inserts the data to supabase
        len_data = 0
        result = self.get_records_from_gbq()
        rows = []
        start_time = time.asctime()
        for i, row in enumerate(result):
            row_dict = dict(row)
            row_dict["inserted_at"] = row_dict["inserted_at"].isoformat()
            rows.append(row_dict)
            if i % 5000 == 0:
                data, count = self.supabase \
                    .table('cc_delvin_messages') \
                    .insert(rows) \
                    .execute()
                rows.clear()
                len_data += len(data[1])
                print(len_data, time.asctime())
        data, count = self.supabase \
            .table('cc_delvin_messages') \
            .insert(rows) \
            .execute()
        rows.clear()
        len_data += len(data[1])
        print(f"len data: {len_data} started: {start_time} finished: {time.asctime()}")


if __name__ == "__main__":
    s = SupabaseETL()
    s.insert_records_to_supabase()
