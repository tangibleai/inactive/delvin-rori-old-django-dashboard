import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

"""
pip install pyqt5
"""

df = pd.read_csv("data/message_metrics4.csv")

x = list(range(df.shape[0]))
y_min = 0
y_max = 501
y_step = 50
x_lim = 25
y_lim = 500

title = "test chart"

"""Pass a dataframe and column names
Draw a line chart with sns, return as image data.
"""

plt.figure(figsize=(16, 10))
plt.yticks(np.arange(y_min, y_max, y_step))
plt.title('Messages per student per week (each line is a student)', fontsize=12)
plt.xlabel('Week', fontsize=12)
plt.ylabel('Message', fontsize=12)
ax = plt.gca()
ax.set_xlim([0, x_lim])
ax.set_ylim([0, y_lim])

for i, c in enumerate(df.columns):
    if not i % 2500:
        print(i)
    plt.plot(
        x,
        df[c],
        color="blue",
        alpha=0.05,
    )


print(i)
# plt.legend()
plt.savefig("data/message_metrics4_chart.png")
