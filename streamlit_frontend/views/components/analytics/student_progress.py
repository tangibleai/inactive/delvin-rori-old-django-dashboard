import pandas as pd
import numpy as np
import streamlit as st
import plotly.graph_objects as go
import plotly.express as px

from dashboard.etl.utils import select_from_supabase

from streamlit_frontend.src.const import MATH_USER_SEGMENTS
from streamlit_frontend.src.etl import get_single_student_metric
from streamlit_frontend.src.sql_utils import WhereClause


def calc_hist_bins(df, df_c, metric):
    _, edges = np.histogram(df[metric], bins='auto')  # You can adjust the number of bins
    bin_size = np.ceil(edges[1] - edges[0])

    _, edges_c = np.histogram(df[metric], bins='auto')
    bin_size_c = np.ceil(edges_c[1] - edges_c[0])

    bin_size_opt = min(bin_size, bin_size_c)
    start_opt = min(edges[0], edges_c[0])
    end_opt = max (edges[-1], edges_c[-1])+bin_size_opt

    return start_opt, end_opt, bin_size_opt

def metric_histogram(data, metric, xbins=None, xrange=None, is_comparison=False):
    if xbins and xrange:
        fig = go.Figure(go.Histogram(x=data[metric], autobinx=False, xbins=xbins))
        fig.update_layout(title=f'{metric} distribution - comparison period',
                          xaxis_title=metric,
                          yaxis_title='Number of students',
                          hovermode='x')
        fig.update_layout(xaxis=dict(range=xrange))

    else:
        fig = px.histogram(data, x=metric)

        fig.update_layout(xaxis_title=metric,
                          yaxis_title='Number of students',
                          hovermode='x')

    if is_comparison:
        fig.update_layout(title=f'{metric} distribution - comparison period')
        fig.update_traces(marker=dict(color='red'))
    else:
        fig.update_layout(title=f'{metric} distribution')

    return fig

def display_student_progress(metric_select, segment_select, date_clause, to_compare=None, where_clause_c=None):
    where_clause = WhereClause(date_clause)

    if segment_select != 'All':
        where_clause.add_str_subclause(field="user_segment",
                                      operator="=",
                                      value=segment_select)
        if to_compare:
            where_clause_c.add_str_subclause(field="user_segment",
                                      operator="=",
                                      value=segment_select)

    df = get_single_student_metric(metric_select, where_clause)

    if to_compare:
        df_c = get_single_student_metric(metric_select, where_clause_c)

        start, end, bin_size = calc_hist_bins(df, df_c, metric_select)
        # Create xbins dictionary for Plotly
        xbins = {
            'start': start,
            'end': end,
            'size': bin_size  # Assuming uniform bin sizes
        }

        xrange = [start-1, end+1]
        col_orig, col_comp = st.columns([1,1])

        with col_orig:
            fig = metric_histogram(df, metric_select, xbins, xrange)
            st.plotly_chart(fig, use_container_width=True)

        with col_comp:
            fig = metric_histogram(df_c, metric_select, xbins, xrange, is_comparison=True)
            st.plotly_chart(fig, use_container_width=True)

    else:
        fig = metric_histogram(df, metric_select)
        st.plotly_chart(fig)

def display_component(date_clause, to_compare=False, date_clause_c = None):
    where_clause = WhereClause(date_clause)

    if to_compare:
        where_clause_c = WhereClause(date_clause_c)

    col_metric, col_segment, col_chart_type = st.columns([3, 3, 1])

    with col_metric:
        metric_select = st.selectbox('Metric to Display', ['num_messages',
                                                           'num_math_messages',
                                                           'num_active_days',
                                                           'num_active_weeks',
                                                           'num_lessons_visited',
                                                           'num_lessons_completed',
                                                           'num_sessions'])

    with col_segment:
        segments = ['All'] + MATH_USER_SEGMENTS
        segment_select = st.selectbox('User Segment', segments)

    if segment_select != 'All':
        where_clause.add_str_subclause(field="user_segment",
                                      operator="=",
                                      value=segment_select)
        if to_compare:
            where_clause_c.add_str_subclause(field="user_segment",
                                      operator="=",
                                      value=segment_select)


    df = get_single_student_metric(metric_select, where_clause)
    if to_compare:
        df_c = get_single_student_metric(metric_select, where_clause_c)

        start, end, bin_size = calc_hist_bins(df, df_c, metric_select)
        # Create xbins dictionary for Plotly
        xbins = {
            'start': start,
            'end': end,
            'size': bin_size  # Assuming uniform bin sizes
        }

        xrange = [start-1, end+1]
        col_orig, col_comp = st.columns([1,1])

        with col_orig:
            fig = metric_histogram(df, metric_select, xbins, xrange)
            st.plotly_chart(fig, use_container_width=True)

        with col_comp:
            fig = metric_histogram(df_c, metric_select, xbins, xrange, is_comparison=True)
            st.plotly_chart(fig, use_container_width=True)

    else:
        fig = metric_histogram(df, metric_select)
        st.plotly_chart(fig)