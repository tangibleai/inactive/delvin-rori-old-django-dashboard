from django.urls import path

from dashboard import views, views_api

urlpatterns = [
    path("active_user/", views.active_user, name="active_user"),
    path("user_lifetime/", views.user_lifetime, name="user_lifetime"),
    path("user_engagement/", views.user_engagement, name="user_engagement"),
    path("question_answer/", views.question_answer, name="question_answer"),
    path("student_stats/", views_api.student_stats, name="student_stats"),
    path("student_details/", views_api.student_details, name="student_details"),
]
