/* 
CONTEXT: 
- Generates a table where messages are joined with chat, contact, content, and tag information
RESULT EXPECTATION
- A table with messages augmented by additional information
*/

SELECT  
  messages.addressees as addressees,
  messages.author_type as author_type,
  messages.button_reply_id as button_reply_id,
  messages.button_reply_title as button_reply_title,
  messages.card_uuid as card_uuid,
  messages.content as content,
  messages.direction as direction,
  messages.inserted_at as inserted_at, 
  messages.chat_id as chat_id,
  messages.in_reply_to as in_reply_to,
  messages.uuid as uuid,
  domain_tag_map.tag_name as domain_tag,
  event_tag_map.tag_name as event_tag,
  event_tag_map.conversion as conversion,
  chats_and_contacts.contact_id as contact_id,
  chats_and_contacts.fields as fields,
  unique_content.title as content_title,
  case
         when fields LIKE '%"gender":"FEMME"%' then 'female'
         when fields LIKE '%"gender":"HOMME"%' then 'male'
         else 'unknown'
       end as gender,
  case
         when fields LIKE '%"age":"MOINSDE20ANS"%' then '<20'
         when fields LIKE '%"age":"DE2049ANS"%' then '20-49'
         when fields LIKE '%"age":"PLUSDE50ANS%' then '>49'
         else 'unknown'
       end as age

FROM {{variables.tables.messages_table}} as messages 
LEFT JOIN
(
  SELECT card_tag_map.card_uuid as card_uuid,
          domain_tags.id as tag_id,
          domain_tags.tag_name as tag_name
            
    FROM
    (
      {{variables.tables.content_tag_map}} as card_tag_map
      inner join
      (SELECT * FROM {{variables.tables.tag_table}} where tag_category = 'domain_tag')as domain_tags
      ON card_tag_map.tag_id = domain_tags.id
    )
   
) as domain_tag_map
ON messages.card_uuid = domain_tag_map.card_uuid

LEFT JOIN
(
  SELECT card_tag_map.card_uuid as card_uuid,
          event_tags.tag_name as tag_name,
          event_tags.conversion as conversion
            
    FROM
    (
      {{variables.tables.content_tag_map}} as card_tag_map
      inner join
      (SELECT * FROM {{variables.tables.tag_table}} where tag_category = 'event_tag')as event_tags
      ON card_tag_map.tag_id = event_tags.id
    )
   
) as event_tag_map
ON messages.card_uuid = event_tag_map.card_uuid

LEFT JOIN
(
    SELECT 
      chats_unique.chat_id as chat_id
      ,chats_unique.state as state
      ,contact_details_unique.contact_id as contact_id
      ,contact_details_unique.fields as fields
    FROM
      (SELECT
        id as chat_id
        ,contact_id
        ,state
        ,inserted_at as chat_inserted_at
        ,updated_at as chat_updated_at
      FROM
        (
        SELECT
        id 
        ,contact_id
        ,state
        ,inserted_at
        ,updated_at

        ,ROW_NUMBER() OVER (PARTITION BY id ORDER BY updated_at DESC) AS rownumber
        FROM
          {{variables.tables.users_table}}
        )
      WHERE
      rownumber = 1) as chats_unique
    INNER JOIN
      (
        SELECT
            contact_id
            ,fields
            ,updated_at as contact_updated_at
          FROM
            (
            SELECT
             contact_id
            ,fields
            ,updated_at
            ,ROW_NUMBER() OVER (PARTITION BY contact_id ORDER BY updated_at DESC) AS rownumber
            FROM
              {{variables.tables.contacts_table}}
            )
          WHERE
          rownumber = 1
          ) as contact_details_unique
    ON chats_unique.contact_id = contact_details_unique.contact_id
 ) as chats_and_contacts 
ON chats_and_contacts.chat_id = messages.chat_id
LEFT JOIN
(
    SELECT
      uuid
      ,title
      ,content
      ,language
      ,updated_at
    FROM
      (
      SELECT
      uuid
      ,title
      ,content
      ,language
      ,updated_at
      --order by updated_at
      ,ROW_NUMBER() OVER (PARTITION BY uuid ORDER BY updated_at DESC) AS rownumber
      FROM
        {{variables.tables.content_table}}
      WHERE
      --add more conditions here if needed 
      is_deleted IS false
      )
    WHERE
    rownumber = 1
  
) as unique_content
ON messages.card_uuid = unique_content.uuid
