import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from collections import Counter
from datetime import timedelta, datetime
from dashboard.utils import select_from_supabase

import pandas as pd
import time
from dashboard.models import ActiveUserDataModel


def calculate_metrics(start_date, stop_date, delta):
    print("Selecting data from Supabase!")

    _db_name = "supabase-db-rori"  # Don't change it unless you know what you are doing!
    # Query string to select data from Supabase with Django connection
    query = "select contact_uuid, date(inserted_at) as date_inserted_at " \
            "from django_delvin_messages " \
            "group by contact_uuid, date(inserted_at) " \
            "order by contact_uuid;"

    query_result = select_from_supabase(db_name=_db_name, query=query)

    df = pd.DataFrame.from_records(query_result)

    final_list = []

    current_date = start_date.date()

    print("Starting calculations!")
    while current_date <= stop_date.date():
        # For each day from start_date, with a 30 days window calculate student metrics.
        active_students = []
        dormant_students = []
        new_students = []
        super_students = []

        mask = df["date_inserted_at"] == current_date
        last_df = df[mask]
        last_count = Counter(last_df["contact_uuid"].to_list())

        mask = df["date_inserted_at"] <= current_date
        all_df = df[mask]
        all_count = Counter(all_df["contact_uuid"].to_list())

        date_range_start = current_date - timedelta(days=delta)
        mask = (df["date_inserted_at"] > date_range_start) & (df["date_inserted_at"] <= current_date)
        window_df = df[mask]
        windows_count = Counter(window_df["contact_uuid"].to_list())

        for student in all_count.keys():
            student_count = windows_count.get(student, 0)

            if student_count >= 5:
                super_students.append(student)
            elif student_count >= 1 and not last_count.get(student, 0):
                active_students.append(student)
            elif last_count.get(student, 0) != 0:
                new_students.append(student)
            else:
                dormant_students.append(student)

        item = {
            "date": datetime.strftime(current_date, "%Y-%m-%d"),
            "active_users": len(active_students),
            "new_users": len(new_students),
            "dormant_users": len(dormant_students),
            "super_users": len(super_students),
            "all_users": len(all_count),
        }

        final_list.append(item)

        current_date = current_date + timedelta(days=1)

    print("Deleting data!")
    query = "delete from active_user_data;"
    select_from_supabase(db_name=_db_name, query=query)

    print("Inserting data!")
    slice_size = 1000
    while final_list:
        part, final_list = final_list[:slice_size], final_list[slice_size:]
        part_to_models = [ActiveUserDataModel(**row) for row in part]
        ActiveUserDataModel.objects.bulk_create(part_to_models)
        print(f"Inserted {len(part)} rows. Time: {time.asctime()}!")


if __name__ == "__main__":
    # From beginning of Rori till today calculate activity metrics
    start_date_ = datetime(2022, 8, 1)
    stop_date_ = datetime.now()
    delta_ = 30  # start from 30 days back for calculation
    calculate_metrics(
        start_date=start_date_,
        stop_date=stop_date_,
        delta=delta_,
    )
