import math

import streamlit as st
from streamlit_chat import message
from streamlit_plotly_events import plotly_events
from streamlit_javascript import st_javascript


from streamlit_frontend.charts.student_charts import student_activity_chart

PAGE_SIZE = 15

def set_session_state(student_messages):
    if 'current_page' not in st.session_state:
        st.session_state['current_page'] = 0

    if 'max_message' not in st.session_state:
        st.session_state['max_message'] = len(student_messages)

    if 'curr_from_message' not in st.session_state:
        st.session_state['curr_from_message'] = st.session_state['max_message'] - PAGE_SIZE

def focus_on_message(message_num):
    st.session_state["curr_from_message"] = message_num-1


def increment_current_page():
    st.session_state["curr_from_message"] = st.session_state["curr_from_message"] + PAGE_SIZE
    if st.session_state["curr_from_message"] > st.session_state["max_message"]:
        st.session_state["curr_from_message"] =st.session_state["max_message"]

def decrement_current_page():
    st.session_state["curr_from_message"] -= PAGE_SIZE
    if st.session_state["curr_from_message"] <0:
        st.session_state["curr_from_message"] = 0


def load_messages(student_messages, from_message):
    to_message = min(from_message + PAGE_SIZE, st.session_state["max_message"])
    curr_messages = student_messages.iloc[from_message:to_message]

    for i, m in curr_messages.iterrows():
        if m['direction'] == 'inbound':
            with st.chat_message("user"):
                st.write(m["message_text"])
        else:
            with st.chat_message("assistant",
                                 avatar='https://images.squarespace-cdn.com/content/v1/6268eb8de6c2576f5d8f5a9a/ba250914-0a7d-49a8-b7b7-22d9892e6625/rori-500px.png'):
                st.write(m["message_text"])



def display_component(contact_uuid, student_records, student_messages):
    set_session_state(student_messages)

    from_message = st.session_state["curr_from_message"]
    st.write(f"curr_from_message: {from_message}")

    _,col_button_left, col_title, col_button_right,_ = st.columns([1,1,3,1,1])

    with col_button_left:
        st.button('<--', on_click=decrement_current_page)

    with col_button_right:
        st.button('-->', on_click=increment_current_page)

    to_message = min(from_message + PAGE_SIZE, st.session_state["max_message"])

    load_messages(student_messages,from_message)

    # with col_title:
    #     st.write(f"Displaying messages {from_message}-{to_message-1} out of {len(student_messages)}")



    # curr_messages = student_messages.iloc[from_message:to_message]
    #
    # for i,m in curr_messages.iterrows():
    #     if m['direction'] == 'inbound':
    #         with st.chat_message("user"):
    #             st.write(m["message_text"])
    #     else:
    #         with st.chat_message("assistant", avatar='https://images.squarespace-cdn.com/content/v1/6268eb8de6c2576f5d8f5a9a/ba250914-0a7d-49a8-b7b7-22d9892e6625/rori-500px.png'):
    #             st.write(m["message_text"])
