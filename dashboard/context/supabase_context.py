import pandas as pd

from dashboard.queries.supabase_queries import today_message_count_sql, today_student_count_sql, message_count_sql, \
    student_count_sql
from dashboard.utils import select_from_supabase


def get_context_by_dates(start_date, stop_date):
    today_message_count = select_from_supabase("supabase-db-rori", today_message_count_sql())[0]["Message Count"]
    today_student_count = select_from_supabase("supabase-db-rori", today_student_count_sql())[0]["Student Count"]

    daily_messages = select_from_supabase("supabase-db-rori",
                                          message_count_sql(start=start_date, stop=stop_date, step="day"))
    df1 = pd.DataFrame.from_records(daily_messages)

    weekly_messages = select_from_supabase("supabase-db-rori",
                                           message_count_sql(start=start_date, stop=stop_date, step="week"))
    df2 = pd.DataFrame.from_records(weekly_messages)

    monthly_messages = select_from_supabase("supabase-db-rori",
                                            message_count_sql(start=start_date, stop=stop_date, step="month"))
    df3 = pd.DataFrame.from_records(monthly_messages)

    daily_students = select_from_supabase("supabase-db-rori",
                                          student_count_sql(start=start_date, stop=stop_date, step="day"))
    df4 = pd.DataFrame.from_records(daily_students)

    weekly_students = select_from_supabase("supabase-db-rori",
                                           student_count_sql(start=start_date, stop=stop_date, step="week"))
    df5 = pd.DataFrame.from_records(weekly_students)

    monthly_students = select_from_supabase("supabase-db-rori",
                                            student_count_sql(start=start_date, stop=stop_date, step="month"))
    df6 = pd.DataFrame.from_records(monthly_students)

    context = {
        'charts': [
            {
                "title": "Messages per day", "xlabel": "Date", "ylabel": "Message count",
                "labels": [label for label in df1.get('Date', [])],
                "data": [msg for msg in df1.get('Message Count', [])]},
            {
                "title": "Messages per week", "xlabel": "Date", "ylabel": "Message count",
                "labels": [label for label in df2.get('Date', [])],
                "data": [msg for msg in df2.get('Message Count', [])]},
            {
                "title": "Messages per month", "xlabel": "Date", "ylabel": "Message count",
                "labels": [label for label in df3.get('Date', [])],
                "data": [msg for msg in df3.get('Message Count', [])]},
            {
                "title": "Students per day", "xlabel": "Date", "ylabel": "Student count",
                "labels": [label for label in df4.get('Date', [])],
                "data": [msg for msg in df4.get('Student Count', [])]},
            {
                "title": "Students per week", "xlabel": "Date", "ylabel": "Student count",
                "labels": [label for label in df5.get('Date', [])],
                "data": [msg for msg in df5.get('Student Count', [])]},
            {
                "title": "Students per month", "xlabel": "Date", "ylabel": "Student count",
                "labels": [label for label in df6.get('Date', [])],
                "data": [msg for msg in df6.get('Student Count', [])]}
        ],
        'indicators': [
            {"label": "Daily Messages", "value": today_message_count},
            {"label": "Daily Students", "value": today_student_count},
            {"label": "New Users", "value": "9999"},
            {"label": "Daily Correct Answers", "value": "9999"},
            {"label": "Daily Wrong Answers", "value": "9999"}
        ]
    }
    return context
