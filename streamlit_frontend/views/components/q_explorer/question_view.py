import streamlit as st
import plotly.express as px
from datetime import datetime

from streamlit_frontend.src.sql_utils import WhereClause
from streamlit_frontend.src.etl import get_math_data_with_clause, get_question_versions
from streamlit_frontend.src.const import *

def display_component(where_clause, lesson, question):

    ## Getting the data

    question_clause = WhereClause(where_clause)
    question_clause.add_str_subclause(field=FN_LESSON,
                                      value=lesson)
    question_clause.add_str_subclause(field=FN_QUESTION_NUM,
                                      value=question)
    fields = [
        FN_ANSWER_TYPE,
        FN_HINT_SHOWN,
        FN_INSERTED_AT,
        FN_MSG_ID,
        FN_MSG_TEXT,
    ]

    lesson_df = get_math_data_with_clause(fields, question_clause)
    lesson_df["month_year"] = lesson_df[FN_INSERTED_AT].apply(lambda x:datetime.strftime(x,'%Y-%m'))

    if not len(lesson_df):
        st.write("## No question answers for this period of time")
        return

    question_versions = get_question_versions(question_clause)

    st.write("### Question versions")
    st.table(question_versions)


        # qchart_df = (
        #     lesson_df.groupby([FN_ANSWER_TYPE])
        #     .agg({FN_MSG_ID: "nunique"})
        #     .rename(columns={FN_MSG_ID: "count"})
        #     .reset_index()
        # )
        # qchart_df[question] = question
        # fig = px.histogram(
        #     qchart_df,
        #     x=question,
        #     y="count",
        #     color=FN_ANSWER_TYPE,
        #     barnorm="percent",
        #     category_orders={
        #         FN_ANSWER_TYPE: [
        #             "success",
        #             "recovered",
        #             "failure",
        #             "harder",
        #             "hint",
        #             "next",
        #         ]
        #     },
        #     title="Answers composition per question",
        # )
        # st.plotly_chart(fig)

    col_correct, col_wrong, col_other = st.columns([1, 1, 1])
    correct_answers = lesson_df[
        lesson_df[FN_ANSWER_TYPE].isin(["success"])
    ].rename(columns={FN_MSG_TEXT: "count"})["count"].value_counts()
    wrong_answers = (
        lesson_df[lesson_df[FN_ANSWER_TYPE].isin(["failure"])]
        .rename(columns={FN_MSG_TEXT: "count"})
        ["count"]
        .value_counts()
        .sort_values(ascending=False)
    )
    other_answers = (
        lesson_df[
            (
                    (lesson_df[FN_ANSWER_TYPE] != "success")
                    & (lesson_df[FN_ANSWER_TYPE] != "recovered")
                    & (lesson_df[FN_ANSWER_TYPE] != "failure")
                    & (lesson_df[FN_ANSWER_TYPE] != "hint")
                    & (lesson_df[FN_ANSWER_TYPE] != "returning_user_message")
            )
            & lesson_df[FN_ANSWER_TYPE].notnull()
            ]
        .groupby([FN_MSG_TEXT, FN_ANSWER_TYPE])
        .agg({FN_MSG_ID: "nunique"})
        .rename(columns={FN_MSG_TEXT: "count"})
    )

    with col_correct:
        st.markdown("#### Correct Answers")
        st.dataframe(correct_answers, width=300)

    with col_wrong:
        st.markdown("#### Wrong Answers")
        st.dataframe(wrong_answers, width=300)

    with col_other:
        st.markdown("#### Other Answers")
        st.dataframe(other_answers, width=400)
