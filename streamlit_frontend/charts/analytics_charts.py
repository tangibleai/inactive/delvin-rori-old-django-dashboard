import pandas as pd
import numpy as np
import copy
import plotly.express as px
import plotly.graph_objects as go
import plotly.figure_factory as ff

from dashboard.metrics.user_features import run_custom_query

from streamlit_frontend.src.sql_utils import WhereClause
from streamlit_frontend.src.const import ALL_USER_SEGMENTS
from streamlit_frontend.src.etl import get_math_event_data
from streamlit_frontend.charts.sankey_charts import sankey_chart


def onboarding_stage_by_time(date_clause=None, percentage=False):
    where_string = ''
    if date_clause:
        where_string = date_clause.get_clause()

    df = run_custom_query(
        f"""
        SELECT 
            DATE(inserted_at) AS date,
            onboarding_stage, 
            count (distinct contact_uuid) as num_users
        FROM
            student_metrics
        {where_string}
        GROUP BY
            DATE(inserted_at), onboarding_stage;
        """
    )

    df['onboarding_stage'] = df['onboarding_stage'].fillna('Greeting')
    print(df['onboarding_stage'].value_counts())

    if percentage:
        fig = px.area(df, x='date', y='num_users', color='onboarding_stage',
                  line_group='onboarding_stage',groupnorm='percent',
                  category_orders= {'onboarding_stage':['Greeting', 'Registration_AF', 'Onboarding_AF', 'Level', 'Math Quiz']},
                  hover_data=['num_users'],
                  labels={'num_users': '% of Users'})

        fig.update_layout(
                      xaxis_title='Date',
                      yaxis_title='% of Users',
                      showlegend=True,
                      hovermode='x',
                      )

    else:
        fig = px.area(df, x='date', y='num_users', color='onboarding_stage',
                      line_group='onboarding_stage',
                      category_orders={'onboarding_stage': ['Greeting','Registration_AF','Onboarding_AF',  'Level', 'Math Quiz']},
                      hover_data=['num_users'],
                      labels={'num_users': 'Number of Users'})

        fig.update_layout(
                          xaxis_title='Date',
                          yaxis_title='Number of Users',
                          showlegend=True,
                          hovermode='x',
                          )

    return fig



def user_segment_by_time(percentage=False):
    df = run_custom_query(
        """
        SELECT 
            DATE(inserted_at) AS date,
            user_segment, 
            count (distinct contact_uuid) as num_users
        FROM
            student_metrics
        GROUP BY
            DATE(inserted_at), user_segment;
        """
    )

    if percentage:
        fig = px.area(df, x='date', y='num_users', color='user_segment',
                      line_group='user_segment', groupnorm='percent',
                      category_orders= {'user_segment':ALL_USER_SEGMENTS},
                      hover_data=['num_users'],
                      labels={'num_users': '% of Users'})

        fig.update_layout(
                      xaxis_title='Date',
                      yaxis_title='% of Users',
                      showlegend=True,
                      hovermode='x',
                      )

    else:
        fig = px.area(df, x='date', y='num_users', color='user_segment',
                      line_group='user_segment',
                      category_orders={'user_segment': ALL_USER_SEGMENTS},
                      hover_data=['num_users'],
                      labels={'num_users': 'Number of Users'})

        fig.update_layout(
                          xaxis_title='Date',
                          yaxis_title='Number of Users',
                          showlegend=True,
                          hovermode='x',
                          )

    return fig

def display_student_metric(date_clause, metric, segment, is_comparison=False, xbins=None, xrange=None, chart_type='distribution'):
    if not date_clause:
        where_clause = WhereClause()
    else:
        where_clause = copy.deepcopy(date_clause)

    if segment != 'All':
        where_clause.add_str_subclause(field="user_segment",
                                       operator="=",
                                       value=segment)


    if chart_type == 'timeline':
        df = run_custom_query(
            f"""
            SELECT 
                DATE(inserted_at) AS date,
                count (distinct contact_uuid) as num_users,
                PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY {metric}) as metric
            FROM
                student_metrics 
            {where_clause.get_clause()}
            GROUP BY
                DATE(inserted_at)
            order by 
                DATE(inserted_at)
            """
        )
        df['moving_average']= df['metric'].rolling(7).mean()

        fig = px.line(df, x='date', y='metric', hover_data=['metric', 'num_users'])

        fig.update_layout(title=f'Median {metric} By Date of Joining',
                          xaxis_title='Date',
                          yaxis_title=metric,
                          hovermode='x')

    elif chart_type == 'distribution':
        df = run_custom_query(
                f"""
                SELECT 
                    contact_uuid, 
                    {metric} 
                FROM 
                    student_metrics
                {where_clause.get_clause()};
                """
        )

        df=df.fillna(0)

        #removing outliers
        # cutoff = df[metric].quantile(0.95)
        # df = df[df[metric] <= cutoff]
        if xbins:
            fig = go.Figure(go.Histogram(x=df[metric], autobinx=False, xbins=xbins))
        else:
            fig = px.histogram(df, x=metric)

        fig.update_layout(title=f'Distribution of {metric}',
                          xaxis_title=metric,
                          yaxis_title='Number of students',
                          hovermode='x')

        if xrange:
            fig.update_layout(xaxis = dict(range=xrange))

        if is_comparison:
            fig.update_traces(marker=dict(color='red'))

    return fig


def last_activity_by_segment():
    query = """
        select count(contact_uuid) as num_users, user_segment, last_activity
        from student_metrics
        group by user_segment, last_activity
    """

    df = run_custom_query(query)

    fig = px.bar(df, x='user_segment', y='num_users', color='last_activity')

    fig.update_layout(title='Last Activity by Segment',
                      xaxis_title='User Segment')
    return fig

def user_acquisition_funnel(date_clause=None, is_comparison=False, include_segments=[]):
    where_string = ''
    if date_clause:
        where_string = date_clause.get_clause()

    df = run_custom_query(
        f"""
        SELECT 
            user_segment,
            count (distinct contact_uuid) as num_users
        FROM
            student_metrics
        {where_string}
        GROUP BY
             onboarding_stage, user_segment;
        """
    )

    df = df.sort_values('user_segment')

    df['num_users_reached'] = df['num_users'][::-1].cumsum()[::-1]

    if include_segments:
        df = df[df['user_segment'].isin(include_segments)]

    fig = go.Figure(go.Funnel(
        y=df['user_segment'],
        x=df['num_users_reached'],
        textinfo="value+percent initial+percent previous",
        textposition="auto",
    ))

    num_steps = len(df)
    fig.update_layout(
        autosize=False,
        height= (num_steps*90+100),
    )

    if is_comparison:
        fig.update_traces(marker=dict(color='red'))

    return fig

def student_activity_chart(where_clause):
    def discrete_colorscale(bvals, colors):
        """
        bvals - list of values bounding intervals/ranges of interest
        colors - list of rgb or hex colorcodes for values in [bvals[k], bvals[k+1]],0<=k < len(bvals)-1
        returns the plotly  discrete colorscale
        """
        if len(bvals) != len(colors) + 1:
            raise ValueError('len(boundary values) should be equal to  len(colors)+1')
        bvals = sorted(bvals)
        nvals = [(v - bvals[0]) / (bvals[-1] - bvals[0]) for v in bvals]  # normalized values

        dcolorscale = []  # discrete colorscale
        for k in range(len(colors)):
            dcolorscale.extend([[nvals[k], colors[k]], [nvals[k + 1], colors[k]]])
        return dcolorscale

    query = """
            SELECT 
                contact_uuid,
                message_inserted_at,
                question_micro_lesson, 
                question_number,
                CASE 
                    WHEN question_number='0' THEN 0.5
                    WHEN answer_type='next' or answer_type='menu' THEN 0.0
                    WHEN answer_type='failure' or answer_type='success' or answer_type='hint' THEN 0.25
                    ELSE 0.75
                END as event_code,
                ROW_NUMBER() OVER (PARTITION by contact_uuid ORDER BY message_inserted_at) as rank
            FROM
                (
                SELECT 
                )
            """
    df = run_custom_query(query)
    df['unique_question'] = df['question_micro_lesson'] + '-' - df['question_number']
    pivot_df = df.pivot(index='contact_uuid', columns='rank', values='event_code')

    pivot_df['non_NaN_count'] = pivot_df.count(axis=1)
    df_to_plot = pivot_df[pivot_df['non_NaN_count'] >= 100]
    columns_to_plot = pivot_df.columns[:100]
    df_to_plot = df_to_plot[columns_to_plot]
    df_to_plot['percent_new'] = df_to_plot.apply(lambda row: (row == 0.5).sum(), axis=1) / 100
    df_to_plot = df_to_plot.sort_values(by='percent_new')


    bvalues = [0.0, 0.25, 0.5, 0.75, 1.0]
    colors = ['rgb(255,0,0)', 'rgb(0,255,0)', 'rgb(0,0,255)', 'rgb(200,200,200)']

    colorscale = discrete_colorscale(bvalues, colors)
    bvals = np.array(bvalues)
    tickvals = [np.mean(bvals[k:k + 2]) for k in
                range(len(bvals) - 1)]  # position with respect to bvals where ticktext is displayed
    ticktext = ['next/menu', 'new lesson', 'question answer', 'other']

    fig = go.Figure(data=go.Heatmap(
        z=df_to_plot.values,
        x=columns_to_plot,
        y=df_to_plot.index,
        colorscale=colorscale,
        colorbar=dict(thickness=25,
                      tickvals=tickvals,
                      ticktext=ticktext)
        )
    )


def accuracy_trends():
    query= """
           SELECT contact_uuid, question_micro_lesson, message_inserted_at, answer_type
           from math_question_answer   
           """
    df = run_custom_query(query)
    df = df[(df['answer_type']=='success') | (df['answer_type']=='failure')]
    df = df.sort_values(by='message_inserted_at')

    # 2. Group the DataFrame by 'contact_uuid' and 'question_micro_lesson'
    groups = df.groupby(['contact_uuid', 'question_micro_lesson'])

    # 3. Calculate accuracy and minimum 'message_inserted_at' per group
    grouped_stats = groups.agg(
        accuracy=('answer_type', lambda x: (x == 'success').mean()),
        min_inserted_at=('message_inserted_at', 'min'),
        total_rows=('message_inserted_at', 'count')
    )

    # 4. Rank each lesson in chronological order
    grouped_stats['rank'] = grouped_stats.groupby('contact_uuid')['min_inserted_at'].rank(method="first").astype(int)
    grouped_stats['completion'] = grouped_stats['total_rows'].apply(lambda x: min(x/10.0, 1.0))

    # 5. Pivot the DataFrame
    pivot_table = grouped_stats.pivot_table(
        index='contact_uuid', columns='rank', values='completion'
    )

    # 6. Filter users with at least 10 non-NaN values
    filtered_users = pivot_table.dropna(thresh=10)

    # 7. Filter only the first 10 columns
    df_to_plot = filtered_users.iloc[:, :20]
    df_to_plot['num_lessons'] = df_to_plot.count(axis=1)
    order = ['num_lessons'] + list(range(1,21).__reversed__())
    df_to_plot = df_to_plot.sort_values(by=order)
    df_to_plot = df_to_plot.drop(columns=['num_lessons'])

    # Display the filtered pivot table
    fig = go.Figure(data=go.Heatmap(
        z=df_to_plot.values,
        x=df_to_plot.columns,
        y=df_to_plot.index,
        colorscale='bluered'
    )
    )
    fig.update_layout(yaxis_visible=False)

    return fig

def user_pathways_levels(date_clause, segment = None, link_prune_val=50):
    data = get_math_event_data('question_level', segment=segment, date_clause=date_clause)
    fig = sankey_chart(data, max_rank=5, link_prune_val=link_prune_val)
    return fig

def user_pathways_lessons(date_clause, level, segment=None, link_prune_val=50):
    data = get_math_event_data('question_micro_lesson',
                               date_clause=date_clause,
                               segment=segment,
                               level=level)
    fig = sankey_chart(data, max_rank=5, link_prune_val=link_prune_val)
    return fig



