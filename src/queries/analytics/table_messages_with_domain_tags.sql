/* 
CONTEXT: 
- Generates a table where messages are joined with content tags
RESULT EXPECTATION
- A table with messages augmented by tag information when available
*/

SELECT  
  messages.addressees as addressees,
  messages.author_type as author_type,
  messages.card_uuid as card_uuid,
  messages.content as content,
  messages.direction as direction,
  messages.inserted_at as inserted_at, 
  messages.chat_id as chat_id,
  messages.in_reply_to as in_reply_to,
  messages.uuid as uuid,
  domain_tag_map.tag_name as domain_tag,

FROM {{variables.tables.messages_table}} as messages 
LEFT JOIN
(
  SELECT card_tag_map.card_uuid as card_uuid,
          domain_tags.id as tag_id,
          domain_tags.tag_name as tag_name
            
    FROM
    (
      {{variables.tables.content_tag_map}} as card_tag_map
      inner join
      (SELECT * FROM {{variables.tables.tag_table}} where tag_category = 'domain_tag')as domain_tags
      ON card_tag_map.tag_id = domain_tags.id
    )
   
) as domain_tag_map
ON messages.card_uuid = domain_tag_map.card_uuid