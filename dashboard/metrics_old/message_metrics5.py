import json
from datetime import datetime

import pandas as pd

MAX_WEEK = 25  # max value for x axis
MAX_STUDENT = float('inf')


def calculate_metrics(file_name, start_date, stop_date):
    df = pd.read_csv(file_name, usecols=["id", "sec_addr", "short_inserted_at"])
    df["short_inserted_at"] = df["short_inserted_at"].apply(
        lambda x: datetime.strptime(x, "%Y-%m-%d").date()
    )

    all_students = []

    mask = (df["short_inserted_at"] > start_date.date()) & (df["short_inserted_at"] <= stop_date.date())
    all_df = df[mask]
    agg_addr_df = all_df.groupby(['sec_addr', 'short_inserted_at']).agg({'id': ['count']}).reset_index()

    if agg_addr_df.empty:
        return {
            'average_engaged_days': 0,
            'final_df': pd.DataFrame(
                [],
                columns=["student", "number_of_engaged_days"]
            ),
        }

    for i, student in enumerate(agg_addr_df[('sec_addr', '')].unique()):
        mask = (agg_addr_df[('sec_addr', '')] == student) & (agg_addr_df[('id', 'count')] >= 10)
        masked_df = agg_addr_df[mask]
        engaged_days = masked_df[('id', 'count')].to_list()
        number_of_engaged_days = len(engaged_days)

        all_students.append({
            "student": student,
            "number_of_engaged_days": number_of_engaged_days
        })
        if not i % 500:
            print(i)

    all_students_df = pd.DataFrame.from_records(all_students)
    del all_students

    total_students = all_students_df["student"].count()
    total_engaged_days = all_students_df["number_of_engaged_days"].sum()
    average_engaged_days = total_engaged_days / total_students if total_students else 0

    all_students_df.to_csv("data/message_metrics5.csv", index=False),
    del all_students_df

    indicators = {
        "total_engaged_days": f"{total_engaged_days:,}",
        "total_students": f"{total_students:,}",
        "average_engaged_days": f"{average_engaged_days:,.2f}",
    }

    with open('data/message_metrics5_indicators.json', 'w') as file:
        json.dump(indicators, file)


if __name__ == "__main__":
    start_date_ = datetime(2022, 8, 1)
    stop_date_ = datetime.now()
    calculate_metrics(
        file_name="data/lifetime.csv",
        start_date=start_date_,
        stop_date=stop_date_,
    )
