"""Standalone script"""

import os
import time
import pandas as pd

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()

from dashboard.etl.utils import get_message_records_batch, etl_mqa_records
from dashboard.models import MathQuestionAnswerModel
from django.db.utils import IntegrityError
from pytz import UTC
import json


BATCH_SIZE=3000

def insert_records_to_mqa_with_model(result):
    """
    Turns `message` records into `math_question_answer` records and inserts into the database using Django ORM
    :param result: The records from the `message` table that need to be processed"
    """
    if not len(result):
        return 0
    processed_df = etl_mqa_records(result)
    print("Inserting rows to Supabase")

    rows = []
    for i, row in processed_df.iterrows():
        row_dict = dict(row)
        row_model = MathQuestionAnswerModel(**row_dict)
        rows.append(row_model)
        if i % 50 == 0 or i == (len(processed_df) - 1):
            try:
                MathQuestionAnswerModel.objects.bulk_create(rows)
            except IntegrityError:
                print("Duplicate record detected")
                for individual_row in rows:
                    try:
                        individual_row.save()
                    except IntegrityError as e:
                        # Handle individual insert error (e.g., log the error)
                        print(f"Individual insert failed: {e}")
            print(f"Inserted: {len(rows)} rows. Time: {time.asctime()}!")
            rows.clear()
    return len(processed_df)


if __name__ == "__main__":
    batch = get_message_records_batch(BATCH_SIZE)
    num_rows = insert_records_to_mqa_with_model(batch)
    while num_rows > 0:
        batch = get_message_records_batch(BATCH_SIZE)
        num_rows = insert_records_to_mqa_with_model(batch)
