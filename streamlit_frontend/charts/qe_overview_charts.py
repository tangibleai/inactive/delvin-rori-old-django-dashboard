import pandas as pd
import random
import plotly.express as px

from streamlit_frontend.src.const import *

COL_TOTAL_ANSWERS = "total_answers"
COL_NUM_USERS = "num_users"
COL_CORRECT_ANSWERS = "success_answers"
COL_WRONG_ANSWERS = "failure_answers"
COL_HINT_REQUESTS = "hint_requests"

def calc_popular_lessons(data, grouping_column, res_column_name, small_value_threshold, apply_small_value_threshold=False):
    df = data.set_index(grouping_column)
    df_result = pd.DataFrame({
        res_column_name: df[COL_NUM_USERS]})
    df_result.reset_index(inplace=True)
    if apply_small_value_threshold:
        df_result = df_result[df_result[res_column_name] > small_value_threshold]

    return df_result


def calc_hints_percent_by_lesson(data, grouping_column, res_column_name, small_value_threshold, apply_small_value_threshold=False):
    df = data.set_index(grouping_column)
    df_result = pd.DataFrame({
        'Hint Requests': df[COL_HINT_REQUESTS],
        'Total Answers': df[COL_TOTAL_ANSWERS]
    })
    df_result = df_result[df_result['Hint Requests'] > 0]

    if apply_small_value_threshold:
        df_result = df_result[df_result['Total Answers'] > small_value_threshold]

    df_result.reset_index(inplace=True)
    df_result[res_column_name] = df_result['Hint Requests'] / df_result['Total Answers']
    return df_result

def calc_failure_percent(data, grouping_column, res_column_name, small_value_threshold, apply_small_value_threshold=False):
    df = data.set_index(grouping_column)
    df["total"] = df[COL_CORRECT_ANSWERS]+df[COL_WRONG_ANSWERS]
    df_result = pd.DataFrame({
        'Wrong Answers': df[COL_WRONG_ANSWERS],
        'Total Answers': df["total"]
    })
    df_result = df_result[df_result['Total Answers'] > 0]

    if apply_small_value_threshold:
        df_result = df_result[df_result['Total Answers'] > small_value_threshold]

    df_result.reset_index(inplace=True)
    df_result[res_column_name] = df_result['Wrong Answers'] / df_result['Total Answers']
    df_result.dropna(inplace=True)
    df_result[res_column_name] = round(df_result[res_column_name]*100,3)
    return df_result



class ChartType:
    def __init__(self, chart_title, chart_desc, calc_func, res_column_name,
                 x_label, y_label, additional_hover_data, small_value_threshold=None,small_value_note=None):
        self.chart_title = chart_title
        self.chart_desc = chart_desc
        self.calc_func = calc_func
        self.res_column_name = res_column_name
        self.x_label = x_label
        self.y_label = y_label
        self.additional_hover_data = additional_hover_data
        self.small_value_threshold = small_value_threshold
        self.small_value_note = small_value_note

    def transform_data(self, data, grouping_column, apply_small_value_threshold=False):
        return self.calc_func(data, grouping_column,
                              res_column_name=self.res_column_name,
                              small_value_threshold=self.small_value_threshold,
                              apply_small_value_threshold=apply_small_value_threshold)



OVERVIEW_CHART_TYPES = {
    "Popular Micro-Lessons": ChartType(
        chart_title='Popular Micro-Lessons',
        chart_desc='Lessons in which most(least) students participated by answering at least one question.',
        calc_func=calc_popular_lessons,
        res_column_name='students_engaged',
        x_label='Micro-Lesson',
        y_label='Number of Students Engaged',
        additional_hover_data=[],
        small_value_threshold=100,
        small_value_note='lessons with under 100 answers'),
    "Hints in Micro-Lessons": ChartType(
        chart_title="Hints in Micro-Lessons",
        chart_desc='Lessons by ratio of hint requests to total number of student answers',
        calc_func=calc_hints_percent_by_lesson,
        res_column_name='hint_ratio',
        x_label='Micro-Lesson',
        y_label='Hint Request Ratio',
        additional_hover_data=['Hint Requests', 'Total Answers'],
        small_value_threshold=100,
        small_value_note='lessons with under 100 answers'),
    "Difficult Micro-Lessons": ChartType(
        chart_title="Difficult Micro-Lessons",
        chart_desc='Lessons by % of wrong math answers to total number of student answers',
        calc_func=calc_failure_percent,
        res_column_name='failure_ratio',
        x_label='Micro-Lesson',
        y_label='Wrong Answer Ratio',
        additional_hover_data=['Wrong Answers', 'Total Answers'],
        small_value_threshold=100,
        small_value_note='lessons with under 100 answers'),
"Difficult Questions": ChartType(
        chart_title="Difficult Questions",
        chart_desc='Individual questions by ratio of wrong math answers to total number of student answers',
        calc_func=calc_failure_percent,
        res_column_name='failure_ratio',
        x_label='Question + Micro Lesson',
        y_label='Wrong Answer Ratio',
        additional_hover_data=['Wrong Answers', 'Total Answers'],
        small_value_threshold=10,
        small_value_note='questions with less than 10 answers')
}


def random_color_sequence(hue=None):
    if hue == 'red':
        ans = (random.randint(180, 255), random.randint(32, 140), random.randint(32, 140))
    elif hue == 'blue':
        ans = random.randint(32, 140), random.randint(32, 140), random.randint(180, 255)
    else:
        ans = random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
    return 'rgb' + str(ans)


def horizontal_bar_chart(data, x, y, x_label,y_label, color_hue, descending = False, additional_hover_data = [], num_bars=20):
    fig = px.bar(data.head(num_bars), x=y, y=x,
                 orientation='h',
                 text=y,
                 hover_data=[y] + additional_hover_data,
                 labels={x: x_label, y: y_label},
                 color_discrete_sequence=[random_color_sequence(color_hue)],
                 height = num_bars*50+50)

    # Customize the layout
    fig.update_layout(
        yaxis=dict(showgrid=True, gridcolor='lightgray')
    )

    if descending:
        fig.update_layout(
            yaxis=dict( autorange='reversed')
        )

    fig.update_traces(textposition='inside')
    return fig