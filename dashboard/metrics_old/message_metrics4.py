import json
import time
from datetime import datetime, timedelta

import pandas as pd

MAX_WEEK = 25  # max value for x axis
MAX_STUDENT = float('inf')


def calculate_metrics(file_name, start_date, stop_date):
    df = pd.read_csv(file_name, usecols=["id", "sec_addr", "short_inserted_at"])
    df["short_inserted_at"] = df["short_inserted_at"].apply(
        lambda x: datetime.strptime(x, "%Y-%m-%d").date()
    )

    # for graph x is limited to MAX_WEEK parameter. But indicator calculation is based on
    # all values. all_students list is for indicator calculation.
    all_students = []

    mask = (df["short_inserted_at"] > start_date.date()) & (df["short_inserted_at"] <= stop_date.date())
    all_df = df[mask]

    # reset_index to list all columns
    agg_addr_df = all_df.groupby(['sec_addr', 'short_inserted_at']).agg({'id': ['count']}).reset_index()

    if agg_addr_df.empty:
        return {
            'total_messages': 0,
            'total_students': 0,
            'total_active_days': 0,
            'average_by_students': 0,
            'average_by_days': 0,
            'final_df': pd.DataFrame(
                [],
                columns=["Students", "Message count", "Active days", "Average messages"]
            ),
            'all_messages_df': pd.DataFrame([], columns=["Message"]),
        }

    # create an empty dataframe
    # final_df = pd.DataFrame()
    final_dict = dict()
    for i, student in enumerate(agg_addr_df[('sec_addr', '')].unique()):
        weekly_messages = []

        current_day = agg_addr_df[('short_inserted_at', '')][
            agg_addr_df[('sec_addr', '')] == student].min()

        while current_day < stop_date.date():
            window_stop = current_day + timedelta(days=7)
            mask = (agg_addr_df[('sec_addr', '')] == student) & \
                   (agg_addr_df[('short_inserted_at', '')] >= current_day) & \
                   (agg_addr_df[('short_inserted_at', '')] < window_stop)
            window_df = agg_addr_df[mask]
            weekly_messages.append(window_df[('id', 'count')].sum())
            current_day = window_stop

        # From first message week till today, calculating total messages and total weeks per student
        # Example: [63, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        message_sent = sum(weekly_messages)  # number of messages sent by student
        message_week = len(weekly_messages)  # from first message week till today, total weeks

        average_per_week = round(message_sent / message_week, 2) if message_week else 0

        all_students.append({
            "Student": student,
            "Message sent": message_sent,
            "Message week": message_week,
            "Average messages": average_per_week,
        })

        final_dict.update({i: weekly_messages[:MAX_WEEK]})

        if not i % 100:
            print(f"i:{i}, time:{time.asctime()}")
        # to limit calculations
        if i >= MAX_STUDENT:
            break

    final_df = pd.DataFrame(dict([(key, pd.Series(value)) for key, value in final_dict.items()]))
    del final_dict
    final_df.fillna(0.0, inplace=True)
    final_df.to_csv("data/message_metrics4.csv", index=False)
    del final_df

    # make the list length 25 with a parameter
    # y axis max value to 25, x axis max value 25.

    all_students_df = pd.DataFrame.from_records(all_students)

    total_messages = all_students_df['Message sent'].sum()
    total_students = all_students_df['Student'].count()
    total_weeks = all_students_df['Message week'].sum()
    average_by_students = total_messages / total_students if total_students else 0
    average_by_weeks = total_messages / total_weeks if total_weeks else 0

    del all_students_df

    indicators = {
        "total_messages": f"{total_messages:,}",
        "total_students": f"{total_students:,}",
        "total_weeks": f"{total_weeks:,}",
        "average_by_students": f"{average_by_students:,.2f}",
        "average_by_weeks": f"{average_by_weeks:,.2f}",
    }

    with open('data/message_metrics4_indicators.json', 'w') as file:
        json.dump(indicators, file)


if __name__ == "__main__":
    start_date_ = datetime(2022, 8, 1)
    stop_date_ = datetime.now()
    calculate_metrics(
        file_name="data/lifetime.csv",
        start_date=start_date_,
        stop_date=stop_date_,
    )
