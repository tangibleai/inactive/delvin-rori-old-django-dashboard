import pandas as pd
from apiclient import discovery
from google.oauth2 import service_account

"""To access
https://denisluiz.medium.com/python-with-google-sheets-service-account-step-by-step-8f74c26ed28e
Enable `Google Sheets API` on the project
Create a service acc key for project
Download the json key
Read the email from the key and share the `sheet` with this email.
"""


class SheetsAPI:
    """Google sheet API"""

    @staticmethod
    def create_service(secret_file, scopes):
        """Returns a Google API service"""
        try:
            credentials = service_account.Credentials.from_service_account_file(
                filename=secret_file,
                scopes=scopes,
            )
            service = discovery.build('sheets', 'v4', credentials=credentials)
            return service

        except OSError as e:
            print(e)

    @staticmethod
    def get_sheet_names(service, spreadsheet_id):
        """Returns sheet names from the spreadsheet"""

        # The ranges to retrieve from the spreadsheet.
        ranges = []

        # This parameter is ignored if a field mask was set in the request.
        include_grid_data = False

        request = service.spreadsheets().get(
            spreadsheetId=spreadsheet_id,
            ranges=ranges,
            includeGridData=include_grid_data,
        )
        response = request.execute()

        sheet_metadata = response.get('sheets')

        sheet_names = []
        for item in sheet_metadata:
            sheet_names.append(item.get('properties').get('title'))
        return sheet_names

    @staticmethod
    def get_sheet_data(service, spreadsheet_id, range_name):
        """Returns sheet data from the spreadsheet by sheet name"""

        rows = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=range_name).execute()
        data = rows.get('values')
        df = pd.DataFrame(data)
        return df
