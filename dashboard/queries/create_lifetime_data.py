import pandas as pd
from dotenv import load_dotenv
from google.cloud import bigquery

# To set google application credentials file name
load_dotenv()

import datetime


def get_df_from_gbq(start_date, stop_date, max_delta, batch_size, sql_query, output_file):
    client = bigquery.Client()

    ranged_start_date = start_date - datetime.timedelta(days=max_delta)

    query_parameters = [
        bigquery.ScalarQueryParameter("start_date", "DATETIME", ranged_start_date),
        bigquery.ScalarQueryParameter("stop_date", "DATETIME", stop_date),
    ]

    job_config = bigquery.QueryJobConfig(
        query_parameters=query_parameters,
    )

    query_job = client.query(sql_query, job_config=job_config)

    # df = query_job.to_dataframe()
    result = query_job.result()

    result_list = []
    for i, row in enumerate(result):
        result_list.append(dict(row))
        if not i % batch_size:
            if not i:
                partial_df = pd.DataFrame.from_records(result_list)
                partial_df["short_inserted_at"] = partial_df["inserted_at"].apply(
                    lambda x: x.date()
                )
                partial_df.to_csv(output_file, mode="w", index=False)
            else:
                partial_df = pd.DataFrame.from_records(result_list)
                partial_df["short_inserted_at"] = partial_df["inserted_at"].apply(
                    lambda x: x.date()
                )
                partial_df.to_csv(output_file, mode="a", index=False, header=False)
            result_list = []

    partial_df = pd.DataFrame.from_records(result_list)
    partial_df["short_inserted_at"] = partial_df["inserted_at"].apply(
        lambda x: x.date()
    )
    partial_df.to_csv(output_file, mode="a", index=False, header=False)

    message = f"{query_job.total_bytes_processed // 1_000_000} MB processed!"

    return message


if __name__ == "__main__":
    stop_date_ = datetime.datetime.now()
    start_date_ = stop_date_ - datetime.timedelta(days=9999)

    # multi id for each object change (not used)
    sql_query1 = """
    select msg.id, to_hex(sha256(msg.from_addr)) as sec_addr, msg.inserted_at,
    from `rori-turn.12065906259.messages` as msg
    where direction = 'inbound' and msg.inserted_at >= @start_date and msg.inserted_at <= @stop_date; 
    """

    # grouping by id and selecting unique id for calculations
    sql_query2 = """
    select msg.id as id, count(msg.id) as cid, to_hex(sha256(max(msg.from_addr))) as sec_addr, max(msg.inserted_at) as inserted_at,
    from `rori-turn.12065906259.messages` as msg
    where direction = 'inbound' and msg.inserted_at >= @start_date and msg.inserted_at <= @stop_date
    group by msg.id
    order by msg.id asc;
    """

    output_file_ = "data/lifetime.csv"

    max_delta_ = 30
    batch_size_ = 1000
    print("Starting download from GBQ")
    r = get_df_from_gbq(start_date_, stop_date_, max_delta_, batch_size_, sql_query2, output_file_)
    print(r)
