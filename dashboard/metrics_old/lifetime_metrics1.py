from collections import Counter
from datetime import timedelta, datetime

import pandas as pd


def activity_metrics(file_name, start_date, stop_date, delta):
    df = pd.read_csv(file_name, usecols=["sec_addr", "short_inserted_at"])
    df["short_inserted_at"] = df["short_inserted_at"].apply(
        lambda x: datetime.strptime(x, "%Y-%m-%d").date()
    )
    # for each day we only need one record
    df.drop_duplicates(keep='first', inplace=True)

    final_list = []
    current_date = start_date.date()

    while current_date <= stop_date.date():
        active_students = []
        dormant_students = []
        new_students = []
        super_students = []

        mask = df["short_inserted_at"] == current_date
        last_df = df[mask]
        last_count = Counter(last_df["sec_addr"].to_list())

        mask = df["short_inserted_at"] <= current_date
        all_df = df[mask]
        all_count = Counter(all_df["sec_addr"].to_list())

        date_range_start = current_date - timedelta(days=delta)
        mask = (df["short_inserted_at"] > date_range_start) & (df["short_inserted_at"] <= current_date)
        window_df = df[mask]
        windows_count = Counter(window_df["sec_addr"].to_list())

        for student in all_count.keys():
            student_count = windows_count.get(student, 0)

            if student_count >= 5:
                super_students.append(student)
            elif student_count >= 1 and not last_count.get(student, 0):
                active_students.append(student)
            elif last_count.get(student, 0) != 0:
                new_students.append(student)
            else:
                dormant_students.append(student)

        final_list.append({
            "Date": current_date,
            "Active": len(active_students),
            "New": len(new_students),
            "Dormant": len(dormant_students),
            "Super": len(super_students),
            "All": len(all_count),
        })

        current_date = current_date + timedelta(days=1)

    del df

    save_file_name = f"data/lifetime_metrics1.csv"
    final_df = pd.DataFrame.from_records(final_list)
    final_df.to_csv(save_file_name, index=False)


if __name__ == "__main__":
    start_date_ = datetime(2022, 8, 1)
    stop_date_ = datetime.now()
    delta_ = [30]  # start from 30 days back for calculation
    for d in delta_:
        activity_metrics(
            file_name="data/lifetime.csv",
            start_date=start_date_,
            stop_date=stop_date_,
            delta=d,
        )
