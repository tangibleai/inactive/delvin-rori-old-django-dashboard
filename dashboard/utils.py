import base64
import os
from datetime import datetime, timedelta
from io import BytesIO

import matplotlib.pyplot as plt
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
import seaborn as sns
from django.db import connections
from google.cloud import bigquery

# Define timedelta in days for queries
TIME_DELTA = 30
PROD_SUPABASE = "supabase-db-rori"


def bulk_load_from_supabase(db_name, query):
    with connections[db_name].cursor() as cursor:
        cursor.execute(query)
        columns = [col[0] for col in cursor.description]
        batch_data = [
            # returns column name and the value as dict
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
        if not batch_data:
            return []
        return batch_data


def select_from_supabase(db_name, query):
    # Connect and get data from supabase as dictionary
    with connections[db_name].cursor() as cursor:
        cursor.execute(query)
        # returns column names
        if not cursor.description:
            return

        columns = [col[0] for col in cursor.description]

        return [
            # returns column name and the value as dict
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]


def execute_many_on_supabase(db_name, query, data_list):
    # Connect and execute many on Supabase
    with connections[db_name].cursor() as cursor:
        cursor.executemany(query, data_list)


def get_from_bigquery(query):
    # Define variable for the google
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.environ.get("BIG_QUERY_KEY")

    # Connect and get data from bigquery as dictionary
    data = []

    with bigquery.Client() as client:
        query_job = client.query(query)

        for row in query_job.result():
            data.append(dict(row.items()))

    return data


def draw_bar_plotly_html(
    df, title, x, y, color_discrete_map, barmode="group", showlegend=True
):
    """Pass a dataframe and column names
    Draw a line chart with plotly, return as html data.
    """

    fig = px.bar(
        data_frame=df,
        x=x,
        y=y,
        title=title,
        width=1600,
        height=600,
        color_discrete_map=color_discrete_map,
    )

    fig.update_layout(
        barmode=barmode,
        showlegend=showlegend,
    )

    return fig.to_html(full_html=False)


def draw_line_plotly_html(df, title, x, y, showlegend=True):
    """Pass a dataframe and column names
    Draw a line chart with plotly, return as html data.
    """

    fig = px.line(
        data_frame=df,
        x=x,
        y=y,
        title=title,
        width=1600,
        height=600,
        markers=True,
    )

    fig.update_layout(showlegend=showlegend)

    return fig.to_html(full_html=False)


def draw_table_plotly_html(df, title, columns):
    """Pass a dataframe and column names
    Create a table with plotly, return as html data.
    """

    cells = [df[column] for column in columns]

    fig = go.Figure(
        layout=dict(title=title),
        data=[
            go.Table(
                header=dict(values=columns, align="left"),
                cells=dict(values=cells, align="left"),
            )
        ],
    )
    fig.update_layout(width=1600, height=600)
    return fig.to_html(full_html=False)


def draw_histogram_plotly(
    df, title, x, bin_start, bin_end, bin_size, y_axis_title="Frequency"
):
    """Pass a dataframe and column names
    Draw a histogram chart with plotly, return as html data.
    """

    fig = px.histogram(
        data_frame=df,
        x=x,
        title=title,
        width=1600,
        height=600,
        # labels={x: "next_text"},  # to update labels
    )

    fig.update_traces(
        xbins=dict(  # bins used for histogram
            start=bin_start, end=bin_end, size=bin_size
        )
    )

    fig.update_layout(yaxis_title=y_axis_title)

    return fig.to_html(full_html=False)


def get_line_sns(df, x, y, title):
    """Pass a dataframe and column names
    Draw a line chart with sns, return as image data.
    """
    plt.switch_backend("AGG")
    plt.figure(figsize=(32, 6))
    sns.set_style("whitegrid")
    for col in y:
        print(col)
        sns.lineplot(df, x=x, y=df[col], markers=True).set(title=title)
    buffer = BytesIO()
    plt.savefig(buffer, format="png")
    image_png = buffer.getvalue()
    graph = base64.b64encode(image_png).decode("utf-8")
    buffer.close()
    return graph


def draw_line_plotly(df, title, x, y):
    """Pass a dataframe and column names
    Draw a line chart with plotly, return as image data.
    """
    if df.empty:
        df[x] = []
        df[y] = []

    fig = px.line(
        data_frame=df,
        x=x,
        y=y,
        title=title,
        width=1600,
        height=600,
        markers=True,
    )
    fig_png = fig.to_image(format="png")  # kaleido library

    buffer = BytesIO()
    buffer.write(fig_png)
    image_png = buffer.getvalue()
    graph = base64.b64encode(image_png).decode("utf-8")
    buffer.close()
    return graph


def draw_multiline_plotly(
    df,
    x,
    y,
    x_lim,
    y_lim,
    y_min,
    y_max,
    y_step,
    figsize,
    title,
    xlabel,
    ylabel,
    fontsize,
    color,
    alpha,
):
    """Returns a multiline chart as image data string.
    x: column name of x axis.
    y: column names in dataframe as list of strings"""
    plt.figure(figsize=figsize)
    plt.yticks(np.arange(y_min, y_max, y_step))
    plt.title(title, fontsize=fontsize)
    plt.xlabel(xlabel, fontsize=fontsize)
    plt.ylabel(ylabel, fontsize=fontsize)
    ax = plt.gca()
    ax.set_xlim([0, x_lim])
    ax.set_ylim([0, y_lim])
    x = df[x]
    for i, c in enumerate(y):
        """for each column in dataframe plots a line"""
        plt.plot(
            x,
            df[c],
            color=color,
            alpha=alpha,
        )

    buffer = BytesIO()
    plt.savefig(buffer, format="png")
    image_png = buffer.getvalue()
    image_data = base64.b64encode(image_png).decode("utf-8")
    buffer.close()
    return image_data


def validate_date(date_string):
    """Validate a string if it's a date"""
    try:
        date = datetime.strptime(date_string, "%Y-%m-%d")
    except ValueError:
        return None
    except TypeError:
        return None
    return date


def define_dates():
    """Define a start and stop data with the given timedelta"""
    stop_date = datetime.now()
    start_date = stop_date - timedelta(days=TIME_DELTA)
    return start_date, stop_date


def png_to_str(file_path):
    """Converts a png image to html string to render"""
    with open(file_path, "rb") as image2string:
        converted_string = base64.b64encode(image2string.read()).decode("utf-8")
    return converted_string


def image_str_to_file(img_string, file_name="image.png"):
    """Write an image data to file"""
    with open(file_name, mode="wb") as file:
        file.write(base64.urlsafe_b64decode(img_string))


class SupabaseCreateUpdateETL:
    @staticmethod
    def insert_or_update_by_row(df, model):
        """Creates or updates records in Supabase as row"""

        for i, row in df.iterrows():
            row_dict = dict(row)
            updated_row = model.objects.filter(id=row["id"]).update(**row_dict)
            if updated_row:
                print("creating:", updated_row)
            if not updated_row:
                # if not exists, create new
                model.objects.create(**row_dict)
                print("Creating new row!")

    @staticmethod
    def insert_or_update_bulk(df, model, unique_fields, update_fields):
        """Creates or updates records in Supabase as bulk"""

        rows = []
        for i, row in df.iterrows():
            row_dict = dict(row)
            rows.append(model(**row_dict))
            if i and not i % 1000:
                model.objects.bulk_create(
                    rows,
                    update_conflicts=True,
                    unique_fields=unique_fields,
                    update_fields=update_fields,
                )
                print(f"{len(rows)} created or updated!")
                rows.clear()
        model.objects.bulk_create(
            rows,
            update_conflicts=True,
            unique_fields=unique_fields,
            update_fields=update_fields,
        )
        print(f"{len(rows)} created or updated!")
        rows.clear()


def delete_file_by_path(path):
    try:
        os.remove(path)
    except FileNotFoundError as err:
        print(err)


def add_content_to_file(path, text):
    with open(path, mode="a") as file:
        file.write(text)
