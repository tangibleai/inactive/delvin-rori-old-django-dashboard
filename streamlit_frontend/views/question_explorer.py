
from datetime import datetime
import numpy as np
import streamlit as st

from streamlit_frontend.src.etl import get_data_for_qe_selectors
from streamlit_frontend.views.components.q_explorer import lesson_view, level_topic_view, overview, question_view
from streamlit_frontend.src.const import *
from streamlit_frontend.src.sql_utils import WhereClause

def load_page():
    col_page_title, col_line_selector = st.columns([5, 1])
    with col_page_title:
        st.title("Rori Question Explorer")

    with col_line_selector:
        select_line = st.selectbox("Rori line/s", ['Both Lines', 'Open Line Only', 'Rising Line Only'])

    where_clause = WhereClause()

    # choosing the date
    spacer_col_1, col_start_date, col_end_date, spacer_col_2, download_col = st.columns([1, 1, 1, 1, 1])

    with col_start_date:
        start_date = st.date_input("Choose start date", value=datetime(2023,9,1), min_value=datetime(2023, 5, 16))

    with col_end_date:
        end_date = st.date_input("Choose end date", min_value=datetime(2023, 5, 16))

    if start_date and end_date:
        where_clause = WhereClause()
        where_clause.add_datelimit_subclause(FN_INSERTED_AT, start_date, end_date )

    if select_line == 'Rising Line Only':
        where_clause.add_int_subclause(field=FN_LINE_NUM,value=RORI_RISING_LINE)
    elif select_line == 'Open Line Only':
        where_clause.add_int_subclause(field=FN_LINE_NUM, value=RORI_OPEN_LINE,)

    # with download_col:
    #     if st.button('Export Data:'):
    #         csv = export_df(q_df)
    #         st.download_button(
    #             label="Download data as CSV",
    #             data=csv,
    #             file_name='math_question_answers.csv',
    #             mime='text/csv',
    #         )

    # Level/Topic/Skill/Lesson selectors
    q_df = get_data_for_qe_selectors(where_clause)

    level_options = ('Overview',) + tuple(['Level ' + str(level) for level in np.sort(q_df[FN_LEVEL].unique())])
    level_raw = st.selectbox("Select a level", level_options)

    if level_raw == 'Overview':
        overview.display_component(where_clause)

    else:
        level = int(level_raw[-1])

        col_topic, col_skill = st.columns([1, 1])
        with col_topic:
            topic_names = q_df[q_df[FN_LEVEL] == level][FN_TOPIC].unique()
            toptions = ("",) + tuple(topic_names)
            topic = st.selectbox("Choose a topic (optional)", toptions)

        with col_skill:
            soptions = tuple()
            if topic:
                skill_names = q_df[(q_df[FN_LEVEL] == level) & (q_df[FN_TOPIC] == topic)][FN_SKILL].unique()
                soptions = ("",) + tuple(skill_names)
            skill = st.selectbox("Choose a skill (optional)", soptions)

        level_df = q_df[q_df[FN_LEVEL] == level]
        level_clause = WhereClause(where_clause)
        level_clause.add_int_subclause(field=FN_LEVEL, value=level)

        if topic:
            level_df = level_df[level_df[FN_TOPIC] == topic]
            level_clause.add_str_subclause(field=FN_TOPIC,value=topic)
        if skill:
            level_df = level_df[level_df[FN_SKILL] == skill]
            level_clause.add_str_subclause(field=FN_SKILL,value=skill)

        lesson = ""
        question = ""
        col_lesson, col_question = st.columns([1, 1])
        with col_lesson:
            lesson_names = np.sort(level_df[FN_LESSON].unique())
            loptions = ("All",) + tuple(lesson_names)
            lesson = st.selectbox("Choose a lesson", loptions)

        with col_question:
            qoptions = tuple()
            if lesson != "All":
                qoptions = np.sort(level_df[level_df[FN_LESSON] == lesson][FN_QUESTION_NUM].unique())
                qoptions = ("Lesson Overview",) + tuple(qoptions)
            question = st.selectbox("Choose a question", qoptions)

        st.write("----")

        ## Specific Question Chart
        if lesson != "All" and question != "Lesson Overview":
            question_view.display_component(level_clause, lesson, question)

        # Lesson overview charts
        elif lesson != "All" and question == "Lesson Overview":
            lesson_view.display_component(level_clause, lesson)

        # If only level/skill/topic is chosen, shows the completion ratio of lessons for that level
        else:
            level_topic_view.display_component(level_clause)



