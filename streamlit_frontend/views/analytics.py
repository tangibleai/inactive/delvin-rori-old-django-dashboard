import streamlit as st
from datetime import datetime, date
import numpy as np

from streamlit_frontend.charts.analytics_charts import \
    display_student_metric,\
    user_acquisition_funnel, \
    user_pathways_levels, \
    user_pathways_lessons

from dashboard.metrics.user_features import run_custom_query
from streamlit_frontend.views.components import double_chart
from streamlit_frontend.views.components.analytics import student_progress
from streamlit_frontend.src.sql_utils import WhereClause
from streamlit_frontend.src.const import MATH_USER_SEGMENTS, ALL_USER_SEGMENTS

def load_page():
    st.write("## User Analysis (Open line only)")
    _, col_stud_joined, col_start_date, col_end_date, _ = st.columns([1,2, 1, 1, 1])


    with col_stud_joined:
        st.write('View Students that Joined')
        st.write('#')

    with col_start_date:
        start_date = st.date_input("From", value=datetime(2023, 5, 16), min_value=datetime(2023, 1, 13))

    if start_date < date(2023,5,16):
        st.warning('Warning: Date range includes dates for which math data is not available. Full math data logging started on May 16th, 2023. Math-related charts might be inaccurate or missing', icon="⚠️")


    with col_end_date:
        end_date = st.date_input("To", min_value=datetime(2023, 5, 16))

    # choose comparison date range
    _, col_stud_joined_c, col_start_date_c, col_end_date_c, _= st.columns([1, 2, 1, 1, 1])

    with col_stud_joined_c:
        to_compare = st.checkbox('Compare to date range:')

    if to_compare:
        with col_start_date_c:
            start_date_c = st.date_input("From", value=datetime(2023, 8, 1), min_value=datetime(2023, 1, 13), key="from_comp")

        if start_date_c < date(2023, 5, 16):
            st.warning(
                'Warning: Date range includes dates for which math data is not available. Full math data logging started on May 16th, 2023. Math-related charts might be inaccurate or missing',
                icon="⚠️")

        with col_end_date_c:
            end_date_c = st.date_input("To", value=datetime(2023, 8, 31), min_value=datetime(2023, 5, 16), key="to_comp")

    if start_date < date(2023,5,16):
        st.warning('Warning: Date range includes dates for which math data is not available. Full math data logging started on May 16th, 2023, Math-related charts might be inaccurate or missing', icon="⚠️")

    date_clause = WhereClause()
    date_clause.add_datelimit_subclause('inserted_at', start_date, end_date)

    if to_compare:
        date_clause_c = WhereClause()
        date_clause_c.add_datelimit_subclause('inserted_at', start_date_c, end_date_c)

    tab_acquisition, tab_progress, tab_pathways = \
        st.tabs(['User Acquisition', 'User Progress', 'User Pathways'])

    with tab_acquisition:
        st.write("### User Acquisition Funnel")

        include_segments = st.multiselect(label="Choose User Segments", options=ALL_USER_SEGMENTS, default=ALL_USER_SEGMENTS)

        if to_compare:
            double_chart.show_double_chart(date_clause,
                                           date_clause_c,
                                           user_acquisition_funnel,
                                           kwargs=dict(
                                               include_segments=include_segments
                                           ))
        else:
            fig = user_acquisition_funnel(date_clause, include_segments=include_segments)
            st.plotly_chart(fig)

    with tab_progress:
        if to_compare:
            student_progress.display_component(date_clause, to_compare, date_clause_c)

        else:
            student_progress.display_component(date_clause)


    with tab_pathways:

        st.write("## Pathways Between Math Levels")
        _, col_prune, col_segment, col_compute, _ = st.columns([1,2,2,2,1])
        with col_prune:
            lev_link_prune_num = st.number_input(label="Prune links of less than",
                                             value=50,
                                             key='an_path_lev_prune')
        with col_segment:
            segments = ['All'] + MATH_USER_SEGMENTS
            lev_segment_select = st.selectbox('User Segment',
                                              segments,
                                              key='an_path_lev_seg')
            if lev_segment_select == 'All':
                lev_segment_select = None

        with col_compute:
            st.write("######")
            compute_pushed = st.button("Calculate", key="an_path_compute")

        if compute_pushed:
            fig = user_pathways_levels(date_clause,
                                       link_prune_val=int(lev_link_prune_num),
                                       segment=lev_segment_select)
            st.plotly_chart(fig, use_container_width=True)
            compute_pushed = False


        st.write("## Pathways Between Lessons")
        _, les_col_prune, les_col_segment, les_col_level, les_col_compute, _ = st.columns([1, 2,2, 2,2, 1])
        with les_col_prune:
            les_link_prune_num = st.number_input(label="Prune links of less than",
                                                 value=50,
                                                 key='an_path_les_prune')
        with les_col_segment:
            segments = ['All'] + MATH_USER_SEGMENTS
            les_segment_select = st.selectbox('User Segment',
                                              segments,
                                              key='an_path_les_seg')
            if les_segment_select == 'All':
                les_segment_select = None

        with les_col_level:
            level_options = range(1,10)
            level = st.selectbox("Choose a level", level_options)

        with les_col_compute:
            st.write("######")
            les_compute_pushed = st.button("Calculate", key="an_path_les_compute")

        if les_compute_pushed:
            fig = user_pathways_lessons(date_clause,
                                        level,
                                        link_prune_val=int(les_link_prune_num),
                                        segment=les_segment_select)
            st.plotly_chart(fig, use_container_width=True)
            les_compute_pushed = False
