import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from dashboard.utils import select_from_supabase
import pandas as pd
import time
from dashboard.models import UserEngagement1Model


def calculate_metrics():
    print("Selecting data from Supabase!")

    _db_name = "supabase-db-rori"
    # Query string to select data from Supabase with Django connection
    query = "select id, contact_uuid, date(inserted_at) as date_inserted_at " \
            "from django_delvin_messages " \
            "order by contact_uuid;"

    query_result = select_from_supabase(db_name=_db_name, query=query)

    df = pd.DataFrame.from_records(query_result)

    print("Starting calculations!")

    agg_addr_df = df.groupby('contact_uuid').agg({'date_inserted_at': ['min', 'max'], 'id': ['count']})

    agg_addr_df["Lifetime"] = agg_addr_df.apply(
        lambda row: (row[('date_inserted_at', 'max')] - row[('date_inserted_at', 'min')]).days + 1,
        axis=1
    )
    agg_addr_df.drop(
        columns=[('date_inserted_at', 'max'), ('date_inserted_at', 'min')],
        inplace=True
    )

    all_users = []
    for user in agg_addr_df.index.to_list():
        lifetime = int(agg_addr_df.loc[user, ('Lifetime', '')])
        message_sent = int(agg_addr_df.loc[user, ('id', 'count')])
        average_messages = round(message_sent / lifetime, 2) if lifetime else 0
        item = {
            "contact_uuid": user,
            "message_sent": message_sent,
            "lifetime": lifetime,
            "average_messages": average_messages,
        }
        all_users.append(item)

    print("Deleting data!")
    # Delete all old data
    query = "delete from user_engagement_data1;"
    select_from_supabase(db_name=_db_name, query=query)

    print("Inserting data!")
    slice_size = 1000
    while all_users:
        part, all_users = all_users[:slice_size], all_users[slice_size:]
        part_to_models = [UserEngagement1Model(**row) for row in part]
        UserEngagement1Model.objects.bulk_create(part_to_models)
        print(f"Inserted {len(part)} rows. Time: {time.asctime()}!")


if __name__ == "__main__":
    calculate_metrics()
