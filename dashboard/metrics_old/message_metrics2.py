import json
from datetime import datetime

import pandas as pd


def calculate_metrics(file_name, start_date, stop_date):
    df = pd.read_csv(file_name, usecols=["id", "sec_addr", "short_inserted_at"])
    df["short_inserted_at"] = df["short_inserted_at"].apply(
        lambda x: datetime.strptime(x, "%Y-%m-%d").date()
    )

    all_students = []

    mask = (df["short_inserted_at"] > start_date.date()) & (df["short_inserted_at"] <= stop_date.date())
    all_df = df[mask]
    agg_addr_df = all_df.groupby('sec_addr').agg({'short_inserted_at': ['min', 'max'], 'id': ['count']})

    if agg_addr_df.empty:
        return {
            'total_messages': 0,
            'total_students': 0,
            'total_lifetime': 0,
            'average_by_students': 0,
            'average_by_days': 0,
            'final_df': pd.DataFrame(
                [],
                columns=["Students", "Message count", "Lifetime", "Average messages"]
            ),
        }

    agg_addr_df["Lifetime"] = agg_addr_df.apply(
        lambda row: (row[('short_inserted_at', 'max')] - row[('short_inserted_at', 'min')]).days + 1,
        axis=1
    )
    agg_addr_df.drop(
        columns=[('short_inserted_at', 'max'), ('short_inserted_at', 'min')],
        inplace=True
    )

    for student in agg_addr_df.index.to_list():
        lifetime = agg_addr_df.loc[student, ('Lifetime', '')]
        message_sent = agg_addr_df.loc[student, ('id', 'count')]
        average_messages = round(message_sent / lifetime, 2) if lifetime else 0
        all_students.append({
            "Student": student,
            "Message sent": message_sent,
            "Lifetime": lifetime,
            "Average messages": average_messages,
        })

    final_df = pd.DataFrame.from_records(all_students)

    total_messages = final_df['Message sent'].sum()
    total_students = final_df['Student'].count()
    total_lifetime = final_df['Lifetime'].sum()
    average_by_students = total_messages / total_students if total_students else 0
    average_by_lifetime = total_messages / total_lifetime if total_lifetime else 0

    final_df.to_csv("data/message_metrics2.csv", index=False),

    indicators = {
        "total_messages": f"{total_messages:,}",
        "total_students": f"{total_students:,}",
        "total_lifetime": f"{total_lifetime:,}",
        "average_by_students": f"{average_by_students:,.2f}",
        "average_by_lifetime": f"{average_by_lifetime:,.2f}",
    }

    with open('data/message_metrics2_indicators.json', 'w') as file:
        json.dump(indicators, file)


if __name__ == "__main__":
    start_date_ = datetime(2022, 8, 1)
    stop_date_ = datetime.now()
    calculate_metrics(
        file_name="data/lifetime.csv",
        start_date=start_date_,
        stop_date=stop_date_,
    )
