import pandas as pd
import numpy as np
import plotly.graph_objects as go

from streamlit_frontend.src.etl import get_max_question_table

CORRECT_ANSWER = 0.0
WRONG_ANSWER = 1.0
OTHER_ANSWER = 2.0
COMPLETE_LESSON = 3.0
SWITCH_LESSON = 4.0
END_SESSION = 5.0
QUIT_RORI = 6.0


def discrete_colorscale(bvals, colors):
    """
    bvals - list of values bounding intervals/ranges of interest
    colors - list of rgb or hex colorcodes for values in [bvals[k], bvals[k+1]],0<=k < len(bvals)-1
    returns the plotly  discrete colorscale
    """
    if len(bvals) != len(colors) + 1:
        raise ValueError('len(boundary values) should be equal to  len(colors)+1')
    bvals = sorted(bvals)
    nvals = [(v - bvals[0]) / (bvals[-1] - bvals[0]) for v in bvals]  # normalized values

    dcolorscale = []  # discrete colorscale
    for k in range(len(colors)):
        dcolorscale.extend([[nvals[k], colors[k]], [nvals[k + 1], colors[k]]])
    return dcolorscale

def color_code_activity(row):
    if row['session'] != row['next_session']:
        return END_SESSION
    elif (int(row['question_number'])== row['last_question'] and
           row['question_micro_lesson'] != row['next_micro_lesson'] ):
        return COMPLETE_LESSON
    elif (row['answer_type']=='menu' or row['answer_type']=='next'):
        return SWITCH_LESSON
    elif (row['answer_type']=='success'):
        return CORRECT_ANSWER
    elif (row['answer_type']=='failure' or row['answer_type']=='hint'):
        return WRONG_ANSWER
    else:
        return OTHER_ANSWER

def student_activity_chart(stud_data):
    # Compute sessions
    df = stud_data
    df['message_inserted_at'] = pd.to_datetime(df['message_inserted_at'])
    df['time_diff'] = df['message_inserted_at'].diff().fillna(pd.Timedelta(seconds=0))
     # Identify where a new session begins (time difference greater than 1 hour)
    df['new_session'] = df['time_diff'].gt(pd.Timedelta(hours=1))
     # Assign session_id using the cumulative sum of 'new_session'
    df['session'] = df['new_session'].cumsum().astype(int)
     # Clean up intermediate columns
    df = df.drop(columns=['time_diff', 'new_session'])

     # Shift session by 1 for next session
    df['next_session'] = df['session'].shift(-1).fillna('-1').apply(int)

     # Compute next micro lesson
    df['next_micro_lesson'] = df['question_micro_lesson'].shift(-1)


     # Get last question of every microlesson
    last_question_df = get_max_question_table()
    df = df.merge(last_question_df, on='question_micro_lesson', how='left')
    df = df.fillna('-1')
    df['last_question'] = df['last_question'].apply(int)

    df['action_code'] = df.apply(lambda row: color_code_activity(row), axis=1)
    df.at[df.index[-1], 'action_code']  = QUIT_RORI

    bvalues = [CORRECT_ANSWER,
               WRONG_ANSWER,
               OTHER_ANSWER,
               COMPLETE_LESSON,
               SWITCH_LESSON,
               END_SESSION,
               QUIT_RORI,
               QUIT_RORI]
    colors = ['rgb(187,212,166)',
              'rgb(219,67,37)',
              'rgb(200,200,200)',
              'rgb(0,97,100)',
              'rgb(237,162,71)',
              'rgb(255,255,255)',
              'rgb(0,0,0)']

    colorscale = discrete_colorscale(bvalues, colors)
    bvals = np.array(bvalues)
    tickvals = [np.mean(bvals[k:k + 2]) for k in
                 range(len(bvals) - 1)]  # position with respect to bvals where ticktext is displayed
    ticktext = ['Correct Answer',
                'Wrong Answer',
                'Other Answer',
                'Completed Lesson',
                'Switched Lesson',
                'Ended Session',
                'Quit Rori']

    data_to_plot = pd.DataFrame(df[['action_code', 'question_micro_lesson']])
    data_to_plot['y'] = 0

    fig = go.Figure(data=go.Heatmap(
         z=data_to_plot['action_code'],
         y=data_to_plot['y'],
         x=data_to_plot.index,
         colorscale=colorscale,
         colorbar=dict(thickness=25,
                       tickvals=tickvals,
                       ticktext=ticktext),
         hoverinfo='text',
         text=data_to_plot['question_micro_lesson']
         )
     )
    fig.update_layout(yaxis_visible=False)
    fig.update_layout(xaxis_visible=False)
    fig.update_layout(autosize=False,
    height=250,margin = dict(l=20, r=20, t=20, b=20))

    return fig

def student_lesson_table(stud_data):
    df = stud_data
    grouped = df.groupby('lesson_skill_topic').agg(
        first_message=pd.NamedAgg(column='message_inserted_at', aggfunc='min'),
        num_questions=pd.NamedAgg(column='question_number', aggfunc='nunique'),
        success_count=pd.NamedAgg(column='answer_type', aggfunc=lambda x: (x == 'success').sum()),
        failure_count=pd.NamedAgg(column='answer_type', aggfunc=lambda x: (x == 'failure').sum())
    )

    grouped['accuracy'] = grouped['success_count'] / (grouped['success_count'] + grouped['failure_count'])
    grouped['accuracy'] = grouped['accuracy'].fillna(0)

    result = grouped[['num_questions','first_message', 'accuracy']].reset_index()
    result = result.sort_values(by=['first_message'])

    def style_accuracy(s):
        return ['background-color: {}'.format(
            'rgb(146,197,222)' if v > 0.75 else
            'rgb(244,165,130)' if v > 0.5 else
            'rgb(202,0,32)' if v > 0.05 else
            'rgb(247,247,247)') for v in s]

    def style_num_questions(s):
        return ['background-color: {}'.format(
            'rgb(155,191,133)' if v >= 10 else
            'rgb(246,211,232)' if 5 <= v <= 9 else
            'rgb(179,88,154)') for v in s]

    styled_result = result.style.apply(style_accuracy, subset=['accuracy']).apply(style_num_questions,
                                                                                  subset=['num_questions'])

    return styled_result

