/*
CONTEXT:
The `cards` table in turn.io includes all the edits to the card db record. We want to use only the most recent one.
EXPECTED RESULT:
Returns the list of `cards` records, where every record the most recent one among cards with the same uuid
*/

CREATE OR REPLACE TABLE {{variable.tables.content_unique}} AS
  (
    SELECT
      uuid
      ,title
      ,content
      ,language
      ,updated_at
    FROM
      (
      SELECT
      uuid
      ,title
      ,content
      ,language
      ,updated_at
      --order by updated_at
      ,ROW_NUMBER() OVER (PARTITION BY uuid ORDER BY updated_at DESC) AS rownumber
      FROM
      {{variables.tables.content_table}}
      WHERE
      --add more conditions here if needed 
      is_deleted IS false
      )
    WHERE
    rownumber = 1
  )