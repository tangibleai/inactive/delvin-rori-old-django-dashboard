import pandas as pd

from dashboard.utils import draw_bar_plotly_html, select_from_supabase


def get_context_by_dates(start_date, stop_date):
    _db_name = "supabase-db-rori"

    # Get metrics for active, dormant and super users.
    df_fail = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select question_micro_lesson, count(original_message_id) as failure "
              "from math_question_answer "
              "where answer_type = 'failure' "
              "group by question_micro_lesson;"
    ))

    df_success = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select question_micro_lesson, count(original_message_id) as success "
              "from math_question_answer "
              "where answer_type = 'success' "
              "group by question_micro_lesson;"
    ))

    df_all = pd.merge(df_success, df_fail, on="question_micro_lesson")
    df_all.columns = [item.replace("_", " ").capitalize() for item in df_all.columns]

    df_all["Success rate"] = df_all.apply(lambda row: row["Success"] / (row["Failure"] + row["Success"]), axis=1)
    df_all["Failure rate"] = df_all.apply(lambda row: row["Failure"] / (row["Failure"] + row["Success"]), axis=1)

    success_chart = draw_bar_plotly_html(df=df_all,
                                         title="Success chart",
                                         x='Question micro lesson',
                                         y=["Success rate"],
                                         color_discrete_map={},
                                         barmode="stack")

    failure_chart = draw_bar_plotly_html(df=df_all,
                                         title="Failure chart",
                                         x='Question micro lesson',
                                         y=["Failure rate"],
                                         color_discrete_map={"Failure rate": "Red"},
                                         barmode="stack")

    all_chart = draw_bar_plotly_html(df=df_all,
                                     title="All chart",
                                     x='Question micro lesson',
                                     y=["Success rate", "Failure rate"],
                                     color_discrete_map={"Success rate": "Blue", "Failure rate": "Red"},
                                     barmode="group")
    del df_all

    # Create a context to return to the view
    context = {
        "dates": {"start_date": start_date.strftime("%Y-%m-%d"), "stop_date": stop_date.strftime("%Y-%m-%d")},
        "indicators": [
        ],
        "charts": [
            {"chart": success_chart},
            {"chart": failure_chart},
            {"chart": all_chart},
        ],
    }

    return context
