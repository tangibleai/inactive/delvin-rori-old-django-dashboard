"""Standalone script"""

import os
import time

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

import pandas as pd
import numpy as np
from google.cloud import bigquery
from dashboard.utils import select_from_supabase, execute_many_on_supabase
from dashboard.models import BQUserModel
from pytz import UTC
import json


class SupabaseETL:
    def __init__(self):
        self.supabase_db_name = 'supabase-db-rori'

    def get_records_from_gbq(self):

        sql_query = """
         SELECT
             uuid
            ,details
            ,inserted_at
            FROM
              `rori-turn.12065906259.contacts`
            QUALIFY (ROW_NUMBER() OVER (PARTITION BY id ORDER BY updated_at DESC))=1
        """
        client = bigquery.Client()

        query_job = client.query(sql_query)

        result = query_job.to_dataframe()

        return result

    def process_data(self, result):
        df_details = result['details'].map(json.loads)
        df_details = pd.json_normalize(df_details)
        df_full = result.merge(df_details, left_index=True, right_index=True)
        df_full = df_full.rename(columns={'uuid':'contact_uuid'})

        cols_to_select = ['contact_uuid',
                           'inserted_at',
                          'chat_per_week',
                          'current_activity',
                          'current_question',
                          'first_impression',
                          'grade_level',
                          'guardian_consent',
                           'name',
                           'onboarding_complete',
                           'opted_in',
                          'welcome_stack',
                           'helpdesk_mode',
                           'finished_math',
                           'age']

        df_partial = df_full[cols_to_select]
        df_partial = df_partial.replace(np.nan, None)
        df_partial = df_partial.replace('', None)
        return df_partial


    def insert_records_to_supabase_with_model(self):
        # By using GBQ query result object, inserts the data to supabase
        result = self.get_records_from_gbq()

        # delete everything before putting in the new users
        query = "delete from bq_contacts;"
        select_from_supabase(db_name=self.supabase_db_name, query=query)

        processed_df = self.process_data(result)

        print("Inserting rows to Supabase")

        rows = []
        for i, row in processed_df.iterrows():
            row_dict = dict(row)
            # making timezone aware to avoid warnings.
            row_dict["inserted_at"] = row_dict["inserted_at"].replace(tzinfo=UTC)
            row_model = BQUserModel(**row_dict)
            rows.append(row_model)
            if i % 5000 == 0:
                BQUserModel.objects.bulk_create(rows)
                print(f"Inserted: {len(rows)} rows. Time: {time.asctime()}!")
                rows.clear()
        BQUserModel.objects.bulk_create(rows)
        print(f"Inserted: {len(rows)} rows. Time: {time.asctime()}!")
        rows.clear()


if __name__ == "__main__":
    s = SupabaseETL()
    s.insert_records_to_supabase_with_model()