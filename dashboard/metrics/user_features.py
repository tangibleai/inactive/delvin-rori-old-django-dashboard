import datetime
import os
import pandas as pd
import functools
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()

from dashboard.utils import select_from_supabase

supabase_db_name = "supabase-db-rori"


def run_custom_query(query):
    query_result = select_from_supabase(db_name=supabase_db_name, query=query)
    return pd.DataFrame.from_records(query_result)


def user_count_distinct(feature_name, count_column, table_name):
    query = f"""
            select contact_uuid, count (distinct {count_column}) as {feature_name} from {table_name}
            group by contact_uuid 
            """
    return run_custom_query(query)


def select_from_users_table(columns,inserted_after=datetime.date(2023,1,1), inserted_before=datetime.datetime.now()):
    columns_joined = ",".join(columns)
    query = f"""
            select contact_uuid, {columns_joined} from bq_contacts
            where inserted_at >='{inserted_after.strftime('%Y-%m-%d')}'
                and inserted_at <= '{inserted_before.strftime('%Y-%m-%d')}'
        """
    query_result = select_from_supabase(db_name=supabase_db_name, query=query)
    df = pd.DataFrame.from_records(query_result)
    df['inserted_at'] = pd.to_datetime(df['inserted_at'])
    return pd.DataFrame.from_records(query_result)

# ---------------------------------------------FEATURES-----------------------------------------------------------------

def num_messages_feature():
    return user_count_distinct(feature_name='num_messages',
                             count_column='id',
                        table_name='bq_messages')


def num_active_days_feature():
    return user_count_distinct(feature_name="num_active_days",
                               count_column="date(inserted_at)",
                               table_name='bq_messages')


def num_active_weeks_feature():
    user_weeks = run_custom_query(
        query = """
                 SELECT 
                  contact_uuid, 
                  retention_week,
                  count (distinct id) as num_messages
                  FROM
                  (
                    SELECT 
                      id,
                      contact_uuid, 
                      (EXTRACT(days from (inserted_at - first_message_timestamp)) / 7)::int AS retention_week
                    FROM
                    (SELECT id, contact_uuid, inserted_at, MIN(inserted_at) over (PARTITION BY contact_uuid) AS first_message_timestamp
                    FROM bq_messages
                    ) as messages
                  ) as messages_with_ret_week
                GROUP BY contact_uuid, retention_week;
                """
    )
    weeks_per_user = user_weeks.groupby('contact_uuid')['retention_week'].nunique()
    weeks_per_user = weeks_per_user.reset_index().rename(columns={'retention_week': 'num_active_weeks'})
    return weeks_per_user

def num_math_messages_feature():
    return run_custom_query("""
        select
          contact_uuid,
          count(distinct id) as num_math_messages
        from
          bq_messages
        where
          flow_name = 'Math Quiz'
        group by
          contact_uuid;
    """)

def num_lessons_visited_feature():
    return user_count_distinct(
        feature_name='num_lessons_visited',
        count_column='question_micro_lesson',
        table_name='math_question_answer'
    )


def num_lessons_completed_feature(feature_name='num_lessons_completed'):
    return run_custom_query(
        f"""
            SELECT contact_uuid, COUNT(*) AS {feature_name}
            FROM (
                SELECT contact_uuid, question_micro_lesson
                FROM math_question_answer
                GROUP BY contact_uuid, question_micro_lesson
                HAVING COUNT(DISTINCT question_number) >= 10
            ) AS CompletedLessons
            GROUP BY contact_uuid;
        """
    )

def num_topics_visited_feature():
    return user_count_distinct(
        feature_name='num_topics_visited',
        count_column='topic',
        table_name='math_question_answer'
    )

def average_accuracy_rate_feature():
    df = run_custom_query("select contact_uuid, answer_type, supabase_id from math_question_answer")
    accuracy_rate_per_user = (df[df['answer_type'] == 'success'].groupby('contact_uuid')['answer_type'].count() /
                              df[(df['answer_type']== 'success') | (df['answer_type']=='failure')].groupby('contact_uuid')['answer_type'].count())
    return pd.DataFrame(accuracy_rate_per_user).reset_index().rename(columns={'answer_type':
                                                                              'average_accuracy_rate'})


def average_questions_per_lesson_feature():
    pass

def onboarding_stage_feature():
    def calculate_onboarding_stage(group):
        if 'Math Quiz' in group['flow_name'].values:
            return 'Math Quiz'
        elif group['flow_name'].str.contains('Level').any():
            return 'Level Menu'
        elif 'Onboarding_AF' in group['flow_name'].values:
            return 'Onboarding_AF'
        elif 'Registration_AF' in group['flow_name'].values:
            return 'Registration_AF'
        else:
            return None

    df = run_custom_query("""
        select contact_uuid, flow_name, min(inserted_at) as min_time from bq_messages
        where direction='outbound' and inserted_at>'2023-01-13'
        group by contact_uuid, flow_name
        order by contact_uuid, min(inserted_at)
        """)

    onboarding_df = df.groupby('contact_uuid').apply(calculate_onboarding_stage).reset_index()
    onboarding_df.columns = ['contact_uuid', 'onboarding_stage']
    return onboarding_df


def last_activity_feature():

    def clean_last_activity(row):
        if row['flow_name'] is None:
            return "Automation Message"
        elif 'Level' in row['flow_name']:
            return 'Level_Menu'
        elif 'Intro_' in row['flow_name']:
            return 'Intro'
        else:
            return row['flow_name']


    df = run_custom_query("""
    SELECT 
        contact_uuid,
        flow_name
    FROM (
        SELECT
            contact_uuid,
            flow_name,
            ROW_NUMBER() OVER (PARTITION BY contact_uuid ORDER BY inserted_at DESC) AS rn
        FROM
            bq_messages
        WHERE 
            direction='outbound'
        ) AS subquery
    WHERE 
        rn = 1
        """)

    feature_series = df.apply(lambda row: clean_last_activity(row), axis=1)
    feature_df = pd.DataFrame(df['contact_uuid'])
    feature_df['last_activity'] = feature_series
    return feature_df

def num_sessions_feature():
    query = """
            SELECT 
                contact_uuid,
                SUM(
                    CASE WHEN
                        EXTRACT(EPOCH FROM (inserted_at - prev_inserted_at)) > 3600
                    THEN 1
                    ELSE 0
                    END
                )+1 AS num_sessions
            FROM (
                SELECT
                    contact_uuid,
                    inserted_at,
                    LAG(inserted_at) OVER (PARTITION BY contact_uuid ORDER BY inserted_at) AS prev_inserted_at
                FROM
                    bq_messages
                WHERE 
                    flow_name='Math Quiz'
                    AND inserted_at >'2023-01-13'
            ) AS subquery
            GROUP BY 
                contact_uuid;
            """

    return run_custom_query(query)


#------------------------Secondary Features---------------------------------------------
def user_segment_sec_feature(user_data):
    def calculate_user_segment(row):
        if row['num_math_messages'] > 0:
            if row['num_sessions'] > 1:
                if row['num_sessions'] > 5:
                    return '4 - Persisted for >5 Math Sessions'
                else:
                    return '3 - Returned for 2-5 Math Sessions'
            else:
                return '2 - Engaged in 1 Math Session'
        else:
            if row['onboarding_stage'] == 'Registration_AF':
                return '1b - Started Registration'
            elif row['onboarding_stage'] == 'Onboarding_AF':
                return '1c - Started Onboarding'
            elif row['onboarding_stage'] == 'Level Menu':
                return '1d - Finished Onboarding'
            else:
                return '1a - Subscribed'

    feature_series = user_data.apply(lambda row: calculate_user_segment(row), axis=1)
    feature_df = pd.DataFrame(user_data['contact_uuid'])
    feature_df['user_segment'] = feature_series
    return feature_df








