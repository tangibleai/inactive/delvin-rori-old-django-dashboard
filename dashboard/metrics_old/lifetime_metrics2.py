import json
from datetime import timedelta, datetime

import pandas as pd


def lifetime_metrics_detailed(file_name, start_date, stop_date, delta):
    df = pd.read_csv(file_name, usecols=["sec_addr", "short_inserted_at"])
    df["short_inserted_at"] = df["short_inserted_at"].apply(
        lambda x: datetime.strptime(x, "%Y-%m-%d").date()
    )
    df.drop_duplicates(keep='first', inplace=True)

    active_students = []
    dormant_students = []
    all_students = []

    mask = (df["short_inserted_at"] > start_date.date()) & (df["short_inserted_at"] <= stop_date.date())
    all_df = df[mask]
    agg_all_df = all_df.groupby('sec_addr').agg({'short_inserted_at': ['min', 'max']})

    if agg_all_df.empty:
        return {
            "active_students": pd.DataFrame.from_records({"Lifetime": active_students}),
            "dormant_students": pd.DataFrame.from_records({"Lifetime": dormant_students}),
            "all_students": pd.DataFrame.from_records({"Lifetime": all_students}),
            "nbins_active": 200,
            "nbins_dormant": 200,
            "nbins_all": 200,
        }

    agg_all_df["Lifetime"] = agg_all_df.apply(
        lambda row: (row[('short_inserted_at', 'max')] - row[('short_inserted_at', 'min')]).days + 1,
        axis=1
    )

    date_range_start = stop_date - timedelta(days=delta)
    mask = (df["short_inserted_at"] > date_range_start.date()) & (df["short_inserted_at"] <= stop_date.date())
    windows_df = df[mask]
    window_students = windows_df["sec_addr"].unique()

    for student in agg_all_df.index.to_list():
        lifetime = agg_all_df.loc[student, "Lifetime"].values[0]
        if student in window_students:
            active_students.append(lifetime)
        if student not in window_students:
            dormant_students.append(lifetime)
        all_students.append(lifetime)

    avg_active = sum(active_students) / len(active_students) if len(active_students) else 0
    avg_dormant = sum(dormant_students) / len(dormant_students) if len(dormant_students) else 0
    avg_all = sum(all_students) / len(all_students) if len(all_students) else 0

    pd.DataFrame \
        .from_records({"User lifetime in days": active_students}) \
        .to_csv("data/lifetime_metrics2_active_students.csv", index=False),
    pd.DataFrame \
        .from_records({"User lifetime in days": dormant_students}) \
        .to_csv("data/lifetime_metrics2_dormant_students.csv", index=False)
    pd.DataFrame \
        .from_records({"User lifetime in days": all_students}) \
        .to_csv("data/lifetime_metrics2_all_students.csv", index=False)

    indicators = {
        "avg_active": f"{avg_active:,.2f}",
        "avg_dormant": f"{avg_dormant:,.2f}",
        "avg_all": f"{avg_all:,.2f}",
        "total_active": f"{len(active_students):,}",
        "total_dormant": f"{len(dormant_students):,}",
        "total_all": f"{len(all_students):,}"
    }

    with open('data/lifetime_metrics2_indicators.json', 'w') as file:
        json.dump(indicators, file)


if __name__ == "__main__":
    start_date_ = datetime(2022, 8, 1)
    stop_date_ = datetime.now()
    delta_ = 30  # start from 30 days back for calculation
    lifetime_metrics_detailed(
        file_name="data/lifetime.csv",
        start_date=start_date_,
        stop_date=stop_date_,
        delta=delta_,
    )
