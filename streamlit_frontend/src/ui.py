menu_icons = {
        "Analytics": "bar-chart-fill",
        "Students": "mortarboard-fill",
        "Math Questions": "infinity",
        "Conversations": "chat-dots-fill",
        "Natural Language": "translation"
}