import yaml
import os, dotenv

dotenv.load_dotenv()


class AppConfig():
    '''
    Reponsible for loading the configuration file
    '''

    def __init__(self):
        self.env = {}
        for key in os.environ:
            self.env[key] = os.environ.get(key)

        with open('delvin_config.yaml', 'r') as file:
            self.config = yaml.safe_load(file)

    def get_apps(self):
        return self.config['apps']

    def get_analytics_dashboards(self):
        return self.config['dashboards']

    def get_bq_details(self):
        return (self.env["BQ_PROJECT_ID"],
                self.env["BQ_DATASET"])

    def is_bq_data_source(self):
        return self.env["DATA_SOURCE_TYPE"]

    def get_project_title(self):
        return self.config["project_title"]

    def get_project_logo(self):
        return self.config["project_logo"]

    def get_embedder_type(self):
        return self.config["user_queries"]["embedder"]

