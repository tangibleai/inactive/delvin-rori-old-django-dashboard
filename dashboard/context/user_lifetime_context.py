import pandas as pd

from dashboard.utils import draw_histogram_plotly, select_from_supabase


def get_context(start_date, stop_date):
    _db_name = "supabase-db-rori"

    # Get metrics for active, dormant and super users.
    df_all = pd.DataFrame.from_records(select_from_supabase(
        db_name="supabase-db-rori",
        query="select user_lifetime_in_days from lifetime_user_data"
    ))
    avg_all = df_all["user_lifetime_in_days"].mean()
    df_all.columns = [item.replace("_", " ").capitalize() for item in df_all.columns]
    min_all, max_all, bin_width_all = df_all.min().iloc[0], df_all.max().iloc[0], 5
    all_users_hist = draw_histogram_plotly(df=df_all, title=f"All users lifetime (bin width: {bin_width_all})",
                                           x="User lifetime in days",
                                           bin_start=min_all, bin_end=max_all, bin_size=bin_width_all)
    del df_all

    df_active = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select user_lifetime_in_days from lifetime_user_data where user_type = 'active_users'"
    ))
    avg_active = df_active["user_lifetime_in_days"].mean()
    df_active.columns = [item.replace("_", " ").capitalize() for item in df_active.columns]
    min_active, max_active, bin_width_active = df_active.min().iloc[0], df_active.max().iloc[0], 5
    active_users_hist = draw_histogram_plotly(df=df_active,
                                              title=f"Active users lifetime (bin width: {bin_width_active})",
                                              x="User lifetime in days",
                                              bin_start=min_active, bin_end=max_active, bin_size=bin_width_active)
    del df_active

    df_dormant = pd.DataFrame.from_records(select_from_supabase(
        db_name=_db_name,
        query="select user_lifetime_in_days from lifetime_user_data where user_type = 'dormant_users'"
    ))
    avg_dormant = df_dormant["user_lifetime_in_days"].mean()
    df_dormant.columns = [item.replace("_", " ").capitalize() for item in df_dormant.columns]
    min_dormant, max_dormant, bin_width_dormant = df_dormant.min().iloc[0], df_dormant.max().iloc[0], 5
    dormant_users_hist = draw_histogram_plotly(df=df_dormant,
                                               title=f"Dormant users lifetime (bin width: {bin_width_dormant})",
                                               x="User lifetime in days",
                                               bin_start=min_dormant, bin_end=max_dormant, bin_size=bin_width_dormant)
    del df_dormant

    all_users_count = select_from_supabase(
        db_name=_db_name,
        query="select value from django_delvin_messages_indicators where name = 'all_users_count';"
    )[0]["value"]

    context = {
        "dates": {"start_date": start_date.strftime("%Y-%m-%d"), "stop_date": stop_date.strftime("%Y-%m-%d")},
        "indicators": [
            {"label": "All students", "value": f"{all_users_count:,.0f}"},
            {"label": "AVG active user lifetime", "value": f"{avg_active:.2f}"},
            {"label": "AVG dormant user lifetime", "value": f"{avg_dormant:.2f}"},
            {"label": "AVG all users lifetime", "value": f"{avg_all:.2f}"},
        ],
        "charts": [
            {"chart": all_users_hist, },
            {"chart": active_users_hist},
            {"chart": dormant_users_hist},
        ],
    }

    return context
