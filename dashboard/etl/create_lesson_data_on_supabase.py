"""Standalone script"""

import os
import re

import django
import pandas as pd

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()
import time
from pathlib import Path

from dashboard.models import MicrolessonModel
from scripts.sheets_api_operations import SheetsAPI
from dashboard.utils import SupabaseCreateUpdateETL, delete_file_by_path, add_content_to_file

# Define base directory
BASE_DIR = Path(__file__).resolve().parent.parent.parent

# Define scopes to make API work
scopes = ['https://www.googleapis.com/auth/drive.readonly']

# secret file should be on the project root
# secret_file = os.path.join(BASE_DIR, 'cetin-service-acc-secret.json')  # cetin
secret_file = os.path.join(BASE_DIR, 'rori-bigquery-service-acc-secret.json')  # rori

# The ID of the spreadsheet to work
# spreadsheet_id = '1jieIFmnf17czyIGceJbPW2D6nw4wAgnetWtW8ELJmyU'  # cetin
# spreadsheet_id = '1GYzrGxuOmS7ZSmeqtYrw0k8jWU01XIVKBv5qTY3YiEs'  # rori-staging
spreadsheet_id = '11Am5Vf9oKWO62wo-EdyJp60uMm-w20pcPjoFNBN5__k'  # rori-prod





def is_microlesson(text):
    """This function helps to understand if a sheet is microlesson.
    There are other sheets that are not microlesson.
    Returns True for pattern match, else False.
    Text sample: G9.A3.4.1.1
    """
    pattern = r"[A-Z]+[0-9]+[.][A-Z]+[0-9]+[.][0-9]+[.][0-9]+[.][0-9]"
    match = re.match(pattern, text)
    return True if match else False


def is_question(text):
    """Returns True for pattern match, else False.
    Text sample: Q1
    """
    pattern = r"[Q][0-9]+"
    match = re.match(pattern, text)
    return True if match else False


def process_microlesson_df(df, sheet_name):
    """Processes microlesson to format it before DB operations"""
    df['question_microlesson'] = sheet_name
    df['question_number'] = df[2].apply(lambda x: x[1:])
    df['question'] = df[3]
    df['expected_answer'] = df[4]
    df['id'] = df.apply(lambda row: f"{row['question_microlesson']}_{row['question_number']}", axis=1)
    new_df = df[['id', 'question_microlesson', 'question_number', 'question', 'expected_answer']]
    return new_df


def main():
    """Fetches a Google sheet from drive and saves the result to the Supabase, use the correct secret_file, scope and spreadsheet_id!
    While saving to Supabase it is using the database defined in the `dashboard/dbrouters.py`, check it carefully!
    """
    path = f"{BASE_DIR}/create_lesson_data.log"
    delete_file_by_path(path)

    s = SheetsAPI()
    service = s.create_service(secret_file=secret_file, scopes=scopes)
    sheet_names = s.get_sheet_names(service=service, spreadsheet_id=spreadsheet_id)
    time.sleep(1)
    for sheet_name in sheet_names:
        print(sheet_name)
        if is_microlesson(sheet_name):
            try:
                msg = f"Processing {sheet_name}\n"
                add_content_to_file(path, msg)
                df = s.get_sheet_data(service=service, spreadsheet_id=spreadsheet_id, range_name=sheet_name)
                df = process_microlesson_df(df, sheet_name)
                unique_fields = ['id']
                update_fields = df.columns.to_list()
                update_fields.remove('id')  # removing ID from update fields, since its unique
            except Exception as err:
                msg = f"Format might be wrong for {sheet_name}\n{err.__class__}\n{err}\n"
                add_content_to_file(path, msg)
            else:
                try:
                    msg = f"Updating {sheet_name} on Supabase!\n"
                    add_content_to_file(path, msg)
                    e = SupabaseCreateUpdateETL()
                    e.insert_or_update_bulk(df, MicrolessonModel, unique_fields, update_fields)
                except Exception as err:
                    msg = f"Error while updating Supabase {sheet_name}\n{err.__class__}\n{err}\n"
                    add_content_to_file(path, msg)
        else:
            msg = f"Sheet {sheet_name} is not a microlesson!\n"
            add_content_to_file(path, msg)
        time.sleep(1)


if __name__ == '__main__':
    main()
