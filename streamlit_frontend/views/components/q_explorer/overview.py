import streamlit as st
from streamlit_plotly_events import plotly_events

from streamlit_frontend.charts.qe_overview_charts import OVERVIEW_CHART_TYPES, horizontal_bar_chart
from streamlit_frontend.src.etl import get_question_metrics_data, get_lesson_metrics
from streamlit_frontend.src.const import *

def display_component(where_clause):
    st.write('----')
    st.write('### Create Overview Chart')

    ## Widgets for customizing the chart
    most_least_col, chart_type_col, nb_col, lnp_col = st.columns([1, 2, 2, 2])
    with lnp_col:
        lesson_name_pres = st.selectbox(label="Microlesson Chart Labels",
                                        options=('Lesson Name Only', 'Lesson + Topic + Skill'))

    with most_least_col:
        most_or_least = st.radio(label='Most or least?', options=('Most', 'Least'))
    with chart_type_col:
        ct_options = tuple(OVERVIEW_CHART_TYPES.keys())
        chart_type_select = st.selectbox("Choose chart type", options=ct_options)
    with nb_col:
        num_bars_select = st.slider("Choose number of bars", min_value=5, max_value=50, value=10)

    exclude_small_values_select = st.checkbox(
        "Exclude microlessons with <100 answers / questions with < 10 answers")

    st.write('----')


    if chart_type_select == 'Difficult Questions':
        data = get_question_metrics_data(where_clause)
        grouping_column = 'question_and_lesson'
    else:
        data = get_lesson_metrics(where_clause)
        data['lesson_skill_topic'] = data[FN_LESSON] + ' (' + data[FN_TOPIC] + ' - ' + data[FN_SKILL] + ')'
        grouping_column = 'question_micro_lesson' if lesson_name_pres == 'Lesson Name Only' else 'lesson_skill_topic'


    chart_type = OVERVIEW_CHART_TYPES[chart_type_select]

    results = chart_type.transform_data(data, grouping_column,
                                        apply_small_value_threshold=exclude_small_values_select)

    ascending = True if most_or_least == 'Least' else False
    color_hue = 'red' if most_or_least == 'Least' else 'blue'
    chart_title = most_or_least + ' ' + chart_type.chart_title

    results = results.sort_values(chart_type.res_column_name, ascending=ascending)

    fig = horizontal_bar_chart(data=results,
                               num_bars=num_bars_select,
                               x=grouping_column,
                               y=chart_type.res_column_name,
                               x_label=chart_type.x_label,
                               y_label=chart_type.y_label,
                               color_hue=color_hue,
                               descending=not ascending,
                               additional_hover_data=chart_type.additional_hover_data
                               )

    st.write(f"### {chart_type.chart_title}")
    st.write(f"{chart_type.chart_desc}")

    st.plotly_chart(fig)

