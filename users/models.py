from django.db import models
from django.contrib.auth.admin import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bot_name = models.CharField(max_length=100, null=True, blank=True)

    # image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def __str__(self):
        return f"{self.user.username} Profile"

    # override the method to add avatar
    # def save(self, *args, **kwargs):
    #     super().save(*args, **kwargs)

        # img = Image.open(self.image.path)

        # if img.height > 300 or img.width > 300:
        #     output_size = (300, 300)
        #     img.thumbnail(output_size)
        #     img.save(self.image.path)
