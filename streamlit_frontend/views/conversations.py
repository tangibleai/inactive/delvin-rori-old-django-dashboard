import streamlit as st
from datetime import datetime

from streamlit_frontend.src.etl import get_student_conversation_by_id

PAGE_SIZE = 20

def init_state(stud_msgs, message_id):
    if message_id:
        row_number = stud_msgs[stud_msgs['original_message_id'] == message_id].index[0] if not \
            stud_msgs[
                stud_msgs['original_message_id'] == message_id].empty else None
        init_from_msg = max(row_number - 1, 0)
    else:
        init_from_msg = max(len(stud_msgs) - PAGE_SIZE, 0)

    if "curr_from_msg" not in st.session_state:
        st.session_state["curr_from_msg"] = init_from_msg

    if "max_msg" not in st.session_state:
        st.session_state["max_msg"] = len(stud_msgs)

    if "curr_to_msg" not in st.session_state:
        st.session_state["curr_to_msg"] = min(st.session_state["curr_from_msg"] + PAGE_SIZE,
                                              st.session_state["max_msg"])

    if "earlier_btn_disabled" not in st.session_state:
        st.session_state["earlier_btn_disabled"] = False

def on_slider_move():
    st.session_state["curr_from_msg"] = st.session_state["convo_slider"]
    st.session_state["curr_to_msg"] = min(st.session_state["curr_from_msg"] + PAGE_SIZE,
                                          st.session_state["max_msg"])


# def load_earlier_messages():
#     st.session_state["curr_from_msg"] = max(st.session_state["curr_from_msg"] - PAGE_SIZE, 0)
#
# def load_later_messages():
#     st.session_state["curr_to_msg"] = min(st.session_state["curr_to_msg"] + PAGE_SIZE,
#                                           st.session_state["max_msg"])

def load_messages(student_messages, from_message, to_message):
    curr_messages = student_messages.iloc[from_message:to_message]

    for i, m in curr_messages.iterrows():
        col_time, col_message = st.columns([1,5])

        with col_time:
            st.caption(f":gray[{datetime.strftime(m['inserted_at'], '%Y-%m-%d %H:%M:%S')}]")


        with col_message:
            if m['direction'] == 'inbound':
                with st.chat_message("user"):
                    st.write(m["message_text"])
            else:
                with st.chat_message("assistant",
                                     avatar='https://images.squarespace-cdn.com/content/v1/6268eb8de6c2576f5d8f5a9a/ba250914-0a7d-49a8-b7b7-22d9892e6625/rori-500px.png'):
                    st.write(m["message_text"])

def load_page(contact_uuid=None, message_id=None):

    if "contact_uuid" not in st.session_state:
        st.session_state["contact_uuid"] = contact_uuid

    with st.form("Student_id"):
        col_input, col_button = st.columns([2,1])
        with col_input:
            single_contact_uuid = st.text_input("Contact UUID", value='356c40b6-ae76-4574-a36c-4d57c05ccbdd')

        with col_button:
            st.write("###")
            submit = st.form_submit_button("Display Student")

        if submit:
            st.session_state['contact_uuid'] = single_contact_uuid


    if st.session_state['contact_uuid']:
        stud_msgs = get_student_conversation_by_id(st.session_state['contact_uuid'])

        init_state(stud_msgs, message_id)

        st.slider("Browse conversation",
                               min_value=0,
                               max_value=len(stud_msgs),
                               value=st.session_state['curr_from_msg'],
                               on_change=on_slider_move,
                               key="convo_slider")

        _, col_button_left, col_title, col_button_right, _ = st.columns([1, 1, 3, 1, 1])

        if (st.session_state["curr_from_msg"] > 0):
            with st.expander("10 earlier messages"):
                load_messages(stud_msgs, max(st.session_state["curr_from_msg"]-10, 0),
                              st.session_state["curr_from_msg"] )

        load_messages(stud_msgs, st.session_state["curr_from_msg"], st.session_state["curr_to_msg"])

        if (st.session_state["curr_to_msg"] < st.session_state["max_msg"]):
            with st.expander("10 later messages"):
                load_messages(stud_msgs, min(st.session_state["curr_to_msg"]+1, st.session_state["max_msg"]),
                              min(st.session_state["curr_to_msg"]+11, st.session_state["max_msg"]) )





