class WhereClause:
    def __init__(self, orig=None):
        self.subclauses = {}
        if orig:
            self.subclauses = {field: [sc for sc in subclauses]
                               for field, subclauses in orig.subclauses.items()}

    def add_subclause(self, field, subclause, override_prev=True):
        if override_prev:
            self.subclauses[field] = [subclause]
        else:
            if not self.subclauses[field]:
                self.subclauses[field] = []

            self.subclauses[field].append(subclause)

    def add_str_subclause(self, field, value, operator='='):
        subclause = f"{field} {operator} '{value}' "
        self.add_subclause(field,subclause)

    def add_int_subclause(self, field, value, operator='='):
        subclause = f"{field} {operator} {value} "
        self.add_subclause(field,subclause)


    def add_datelimit_subclause(self, field, from_date, to_date):
        if from_date > to_date:
            raise Exception('Start date is later than end_date')

        from_subclause = field + " >= '" + from_date.strftime('%Y-%m-%d') + "' "
        to_subclause = field + " <= '" + to_date.strftime('%Y-%m-%d') + "' "
        self.subclauses[field] = [from_subclause,to_subclause]

    def get_clause(self):
        if self.subclauses:
            return 'WHERE ' + ' AND '.join(' AND '.join(value) for value in self.subclauses.values())
        else:
            return ''