from django.db import models

"""
If you don't want to save to the default database, add models to the 'dbrouters.py'
"""


class MessagesModel(models.Model):
    contact_uuid = models.CharField(max_length=100)
    inserted_at = models.DateTimeField()
    updated_on_supabase = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False  # to avoid migrations
        db_table = 'django_delvin_messages'


class MessagesIndicatorsModel(models.Model):
    name = models.CharField(max_length=255)
    value = models.FloatField()
    image = models.TextField()
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = 'django_delvin_messages_indicators'


class ActiveUserDataModel(models.Model):
    date = models.DateField()
    active_users = models.IntegerField()
    new_users = models.IntegerField()
    dormant_users = models.IntegerField()
    super_users = models.IntegerField()
    all_users = models.IntegerField()
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = 'active_user_data'


class ActiveUserDataDeltaModel(models.Model):
    date = models.DateField()
    active_user = models.IntegerField()
    delta = models.IntegerField()
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = 'active_user_data_delta'


class BQMessageModel(models.Model):
    original_message_id = models.CharField(max_length=100)
    contact_uuid = models.CharField(max_length=100)
    inserted_at = models.DateTimeField()
    updated_on_supabase = models.DateTimeField(auto_now=True)
    message_text = models.TextField()
    flow_name = models.CharField(max_length=200)
    direction = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'bq_messages'

class BQMessageModelRising(models.Model):
    original_message_id = models.CharField(max_length=100)
    contact_uuid = models.CharField(max_length=100)
    inserted_at = models.DateTimeField()
    updated_on_supabase = models.DateTimeField(auto_now=True)
    message_text = models.TextField()
    flow_name = models.CharField(max_length=200)
    direction = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'bq_messages_rising'


class BQUserModel(models.Model):
    contact_uuid = models.CharField(max_length=100, primary_key=True)
    inserted_at = models.DateTimeField()
    chat_per_week = models.TextField()
    current_activity = models.TextField()
    current_question = models.IntegerField()
    first_impression = models.TextField()
    grade_level = models.TextField()
    guardian_consent = models.TextField()
    name = models.TextField()
    onboarding_complete = models.TextField()
    opted_in = models.BooleanField()
    welcome_stack = models.TextField()
    helpdesk_mode = models.TextField()
    finished_math = models.TextField()
    age = models.TextField()

    class Meta:
        managed = False
        db_table = 'bq_contacts'


class StudentStatsModel(models.Model):
    contact_uuid = models.CharField(max_length=100)
    inserted_at = models.DateTimeField()
    num_messages = models.IntegerField()
    num_active_days = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'bq_contacts'


class LifetimeUserModel(models.Model):
    user_lifetime_in_days = models.IntegerField()
    user_type = models.CharField(max_length=100)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = 'lifetime_user_data'


class UserEngagement1Model(models.Model):
    contact_uuid = models.CharField(max_length=100)
    message_sent = models.IntegerField()
    lifetime = models.IntegerField()
    average_messages = models.FloatField()
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = 'user_engagement_data1'


class UserEngagement2Model(models.Model):
    contact_uuid = models.CharField(max_length=100)
    message_sent = models.IntegerField()
    active_days = models.IntegerField()
    average_messages = models.FloatField()
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = 'user_engagement_data2'


class UserEngagement3Model(models.Model):
    contact_uuid = models.CharField(max_length=100)
    weekly_messages = models.JSONField()
    active_weeks = models.IntegerField()
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = 'user_engagement_data3'


class UserEngagement3ModelDuplicate(models.Model):
    contact_uuid = models.CharField(max_length=255)
    weekly_messages = models.JSONField()
    active_weeks = models.IntegerField()
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = 'user_engagement_data3_duplicate'


class MathQuestionAnswerModel(models.Model):
    original_message_id = models.CharField(max_length=100, primary_key=True)
    text = models.TextField()
    message_inserted_at = models.DateTimeField()
    nlu_response_data = models.CharField(max_length=255)
    nlu_response_type = models.CharField(max_length=255)
    nlu_response_confidence = models.TextField()
    contact_uuid = models.CharField(max_length=100)
    question = models.TextField()
    question_level = models.IntegerField()
    question_skill = models.CharField(max_length=255)
    question_topic = models.CharField(max_length=255)
    question_number = models.TextField()
    expected_answer = models.CharField(max_length=100)
    question_micro_lesson = models.CharField(max_length=100)
    line_name = models.CharField(max_length=100)
    line_number = models.CharField(max_length=100)
    answer_type = models.CharField(max_length=255)
    supabase_id = models.IntegerField()
    hint_shown = models.TextField()
    latest_nlu_response_data = models.TextField()
    latest_nlu_response_confidence = models.FloatField()

    class Meta:
        managed = False
        db_table = 'math_question_answer'


class MicrolessonModel(models.Model):
    id = models.CharField(max_length=100, primary_key=True)
    question_microlesson = models.CharField(max_length=100)
    question_number = models.IntegerField()
    question = models.TextField()
    expected_answer = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = False
        db_table = 'microlesson'


class StudentMetricsModel(models.Model):
    contact_uuid = models.CharField(max_length=100, primary_key=True)
    inserted_at = models.DateTimeField()
    num_math_messages = models.IntegerField(blank=True, null=True)
    num_messages = models.IntegerField(blank=True, null=True)
    num_active_days = models.IntegerField(blank=True, null=True)
    num_active_weeks = models.IntegerField(blank=True, null=True)
    num_sessions = models.IntegerField(blank=True, null=True)
    onboarding_stage = models.CharField(max_length=100, blank=True, null=True)
    last_activity = models.CharField(max_length=100, blank=True, null=True)
    num_lessons_visited = models.IntegerField(blank=True, null=True)
    num_lessons_completed = models.IntegerField(blank=True, null=True)
    average_accuracy_rate = models.FloatField(blank=True, null=True)
    user_segment = models.CharField(max_length=100, blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'student_metrics'
