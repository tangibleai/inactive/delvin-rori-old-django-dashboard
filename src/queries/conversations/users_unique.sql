

SELECT
       id as chat_id
      ,contact_id
      ,state
      ,inserted_at as chat_inserted_at
      ,updated_at as chat_updated_at
    FROM
      (
      SELECT
      id 
      ,contact_id
      ,state
      ,inserted_at
      ,updated_at

      ,ROW_NUMBER() OVER (PARTITION BY uuid ORDER BY updated_at DESC) AS rownumber
      FROM
        `dimagi-turnio-chatbot-111120.243827325289.chats`
      )
    WHERE
    rownumber = 1

