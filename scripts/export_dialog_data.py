import os
import time
import pandas as pd
import datetime

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()

from dashboard.utils import select_from_supabase
from streamlit_frontend.src.const import *
from streamlit_frontend.src.sql_utils import WhereClause

supabase_db_name = "supabase-db-rori"

date_limit_list = [
    (datetime.date(2023,9,1), datetime.date(2023,5,31)),
]

for start_date, end_date in date_limit_list:
    where_clause = WhereClause()
    where_clause.add_datelimit_subclause('inserted_at', from_date=start_date, to_date=end_date)

    print (f"Pulling data from supabase: {start_date.strftime('%Y-%m-%d')}-{end_date.strftime('%Y-%m-%d')}")
    query = f"SELECT * FROM bq_messages_rising {where_clause.get_clause()};"
    query_result = select_from_supabase(db_name=supabase_db_name, query=query)

    df = pd.DataFrame.from_records(query_result)
    df.to_csv(f"messages.{start_date.strftime('%Y-%m-%d')}-{end_date.strftime('%Y-%m-%d')}.rising_line.csv", index=False)

