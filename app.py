import os
import time

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()


import streamlit as st
from streamlit_option_menu import option_menu

from streamlit_frontend.views import analytics, question_explorer, students, conversations
from streamlit_frontend.src.auth import create_authenticator
from streamlit_frontend.src.config import AppConfig
from streamlit_frontend.src.styles import set_page_container_style
from streamlit_frontend.src.ui import menu_icons
from streamlit_frontend.src.const import CONVERSATIONS_INDEX


import math

st.set_page_config(page_title="Rori Data Dashboard", layout='wide')
set_page_container_style()


def create_page_structure():

    config = AppConfig()
    apps = config.get_apps()

    icons = [menu_icons[app] for app in apps]
    styles = {
        "icon": {"color": "#4141f7", "font-size": "18px"},
        "nav-link": {"font-size": "18px", "text-align": "left", "margin": "0px", "--hover-color": "#A29BFE"},
        "nav-link-selected": {"background-color": "#4141f7"},
    }

    query_params = st.experimental_get_query_params()

    try:
        menu_index = int(query_params['menu_index'][0])
    except Exception:
        menu_index = 0

    try:
        contact_uuid = query_params['contact_uuid'][0]
    except Exception:
        contact_uuid = None

    try:
        msg_id = query_params['message_id'][0]
    except Exception:
        msg_id = None



    with st.sidebar:
        st.image("streamlit_frontend/assets/rising_academies.png", width=270)

        selected = option_menu(
            menu_title=None,  # required
            options=apps,  # required
            icons=icons,  # optional
            menu_icon="menu-down",  # optional
            default_index=menu_index,
            styles=styles# optional
        )

        st.image("streamlit_frontend/assets/delvin_logo_270.png")
        # st.image("streamlit_frontend/assets/powered_by_tai.png", width=270)

    if selected == "Analytics":
        analytics.load_page()

    elif selected == "Students":
        students.load_page()

    elif selected == "Math Questions":
        question_explorer.load_page()

    elif selected == "Conversations":
        conversations.load_page(contact_uuid, msg_id)


authenticator = create_authenticator()
name, authentication_status, username = authenticator.login('Login', 'main')

if authentication_status:
    create_page_structure()