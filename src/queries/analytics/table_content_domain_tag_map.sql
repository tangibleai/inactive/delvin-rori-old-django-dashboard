/* 
CONTEXT: 
- Generates a table that maps content pieces to domain tags
RESULT EXPECTATION
- A mapping between content_id to domain tags that includes tag_name and tag_id
*/
SELECT card_tag_map.card_uuid as card_uuid,
       domain_tags.id as tag_id,
       domain_tags.tag_name as tag_name        
FROM
(
  {{variables.tables.content_tag_map}} as card_tag_map
  inner join
  (SELECT * FROM {{variables.tables.tag_table}} where tag_category = 'domain_tag') as domain_tags
  ON card_tag_map.tag_id = domain_tags.id
)
