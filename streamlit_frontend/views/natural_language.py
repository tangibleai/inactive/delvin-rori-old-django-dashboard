import streamlit as st

def load_page():
    tab_math_ans, tab_oos, tab_other_intents = st.tabs("Math Answers", "Out Of Scope", "Other Intents")

    with tab_oos:


        colms = st.columns((1, 2, 3, 2, 1))
        fields = ['date', 'email', 'uid', 'verified', 'action']
        for col, field_name in zip(colms, fields):
            # header
            col.write(field_name)