import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
django.setup()

from dashboard.utils import select_from_supabase
import pandas as pd
import numpy as np
import time
from datetime import datetime
import json
from dashboard.models import UserEngagement3Model

MAX_WEEK = 26  # max value for x axis
MAX_STUDENT = float('inf')  # This line is to test. Type a small value while testing.

"""Calculation example, starting from first week, we count for each week total messages sent per user

cetin
01.01.2023 - 08.01.2023 : week0, message count
08.01.2023 - 15.01.2023 : week1, message count
15.01.2023 - 23.01.2023 : week2, message count

hobson
03.01.2023 - 10.01.2023 : week0, message count
17.01.2023 - 22.01.2023 : week1, message count
22.01.2023 - 29.01.2023 : week1, message count
"""


def calculate_metrics(stop_date):
    print("Selecting data from Supabase!")

    _db_name = 'supabase-db-rori'
    # Get all necessary data for calculations
    query = """
    SELECT contact_uuid, retention_week, count (distinct id) as num_messages
        FROM
            (
                SELECT id, contact_uuid, (EXTRACT(days from (inserted_at - first_message_timestamp)) / 7)::int AS retention_week
                FROM
                    (
                    SELECT id, contact_uuid, inserted_at, MIN(inserted_at) over (PARTITION BY contact_uuid) AS first_message_timestamp
                    FROM django_delvin_messages
                    ) as messages
            ) as messages_with_ret_week
    GROUP BY contact_uuid, retention_week;
    """
    tic = time.time()
    query_result = select_from_supabase(db_name=_db_name, query=query)

    df = pd.DataFrame.from_records(query_result)
    passed = time.time() - tic
    print(f"Time passed:{passed // 60} min, {passed % 60} sec")

    print("Starting calculations!")

    # Calculate the number of distinct retention_weeks per user_id
    weeks_per_user = df.groupby('contact_uuid')['retention_week'].nunique()

    # Calculate the maximal retention_week per user_id
    max_week = df.groupby('contact_uuid')['retention_week'].max()

    # Combine the results into a new DataFrame

    pt = pd.pivot_table(df, index='contact_uuid', columns='retention_week', values='num_messages', aggfunc=np.mean)
    pt = pt.fillna(0.0)
    pt["combined"] = pt.apply(lambda row: list(row), axis=1)
    result_df = pd.DataFrame({
                                 'active_weeks': weeks_per_user,
                                 'max_week': max_week,
                                 'weekly_messages': pt['combined']}).reset_index()
    # result_df.rename(columns={'contact_uuid': 'user_sha'}, inplace=True)

    # Delete all data before calculations
    query = "delete from user_engagement_data3;"
    select_from_supabase(db_name=_db_name, query=query)

    batch_size = 1000

    tic = time.time()
    for i in range(0, len(result_df), batch_size):
        batch_df = result_df.iloc[i:i + batch_size]  # Get the current batch

        # Convert each row in the batch into a Django model object and append it to the list
        model_objects = [
            UserEngagement3Model(
                contact_uuid=row['contact_uuid'],
                weekly_messages=json.dumps(row['weekly_messages'][:MAX_WEEK]),
                active_weeks=row['active_weeks']
            )
            for _, row in batch_df.iterrows()
        ]

        UserEngagement3Model.objects.bulk_create(model_objects)

    passed = time.time() - tic
    print(f"Time passed:{passed // 60} min, {passed % 60} sec")


if __name__ == "__main__":
    stop_date_ = datetime.now()
    calculate_metrics(stop_date=stop_date_)
