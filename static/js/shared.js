let navbar = document.querySelector('.navbar');
let nav_btn_div = document.getElementsByClassName("nav-btn")[0];

navbar.addEventListener("mouseenter", () => {
    document.querySelector('.navbar').classList.toggle('open');
    nav_btn_div.style.display = "none";
})

navbar.addEventListener("mouseleave", () => {
    document.querySelector('.navbar').classList.remove('open');
    nav_btn_div.style.display = "block";
})